import * as oracledb from 'oracledb';
import { DB_CONN } from '../config';
export const DBConn = DB_CONN;
class DB {
    pools;
    constructor() {
        oracledb.outFormat = oracledb.OBJECT;
        oracledb.fetchAsString = [ oracledb.CLOB ];
        oracledb.createPool(DBConn).then((pool)=>{
            console.log("DB pool created");
        }).catch((err)=>{
            console.log(err);
        });
    }

    getConnection() {
        return new Promise((resolve, reject)=>{
            oracledb.getConnection().then((conn)=>{
                resolve(conn);
            }).catch((err)=>{
                reject(Error(err));
            });
        });
    }

    mutation(conn, sql, binds={}) {
        return new Promise((resolve, reject)=>{
            conn.execute(sql, binds).then((result)=>{
                resolve(result.outBinds);
            }).catch((err)=>{
                reject(Error(err));
            });
        });
    };

    query(sql, conn = null, params:any = [], option: any = {}): Promise<Array<any>> {
        if (conn) {
            return new Promise((resolve, reject)=>{
                conn.execute(sql, params, option).then((result)=>{
                    if(result.rows == undefined)
                        resolve(result);
                    else
                        resolve(result.rows);
                }).catch((err)=>{
                    reject(Error(err));
                });
            });
        } else {
            return new Promise((resolve, reject)=>{
                option.autoCommit = true;
                oracledb.getConnection().then((conn)=>{
                    conn.execute(sql, params, option)
                    .then((result)=>{
                        conn.close().then();
                        if(result.rows == undefined)
                            resolve(result);
                        else
                            resolve(result.rows);
                    }).catch((err)=>{                        
                        conn.close().then();
                        reject(Error(err));
                    });
                }).catch((err)=>{
                    reject(Error(err));
                });
            });
        }
    }

    areas(areaNo: number) {
        let sql = `
            select AREA_NO "id", AREA_NAME "name", CAMPAIGN_TEXT "campaign", IMAGE_URL "imageURL", AREA_TEXT "text", LICENSE_TEXT "agreement", 
                (select sum(MEMBER_CNT) members from STORE_INFO a, STORE_DETAIL b where a.STORE_NO = b.STORE_NO and a.AREA_NO=SERVICE_AREA.AREA_NO) "nMembers"
            from SERVICE_AREA where USE_YN=1 and DISP_YN='1'
        `;
        if (areaNo !== undefined)
            sql += ` and AREA_NO=${areaNo}`;

        sql += ` order by DISP_ORD`;

        return this.query(sql);
    }
    attrs(storeNo:number){
      let sql = `select (STORE_NO||'-'||ATTR_CODE) "id", ATTR_CODE "code", f_get_codename('120', ATTR_CODE) "value" from store_attr where store_no=${storeNo} and ATTR_VALUE='1'`;
      return this.query(sql);
    }

    menus(storeNo:number , setYN:String , today:String='0' , categoryNo:String ="-1"){
        let query =
            'select MENU_NO as "menuNo",' +
            ' MENU_CODE as "menuCode",' +
            ' MENU_NAME as "menuName", ' +
            'MENU_NAME_EN as "menuNameEn", ' +
            'TAX_TYPE as "taxType", ' +
            'TAX_AMT as "taxAmt", ' +
            'UNIT_PRICE  as "unitPrice", ' +
            'SALE_AMT  as "saleAmt", ' +
            'USE_YN as "useYN", ' +
            'STAMP_USE_YN as "stampUseYN", ' +
            'DOUBLE_MENU_YN as "doubleMenuYN", ' +
            'DOUBLE_ADD_AMT as "doubleAddAmt", ' +
            'TAKEOUT_DC_AMT as "takeoutDCAmt", ' +
            'MENU_ATTR as "menuAttr", ' +
            'MENU_TYPE as "menuType",  ' +
            ' to_char(START_DT,\'yyyy-mm-dd\')  as "startDt", ' +
            'to_char(END_DT,\'yyyy-mm-dd\') as "endDt", ' +
            'CATEGORY_NO as "categoryNo", ' +
            ' to_char(REG_DTTM,\'yyyy-mm-dd hh24:mi\') as "regDttm",' +
            ' to_char(UPD_DTTM,\'yyyy-mm-dd hh24:mi\') as "updDttm", ' +
            'REG_USER_NO  as "regUserNo", ' +
            'MENU_BG_COLOR  as "menuBgColor", ' +
            'DISP_ORDER  as "dispOrder" ' +
            'from store_menu ' +
            'where store_no = '+storeNo;
        if(categoryNo !="-1"){
            query = query + ' and CATEGORY_NO ='+categoryNo
        }
        if(today =='1'){
            query = query + ' and sysdate between START_DT and END_DT'
        }
        if(setYN=='0'){
            query = query +'and  not (menu_No in (select SET_MENU_NO from store_set_menu) ) '
        } else if(setYN=='1' ){
           query = query +'and   (menu_No in (select SET_MENU_NO from store_set_menu) ) '
        }
      return this.query(query);
    }

    stores(page: any, orderBy: string, areaNo: number, storeNo: number, q: string, userNo: number = -1) {
        switch(orderBy) {
            case "NAME":
                orderBy = "STORE_NAME";
            break;
            case "REVIEW":
                orderBy = "REVIEW_CNT desc, STORE_NAME";
            break;
            case "RATING":
                orderBy = "ESTIMATION_POINT desc, STORE_NAME";
            break;
            case "VOTE":
                orderBy ="MEMBER_CNT desc, STORE_NAME";
            break;
        }
        let sql = `
            select rownum, a.STORE_NO "id", STORE_DISP_NAME "name", CAMPAIGN_TEXT "campaign", ADDR1 "addr1", ADDR2 "addr2", TEL "tel",
                STORE_TEXT "text", SERVICE_TIME_TEXT "serviceTime", DIRECTIONS "locationInfo", PARKING_INFO "parkingInfo",
                MEMBERSHIP_TEXT "membershipInfo", HASH_TAG "hashtag", ESTIMATION_POINT "rating", REVIEW_CNT "nReviews", MEMBER_CNT "nMembers",
                (select THUMBNAIL_URL_L from STORE_IMAGE where STORE_NO=a.STORE_NO and DISP_ORD=1) "mainImageURL",
                (select nvl(count(1), 0) from STORE_FAVORITE where USER_NO=${userNo} and STORE_NO=a.STORE_NO and USE_YN<>'0') "isVoted",
                LATITUDE "lat", LONGITUTE "lng", b.POS_NAME "posNm", b.POS_BACK_URL "posBackUrl", b.promotion_img "promotionImg",
                (case when a.NLS_CODE is null then 'ko' else a.NLS_CODE end) "nlsCode" 
            from STORE_INFO a, STORE_DETAIL b where a.STORE_NO=b.STORE_NO and a.SERVICE_STATUS in ('1','2') `;
        
        if (areaNo===0 || areaNo)
            sql += ` and AREA_NO=${areaNo}`;
        if (storeNo)
            sql += ` and a.STORE_NO=${storeNo}`;
        if (q) {
            q = q.toUpperCase();
            sql += ` and (UPPER(a.store_disp_name) like '%${q}%' or
                b.hash_tag like '%${q}%' or
                a.tel like '%${q}%')`;
        }
        if (orderBy)
            sql += ` order by ${orderBy}`;
            
        if (page)
            sql = this._pagination(page, sql);
        return this.query(sql);
    }
    getChangedOrder(orderNo:number){
        console.log(orderNo);
        let sql = `
            select  order_no "id", order_amt "total", to_char(order_dttm,'yyyy-mm-dd hh24:mi:ss') "orderDttm", table_no_array "tableId"   
            from    ORDER_MASTER  
            where   order_status='1' 
                    and order_no = ${orderNo} `;
        return this.query(sql);
    }
    storeImages(storeNo: number) {
        let sql = `
            select DISP_ORD "dispOrder", IMAGE_URL "imageURL", 
                THUMBNAIL_URL_S "thumbnailS", THUMBNAIL_URL_M "thumbnailM", THUMBNAIL_URL_L "thumbnailL",
                ('${storeNo}' || DISP_ORD) "id"
            from STORE_IMAGE where STORE_NO=${storeNo} order by DISP_ORD
        `;

        return this.query(sql);
    }

    reviews(page: any, storeNo: number, userNo: number = -1) {
        let sql = `
            select REVIEW_NO "id", USER_NO "userNo", a.STORE_NO "storeNo", STORE_DISP_NAME "storeName", REVIEW_TEXT "text", COMMENT_CNT "nComments", 
                RECOMMENDED_CNT "nRecomends", ESTIMATION_POINT "rating", to_char(REG_DTTM,'yyyy-mm-dd hh24:mi') "regDate", 
                (select c.DOWNLOAD_URL from REVIEW_FILE c where c.REVIEW_NO=a.REVIEW_NO) "imageURL", 
                (select c.THUMBNAIL_URL_L from REVIEW_FILE c where c.REVIEW_NO=a.REVIEW_NO) "thumbnailURL", 
                RECOMMENDED_CNT "nVotes", 
                (select nvl(count(1),0) from REVIEW_JOIN where REVIEW_NO = a.review_no and USER_NO = ${userNo}) "isVoted"
            from REVIEW a, STORE_INFO b where STATUS in(1,2) and a.STORE_NO = b.STORE_NO
        `;
        if (storeNo)
            sql += ` and a.STORE_NO=${storeNo}`;

        sql += ` order by REG_DTTM desc`;

        if (page)
            sql = this._pagination(page, sql);
        return this.query(sql);
    }

    getReview(reviewNo: number, userNo: number = -1) {
        let sql = `
            select REVIEW_NO "id", USER_NO "userNo", a.STORE_NO "storeNo", STORE_DISP_NAME "storeName", REVIEW_TEXT "text", COMMENT_CNT "nComments", 
                RECOMMENDED_CNT "nRecomends", ESTIMATION_POINT "rating", to_char(REG_DTTM,'yyyy-mm-dd hh24:mi') "regDate", 
                (select c.THUMBNAIL_URL_L from REVIEW_FILE c where c.REVIEW_NO=a.REVIEW_NO) "imageURL", RECOMMENDED_CNT "nVotes", 
                (select nvl(count(1),0) from REVIEW_JOIN where REVIEW_NO = a.review_no and USER_NO = ${userNo}) "isVoted"
            from REVIEW a, STORE_INFO b where a.REVIEW_NO=${reviewNo} and STATUS in(1,2) and a.STORE_NO = b.STORE_NO
        `;

        return this.query(sql);
    }

    comments(reviewNo: number, userNo: number = -1) {
        let sql = `
            select COMMENT_NO "id", REVIEW_NO "reviewNo", USER_NO "userNo", COMMENT_TEXT "text", to_char(REG_DTTM,'yyyy-mm-dd hh24:mi') "regDate" from REVIEW_COMMENT 
            where STATUS in (1,2) and REVIEW_NO=${reviewNo} order by REG_DTTM desc
        `;

        return this.query(sql);
    }

    getComment(commentNo: number, userNo: number = -1) {
        let sql = `
            select COMMENT_NO "id", USER_NO "userNo", COMMENT_TEXT "text", to_char(REG_DTTM,'yyyy-mm-dd hh24:mi') "regDate",
                case when USER_NO=506 then 1 else 0 end "isMine"            
            from REVIEW_COMMENT 
            where STATUS in (1,2) and COMMENT_NO=${commentNo}
        `;

        return this.query(sql);
    }

    user(userNo: number) {
        if (userNo == -1)
            return new Promise<any>((resolve, reject)=>{
                resolve([{id: -1, name: '비회?��', imageURL: ''}]);
            });

        let sql = `
            select USER_NO "id", USER_NAME "name", THUMBNAIL_URL "imageURL" from USER_MASTER where USER_NO=${userNo}
        `;
        return this.query(sql);
    }

    addReview(input: any, userNo: number = -1) {
        let connection = null;
        if (input.reviewNo != null) {
            // 리뷰 ?��?��
            let sql = `
                UPDATE REVIEW
                SET REVIEW_TEXT = '${input.text}',
                    IMAGE_CNT = 1, 
                    UPD_DTTM = sysdate,
                    STATUS = '2',
                    ESTIMATION_POINT = ${input.rating}
                WHERE REVIEW_NO = ${input.reviewNo}
            `;
            return this.getConnection()
                .then((conn)=>{
                    connection = conn;
                    return this.mutation(conn, sql, {});
                })
                .then((res)=>{
                    let image = `
                        MERGE INTO review_file
                        USING DUAL ON (review_no = ${input.reviewNo} and disp_ord = 1)
                        WHEN MATCHED THEN
                            UPDATE
                                SET DOWNLOAD_URL = '${input.image.imageURL}',
                                THUMBNAIL_URL_S = '${input.image.thumbnailS}',
                                THUMBNAIL_URL_M = '${input.image.thumbnailM}',
                                THUMBNAIL_URL_L = '${input.image.thumbnailL}',
                                UPD_DTTM = sysdate
                            WHERE REVIEW_NO = ${input.reviewNo} AND DISP_ORD = 1
                        WHEN NOT MATCHED THEN
                            INSERT (
                                REVIEW_NO, DISP_ORD, DOWNLOAD_URL,
                                THUMBNAIL_URL_S, THUMBNAIL_URL_M, THUMBNAIL_URL_L, UPD_DTTM
                            )
                            VALUES (
                                ${input.reviewNo},
                                1,
                                '${input.image.imageURL}',
                                '${input.image.thumbnailS}',
                                '${input.image.thumbnailM}',
                                '${input.image.thumbnailL}',
                                sysdate
                            )
                    `;
                    return this.mutation(connection, image);
                })
                .then(res=>connection.commit())
                .then(res=>connection.close())
                .then(res=>this.getReview(input.reviewNo, userNo))
                .catch((err)=>{
                    if(connection)
                        connection.close().then();
                });
        } else {
            // 리뷰 추�??
            return this.getConnection()
                // 리뷰번호
                .then((conn)=>{
                    connection = conn;
                    let seq = `select SEQ_REVIEW_NO.NEXTVAL "no" from dual`;
                    return this.query(seq, connection);
                })
                // 리뷰?��?��
                .then((row)=>{
                    input.reviewNo = row[0].no;
                    let review = `
                        INSERT INTO REVIEW (
                            REVIEW_NO, USER_NO, REVIEW_TEXT,
                            IMAGE_CNT, COMMENT_CNT, REG_DTTM,
                            UPD_DTTM, REP_DTTM, DEL_DTTM,
                            STATUS, READ_CNT, RECOMMENDED_CNT,
                            ESTIMATION_POINT, STORE_NO)
                        VALUES (
                            ${input.reviewNo}, ${userNo}, '${input.text}', 1, 0, sysdate, sysdate, null, null,
                            '1', 0, 0, ${input.rating}, ${input.storeNo}
                        )
                    `;
                    return this.mutation(connection, review);
                })
                // ?��진삽?��
                .then((res)=>{
                    let image = `
                        INSERT INTO REVIEW_FILE (
                            REVIEW_NO, DISP_ORD, DOWNLOAD_URL,
                            THUMBNAIL_URL_S, THUMBNAIL_URL_M, THUMBNAIL_URL_L,
                            UPD_DTTM)
                        VALUES (
                            ${input.reviewNo},
                            1, '${input.image.imageURL}', '${input.image.thumbnailS}',
                            '${input.image.thumbnailM}', '${input.image.thumbnailL}', sysdate
                        )
                    `;
                    return this.mutation(connection, image);
                })
                // 커밋
                .then(res=>connection.commit())
                .then(res=>connection.close())
                // 결과리턴
                .then((res)=>this.getReview(input.reviewNo, userNo))
                .catch((err)=>{
                    if(connection)
                        connection.close().then();

                    console.log(err);
                });
        }
    }

    deleteReview(reviewNo: number, userNo: number) {
        let sql = `
            UPDATE REVIEW SET DEL_DTTM = sysdate, STATUS = '3' WHERE REVIEW_NO = ${reviewNo} and USER_NO=${userNo}
        `;
        return this.query(sql);
    }

    addComment(input: any, userNo: number) {
        let connection = null;
        if (input.commentNo != null) {
            return this.getConnection()
                .then(conn=>{
                    connection = conn;
                    let sql = `
                        UPDATE REVIEW_COMMENT SET COMMENT_TEXT='${input.text}', UPD_DTTM=SYSDATE, STATUS='2' WHERE COMMENT_NO=${input.commentNo} AND USER_NO=${userNo}
                    `;
                    return this.query(sql);
                })
                .then(res=>connection.close().then())
                .then(out=>{
                    return this.getComment(input.commentNo, userNo);
                })
                .catch((err)=>{
                    if(connection)
                        connection.close().then();

                    console.log(err);
                });
        } else {
            return this.getConnection()
                .then(conn=>{
                    let sql = `
                        INSERT INTO REVIEW_COMMENT
                        VALUES (
                            SEQ_COMMENT_NO.NEXTVAL,
                            ${input.reviewNo}, ${userNo}, '${input.text}', sysdate, sysdate, null, null,'1'
                        ) RETURNING COMMENT_NO into :commentNo
                    `;
                    connection = conn;
                    return connection.execute(sql, { commentNo: { type: oracledb.NUMBER, dir: oracledb.BIND_OUT } }, { autoCommit: true });
                })
                .then(out=>{
                    connection.close().then();
                    return this.getComment(out.outBinds.commentNo[0], userNo);
                })
                .catch((err)=>{
                    if(connection)
                        connection.close().then();

                    console.log(err);
                });
        }
    }

    deleteComment(commentNo, userNo) {
        let sql = `
            UPDATE REVIEW_COMMENT SET
                DEL_DTTM = SYSDATE,
                STATUS = '3'
            WHERE COMMENT_NO=${commentNo} and USER_NO=${userNo}
        `;

        return this.query(sql);
    }

    voteReview(reviewNo, userNo) {
        let sql = `select * from REVIEW_JOIN WHERE user_no = ${userNo} and review_no=${reviewNo}`;
        return this.query(sql)
            .then((res: any[])=>{
                let sql;
                if (res.length > 0) {
                    sql = `
                        DELETE REVIEW_JOIN WHERE REVIEW_NO=${reviewNo} and USER_NO=${userNo}
                    `;
                } else {
                    sql = `
                        INSERT INTO REVIEW_JOIN (REVIEW_NO, USER_NO, JOIN_TYPE, REG_DTTM)
                        VALUES (${reviewNo}, ${userNo}, '1', sysdate)
                    `;
                }

                return this.query(sql);
            })
            // 결과리턴
            .then((res)=>this.getReview(reviewNo, userNo));
    }

    voteStore(storeNo, userNo) {
        let sql = `select * from STORE_FAVORITE where USER_NO=${userNo} and STORE_NO=${storeNo}`;
        return this.query(sql)
            .then((res: any[])=>{
                let vote = 0;
                if (res.length <= 0 || res[0].USE_YN == '0')
                    vote = 1;

                let sql = `
                    MERGE INTO STORE_FAVORITE USING DUAL ON (user_no = ${userNo} and store_no = ${storeNo})
                    WHEN MATCHED THEN
                        UPDATE SET DEL_DTTM = decode('${vote}','1',null,SYSDATE),
                            USE_YN = '${vote}'
                    WHEN NOT MATCHED THEN
                        INSERT (USER_NO, STORE_NO, REG_DTTM, DEL_DTTM, USE_YN, TRAN_CNT)
                            VALUES (${userNo}, ${storeNo}, sysdate, null, '1',0) `;
                return this.query(sql);
            });
    }

    async addOrder(input: any, details: any , userNo: number = -1,storeNo: number = -1) {
        let connection = null;
        let orderNo : number = input.orderNo;
        let total = 0;
        let dc_total = 0;
        let tax_total = 0;
        let cnt = [];
        let stock = [];
        console.log('add Order!!!!');
        // order_no 있을 경우 : 주문 수정
        // order_no 없을 경우 : 주문 추가
        try{
            connection = await this.getConnection();
            // 이미 영업 마감되었을 경우 주문 추가 X
            let endCheck = await this.query(`select count(*) "cnt" from store_sales where store_no = :storeNo and close_dttm is null`, connection, [storeNo]);
            if(endCheck[0].cnt == 0){
                throw "business ended";
            }
            /**
             *  주문 정보 수정일 경우
             */ 
            if(input.orderNo != null){
                let sum = await this.query(`select sum(pay_total) "payTotal" from order_pay where order_no = :orderNo`,connection,[orderNo]);
                //결제 사항이 있을 경우 주문수정 불가!!
                if(sum && sum[0].payTotal > 0) throw "pay processing";
                // 기존 order에 해당하는 메뉴 목록 가져오기 ()
                let get_prev_menu = `
                        select 
                            a.MENU_NO "menuNo", a.ITEM_CNT "itemCNT" 
                        from ORDER_DETAIL a, STORE_MENU b 
                        where 
                            a.ORDER_NO = ${orderNo} 
                            and a.MENU_NO = b.MENU_NO 
                            and b.STOCK_SALE_YN = '1' `;
                let prev_menu_list = await this.query(get_prev_menu,connection);
                // 기존 주문 메뉴 목록에서 제외된 메뉴 고르기(stock을 update하기 위함)
                // 다 지우고 stock 다 올리고 다시 새걸로 삽입하고 싶지만 diff 때문에 난감스
                let deleted_menu = [];
                for(let prev of prev_menu_list){
                    let flag = false;
                    for(let cur of details){
                        //cur.opts?
                        if(cur.menuNo == prev.menuNo){
                            flag = true;
                            break;
                        }
                    }
                    if(!flag){
                        deleted_menu.push({
                            menuNo:prev.menuNo, 
                            itemCNT:prev.itemCNT
                        });
                    }
                }
                //삭제할 메뉴가 있을 경우 해당 메뉴의 stock 업데이트
                if(deleted_menu.length){ 
                    let upd_deleted_stockCNT_arr = [];
                    for(let menu of deleted_menu){
                        let upd_deleted_stockCNT = `
                            update STORE_MENU 
                            set 
                                STOCK_CNT = STOCK_CNT + ${menu.itemCNT},
                                SOLDOUT_YN = (CASE WHEN (STOCK_CNT + ${menu.itemCNT}) <= 0 THEN '1' 
                                                    ELSE '0' END),
                                UPD_DTTM = sysdate 
                            where MENU_NO = ${menu.menuNo} `;
                        upd_deleted_stockCNT_arr.push(upd_deleted_stockCNT);
                    }
                    let upd_deleted_stockCNT_promise = [];
                    for(let query of upd_deleted_stockCNT_arr){
                        upd_deleted_stockCNT_promise.push(this.query(query,connection));
                    }
                    await Promise.all(upd_deleted_stockCNT_promise);
                }

                let delete_order_detail = `delete from order_detail where ORDER_NO=${orderNo}`;
                await this.query(delete_order_detail,connection);
            }
            /**
             * 최초 주문 추가일 경우
             */
            else{
                // order_no 가져오기
                let get_order_no = `select SEQ_ORDER_NO.NEXTVAL as "orderNo" from  dual`;
                let new_orderNo = await this.query(get_order_no, connection);
                orderNo = new_orderNo[0].orderNo;
                let insert_order = `
                    INSERT 
                        INTO ORDER_MASTER 
                        (ORDER_NO, ORDER_DTTM, ORDER_AMT, ORDER_NAME, ORDER_TYPE 
                        ,PAY_DTTM, WAITING_NO, ORDER_STATUS, REG_USER_NO 
                        ,UPD_DTTM, STORE_NO, BOOKING_NO, TABLE_NO_ARRAY, FLOOR_NO) 
                    VALUES 
                        (${orderNo}, sysdate,0 ,'${input.orderName}','${input.orderType}',
                        null ,:watingNo ,'1' ,${userNo}, 
                        to_date(SYSDATE,'yyyy-mm-dd hh24:mi:ss') ,${storeNo}, null ,:tables, :floorNo)`;
                await this.query(insert_order,connection,[input.waitingNo,input.tables,input.floorNo]);
            }
            /**
             * 공통 부분
             */
            let insert_menu_arr = [];
            let insert_menu_opt_arr = [];
            let itemCNT = null;
            // 메뉴 옵션(order_opt 모두 삭제) -> 삭제 후 최신정보로 재삽입
            let dlt_order_opt = ` delete from order_opt where order_item_seq in (select order_item_seq from order_detail where order_no = ${orderNo}) `;
            await this.query(dlt_order_opt, connection);
            // 주문 총 금액 및 세금 계산
            for (let item of details) {                    
                itemCNT = item.itemCNT;
                if(itemCNT < 0){
                    throw {key:"itemCNT error", value:item.menuName};
                }
                let order_item_seq = await this.query(`select SEQ_ORDER_DETAIL.NEXTVAL as "val" from dual`);
                order_item_seq = order_item_seq[0].val;
                // 메뉴 옵션에 들어간 가격 추가
                let total_opt_amt = 0;
                if(item.opts){
                    for(let opt of item.opts){
                        let get_option = ` select menu_opt_no "menu_opt_no", opt_name "opt_name", opt_amt "opt_amt" from store_menu_opt where menu_opt_no = ${opt.menu_opt_no} `;
                        let opt_info:any = await this.query(get_option, connection);
                        opt_info = opt_info[0];
                        total_opt_amt += opt_info.opt_amt;
                        let opt_cnt = opt.cnt?opt.cnt:1;
                        let insert_order_opt = `
                            insert into order_opt (order_item_seq, opt_name, opt_cnt, opt_amt) values (${order_item_seq}, '${opt_info.opt_name}', ${opt_cnt}, ${opt_info.opt_amt * opt_cnt})`;
                        insert_menu_opt_arr.push(this.query(insert_order_opt, connection));
                    }
                }
                item.itemAmt += total_opt_amt;

                total += item.itemAmt * itemCNT;
                dc_total += (item.dcAmt | 0) * itemCNT;
                if(item.taxType == '1'){
                    let amt = (item.itemAmt - (item.dcAmt | 0)) * itemCNT;
                    let tax = 0;
                    tax = (amt / 11) * 10;
                    tax = Math.round((Math.floor(tax) / 10));
                    tax_total += tax;
                }
                // 주문 메뉴 추가
                let insert_menu = `
                    INSERT into order_detail
                        (ORDER_ITEM_SEQ, ORDER_NO ,MENU_NO, MENU_NAME, ITEM_AMT, ITEM_CNT, DC_NAME, DC_AMT)
                    VALUES
                        (${order_item_seq}, ${orderNo}, ${item.menuNo}, '${item.menuName}', ${item.itemAmt}, ${itemCNT}, :dcName, :dcAmt)`;
                insert_menu_arr.push(this.query(insert_menu, connection,[item.dcName, item.dcAmt]));
            }
            await Promise.all(insert_menu_arr);
            await Promise.all(insert_menu_opt_arr);
            // 메뉴 재고 정보 가져오기
            let get_stock_info = `
                SELECT a.STOCK_SALE_YN "stockSaleYn", a.STOCK_CNT "stockCNT", a.MENU_NO "menuNo" 
                from STORE_MENU a, ORDER_DETAIL b 
                where 
                    b.MENU_NO = a.MENU_NO 
                    and b.ORDER_NO = ${orderNo} `;
            let stock_info = await this.query(get_stock_info,connection);
            // 재고 정보 변수에 저장
            for(let item of stock_info){
                if(item.stockSaleYn == '1')
                    stock[item.menuNo] = item.stockCNT;
                else
                    stock[item.menuNo] = null;
            }
            // stock 정보 업데이트!
            let upd_stockCNT_arr=[];
            for(let item of details){
                if(stock[item.menuNo] != undefined || stock[item.menuNo] != null){
                    if(stock[item.menuNo] + item.diff < 0){
                        upd_stockCNT_arr = [];
                        throw {key:"stock error",value:item.menuName};
                    }
                }
                let upd_stockCNT = `
                    UPDATE STORE_MENU 
                    set 
                        STOCK_CNT = (CASE STOCK_SALE_YN WHEN '1' THEN STOCK_CNT + ${item.diff} 
                                                        ELSE NULL END),
                        SOLDOUT_YN =(CASE STOCK_SALE_YN 
                                        WHEN '1' THEN (CASE WHEN (STOCK_CNT + ${item.diff}) <= 0 THEN '1' 
                                                            ELSE '0' END)
                                        ELSE '0' 
                                    END),
                        UPD_DTTM = sysdate 
                    where MENU_NO = ${item.menuNo} `;
                upd_stockCNT_arr.push(this.query(upd_stockCNT, connection));
            }
            await Promise.all(upd_stockCNT_arr);
            let upd_user_master = `
                update order_master 
                set 
                    ORDER_AMT = ${total}, 
                    PAY_TOTAL = ${total}-${dc_total},
                    TAX_TOTAL = ${tax_total},
                    UPD_DTTM = sysdate   
                where order_no = ${orderNo} `;
            await this.query(upd_user_master, connection);
            await connection.commit();
            connection.close().then();    
            input.tables = input.tables.toString().replace(',','|');
            return {id:orderNo,total:total,tableId:input.tables,error:null};
        }catch(err){
            console.log('ADD ORDER FAILED!!');
            if(connection)
                connection.close().then();
            console.log(err);
            return {id:orderNo,total:null,error:err};
        }
    }
    async addOpt(name: string, amt: number, storeNo: number) {
        let menu_opt_no = await this.query(`select SEQ_MENU_OPT_NO.NEXTVAL "val" from dual `);
        menu_opt_no = menu_opt_no[0].val;
        await this.query(`
            INSERT INTO STORE_MENU_OPT(MENU_OPT_NO, STORE_NO, OPT_NAME, OPT_AMT, REG_DTTM, UPD_DTTM)
            VALUES (
                ${menu_opt_no}, ${storeNo},
                '${name}', ${amt}, sysdate, sysdate
            )
        `);
        return {
            id: menu_opt_no,
            name: name,
            amt: amt
        }
    }

    async deleteOpt(id: number, storeNo: number) {
        let connection = null;
        try{
            connection = await this.getConnection();
            await this.query(`delete from menu_opt where menu_opt_no = ${id}`, connection);
            await this.query(`delete from opt_group_item where menu_opt_no = ${id}`, connection);
            await this.query(`DELETE FROM STORE_MENU_OPT WHERE MENU_OPT_NO = ${id} and STORE_NO = ${storeNo} `, connection);
            await connection.commit();
            connection.close().then();
            return true;
        }catch(err){
            if(connection) connection.close().then();
            console.error('ERROR: delete opt.');
            console.error(err);
            return false;
        }
    }

    addOptToMenu(id: number, menuNo: number) {
        return this.query(`
            INSERT INTO MENU_OPT(MENU_OPT_NO, MENU_NO) VALUES (${id}, ${menuNo})
        `)
        .then(_=>true);
    }

    deleteOptFromMenu(id: number, menuNo: number) {
        return this.query(`
            DELETE FROM MENU_OPT WHERE MENU_OPT_NO = ${id} and MENU_NO = ${menuNo}
        `)
        .then(_=>true);
    }

    addCredit(input: any, details: any , userNo: number = -1,storeNo: number = -1) {
      let connection = null;
      let newOrderNo = input.orderNo;
      let total = 0;
      let tableNo = 0;
        return this.getConnection()
          .then(conn=>{
            connection = conn;
            let sql =`select SEQ_TABLE_NO.NEXTVAL as "val" from dual`
            return this.query(sql,connection)
          })
          .then(res=>{
            tableNo = res[0].val;
            let sql = ` MERGE INTO STORE_ATTR USING DUAL ON (STORE_NO = ${storeNo} and DISP_ORDER = -1) 
               WHEN MATCHED THEN 
               update set DISP_ORDER = -1
               WHEN NOT MATCHED THEN 
               INSERT (store_no,FLOOR_NO,FLOOR_NAME,TABLE_CNT,DISP_ORDER)
               VALUES (${storeNo},SEQ_FLOOR_NO.NEXTVAL, '?��?��',10,-1)`
            return this.query(sql,connection)
          })
          .then(res=>{
            let sql = `insert into store_table (table_no, table_name, store_no, floor_no, disp_order) values (${tableNo},'temp',${storeNo},(select floor_no from store_floor where store_no = ${storeNo} and disp_order = -1),-1)`
            return this.query(sql,connection)
          })
          .then(res=>{
            let sql;
            if (input.orderNo != null) {
                sql = `
                        UPDATE ORDER_MASTER SET UPD_DTTM=SYSDATE WHERE ORDER_NO=${input.orderNo}
                    `;
              } else {
                sql = `select SEQ_ORDER_NO.NEXTVAL as "val" from  dual`
              }
            return this.query(sql,connection);
          })
          .then(res=>{
            let sql;
            if (input.orderNo != null) {
              newOrderNo = input.orderNo
              sql = `delete from order_detail where ORDER_NO=${newOrderNo}`;
            } else {
              newOrderNo = res[0].val;

              sql = `INSERT
             INTO ORDER_MASTER
             (ORDER_NO ,ORDER_DTTM ,ORDER_AMT,ORDER_NAME ,ORDER_TYPE
            ,PAY_DTTM ,WAITING_NO ,ORDER_STATUS ,REG_USER_NO
            ,UPD_DTTM ,STORE_NO ,TABLE_NO
             ,BOOKING_NO )
             VALUES
            (
             ${newOrderNo} ,
             sysdate ,
             0 ,
             '${input.orderName}',
             '${input.orderType}' ,
             null ,
            ${input.waitingNo} ,
            '1' ,
             ${userNo} ,
            to_date(SYSDATE,'yyyy-mm-dd hh24:mi') ,
            ${storeNo} ,
            ${tableNo},
            null
            )`
            }
            return this.query(sql,connection);
          })
          .then(res=>{
            let arr = [];
            for (let item of details) {
              total += (item.itemAmt+item.optAmt)*item.itemCNT;
              let sql = `
                  INSERT into order_detail
                  (ORDER_NO ,MENU_NO, MENU_OPT_NO, MENU_NAME ,ITEM_AMT ,ITEM_CNT ,OPT_AMT)
                  VALUES
                  (${newOrderNo} ,${item.menuNo} , ${item.menuOptNo}, '${item.menuName}' ,${item.itemAmt} ,${item.itemCNT} ,${item.optAmt})
              `;

              arr.push(this.query(sql, connection));
            }
            if (arr.length > 0)
                return Promise.all(arr);
            else
                return new Promise((resolve, reject)=>{ resolve(true); });
          })
          .then(res=>{
            let sql = `
                  update order_master
                   set
                   ORDER_AMT = ${total}
                   where order_no = ${newOrderNo}
              `;

            return this.query(sql,connection);
          })
          .then(res=>connection.commit())
          .then(out=>{
            connection.close().then();
            return {id:newOrderNo,total:total};
          })
          .catch((err)=>{
            if(connection)
              connection.close().then();

            console.log(err);
          });
    }
    deleteOrder(orderNo, userNo) {
        let dlt_order = `
              update order_master set
                ORDER_STATUS = 0
               where order_no=${orderNo}
          `;
        let connection = null;
        return this.getConnection()
        .then(conn=>{
            connection = conn;
            return this.query(dlt_order,connection);
        })
        .then(res=>{
            let get_origin_menu = `
                select 
                    a.MENU_NO "menuNo", a.ITEM_CNT "itemCNT" 
                from ORDER_DETAIL a, STORE_MENU b 
                where 
                    a.ORDER_NO = ${orderNo} 
                    and a.MENU_NO = b.MENU_NO 
                    and b.STOCK_SALE_YN = '1' `;
            return this.query(get_origin_menu,connection);
        })
        .then((origin_menu:any)=>{
            let upd_deleted_stockCNT_arr = [];
            for(let menu of origin_menu){
                let upd_deleted_stockCNT = `
                    update STORE_MENU 
                    set 
                        STOCK_CNT = STOCK_CNT + ${menu.itemCNT},
                        SOLDOUT_YN = (CASE WHEN (STOCK_CNT + ${menu.itemCNT}) <= 0 THEN '1' 
                                            ELSE '0' END),
                        UPD_DTTM = sysdate 
                    where MENU_NO = ${menu.menuNo} `;
                upd_deleted_stockCNT_arr.push(upd_deleted_stockCNT);
            }
            let upd_deleted_stockCNT_promise = [];
            for(let query of upd_deleted_stockCNT_arr){
                upd_deleted_stockCNT_promise.push(this.query(query,connection));
            }
            return Promise.all(upd_deleted_stockCNT_promise);
        })
        .then(res=>{
            return connection.commit();
        })
        .then(end=>{
            connection.close().then();
            return true;
        })
        .catch(err=>{
            if(connection)
                connection.close().then();
            return false;
        });
                    
    }

    feeds(id: number = null, page: any = null) {
        let sql = `
            select a.notice_no as "id",
                a.notice_title as "title",
                a.notice_text as "text",
                to_char(a.reg_dttm, 'yyyy-mm-dd') as "date",
                a.image_url "image"
            from notice a
            where a.notice_user_type = 3
                and sysdate between a.start_dttm and a.end_dttm
        `;

        if (id)
            sql += ` and NOTICE_NO=${id}`;
        sql += ` order by DBMS_RANDOM.RANDOM`;

        if (page)
            sql = this._pagination(page, sql);
        return this.query(sql, null, {}, { fetchInfo: {"text": {type: oracledb.STRING}} });
    }

    story(id: number = -1, filter: any = null, page = null) {
        let sql =`
            select 
                STORY_NO "id",
                TO_CHAR(REG_DTTM, 'yyyy-mm-dd') "regDate",
                STORY_TEXT "text",
                READ_CNT "readCNT",
                STATUS "status",
                TITLE "title",
                IMAGE_URL "imageURL",
                CONTENT_IMAGE_URL "contentURL",
                STORY_YEAR "year",
                STORY_MONTH "month",
                STORY_CATEGORY "volume",
                f_get_codename('500', STORY_CATEGORY) "volumeName"
            from STORY where 1=1
        `;
        if(id != -1){
            sql += ` and story_no = ${id}`
        }
        if (filter && filter!=-1)
            sql += ` and STORY_CATEGORY=${filter}`;

        sql += ` order by REG_DTTM desc`;

        if (page)
            sql = this._pagination(page, sql);

        return this.query(sql, null, {}, { fetchInfo: {"text": {type: oracledb.STRING}} });
    }

    addStory(input) {
        let sql = `select SEQ_STORY_NO.NEXTVAL as "val" from  dual`;
        let connection = null;
        let id = 0;
        return this.getConnection()
            .then(conn=>{
                connection = conn;
                return this.query(sql,connection);
            })
            .then(res =>{
                id = res[0].val;
                let sql2 =`
                    insert into story 
                    (STORY_NO, REG_DTTM, STORY_TEXT ,READ_CNT, STATUS, TITLE,
                    IMAGE_URL, CONTENT_IMAGE_URL, STORY_YEAR, STORY_MONTH, STORY_CATEGORY)
                    values
                    (${id},sysdate, :storytext, 0, '${input.status}','${input.title}',
                    '${input.imageURL}', '${input.contentURL}',
                    ${input.year},${input.month},'${input.volume}'
                    )
                `
              return this.query(sql2,connection,{ storytext: input.text });
            })
            .then(res=>connection.commit())
            .then(out=>{
                connection.close().then();
                input.id = id;
                input.readCNT = 0;
                return input;
            })
            .catch((err)=>{
                if(connection)
                    connection.close().then();

                console.log(err);
                return 0;
            });

    }
    updateStory(storyNo:number,input:any){
        let sql = `
            update story set story_text = :storytext, title = '${input.title}',
            status = '${input.status}', image_url =  '${input.imageURL}', content_image_url='${input.contentURL}', story_year = ${input.year},
            story_month= ${input.month}, STORY_CATEGORY='${input.volume}'
            where story_no = ${storyNo}
            `;
        return this.query(sql, null, { storytext: input.text })
            .then(res=>{
                input.id = storyNo;
                input.readCNT = 0;
                return input;
            });
    }
    readStory(storyNo:number){
        let sql = `
            update story set read_cnt = (select sum(read_cnt) from story where story_no= ${storyNo})+1
            where story_no = ${storyNo}
        `;
        return this.query(sql).then(res=>1);
    }
    deleteStory(storyNo:number){
        let sql = `
            delete from story where story_no = ${storyNo}
        `;
        return this.query(sql).then(res=>true);
    }

    private _pagination(page: any, sql: string) {
        return `
            SELECT * FROM ( SELECT tmp.*, rownum rn FROM (${sql}) tmp WHERE rownum <= ${page.offset + page.limit}) WHERE rn > ${page.offset}
        `;
    }
}
let db = new DB();
export { db };
