/**
 * Created by sdh077 on 17. 3. 24.
 */
var jwt = require('jsonwebtoken');


const key = "ppoint"
class JWT {
  getJWT(userNo): any {
    let payLoad  = {
          'userNo':userNo
          ,userType:'user'
        };
    let exp;
    let token =  jwt.sign(payLoad,key,{
      algorithm : 'HS256', //"HS256", "HS384", "HS512", "RS256", "RS384", "RS512" default SHA256
      expiresIn : 3600 //expires in 24 hours
    });
    let refresh_token =  jwt.sign({userNo: payLoad.userNo, type:'refresh'},key,{
      algorithm : 'HS256', //"HS256", "HS384", "HS512", "RS256", "RS384", "RS512" default SHA256
      expiresIn : 3600*24 //expires in 24 hours
    });
    return new Promise((resolve, reject)=> {
      jwt.verify(token, key, function (err, decoded) {
        if (err) {
          reject(Error("토큰생성실패"));
        }
        exp = decoded.exp*1000
        resolve({
          token_type:"JWT",
          expires_in:exp,
          access_token:token,
          refresh_token:refresh_token
        });
      });
    });
  }
  verifyJWT(jwtoken) {
    return new Promise((resolve, reject) => {
      jwt.verify(jwtoken, key, function (err, decoded) {
        if (err) {
          reject(Error("유효하지 않은 토큰"));
        }
        resolve(decoded);
      });
    });
  }
}
export var jsonWT = new JWT();
export function validateToken(token: any) {
  var user: any = { id: -1 };

  return jsonWT.verifyJWT(token)
  .then((result: any)=>{
    if(result){
      if(result.userType && result.userType == 'user'){
        user.no = result.userNo;
        user.userType = result.userType;
      } else if (result.userType && result.userType == 'store'){
        user.no = result.no;
        user.uid = result.uid;
        user.userType = result.userType;
      } else {
        throw new Error("Invalid token1.");
      }
    }else{
      throw new Error("Invalid token2.")
    }
    return user;
  }).catch(err=>{
    throw new Error("Invalid token3.");  // 운영에선 이 코드를 넣으세요.
      
    // user.no = 266; // 운영에선 이 코드를 지우세요.
    // user.uid = 181; // 운영에선 이 코드를 지우세요.
    // user.userType = 'store'; // 운영에선 이 코드를 지우세요.
    // return user; // 운영에선 이 코드를 지우세요.
  });
}