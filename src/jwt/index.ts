import { JWT_AUDIENCE, JWT_ISSUER, JWT_SECRET } from '../config';
const expressJWT = require('express-jwt');

export const jwt = expressJWT({
    secret: JWT_SECRET,
    audience: JWT_AUDIENCE,
    issuer: JWT_ISSUER
}).unless({
    path: [
        {
            url:'/pcp/auth/test',
            methods: ['GET']
        },
        {
            url: '/pcp/auth/token',
            methods: ['POST']
        },
        {
            url: '/pcp/payload',
            methods: ['POST']
        },
        {
            url:'/pcp/store/menuCategory/simple',
            methods:['GET']
        }
    ] 
});