import { CP_SERVER_KEY, PP_SERVER_KEY } from "./config";
import * as FCM from "fcm-push";
import { db } from './data-base/db';
import { oracledb } from 'oracledb';
enum TYPE {
    ORDER_CHANGED = 'ORDER_CHANGED',
    ORDER_DELETED = 'ORDER_DELETED',
    PAID = 'PAID',
    IMMEDIATE_REPAIR_START = 'IMMEDIATE_REPAIR_START',
    IMMEDIATE_REPAIR_END = 'IMMEDIATE_REPAIR_END',
    UPDATE_REQUIRED = 'UPDATE_REQUIRED',
    MENU_REFRESHED = 'MENU_REFRESHED',
    APP_LOGOUT = 'APP_LOGOUT',
    CAT_CANCEL_PAY = 'CAT_CANCEL_PAY'
}
export const nvl = (val, defaultVal = null) => {//strong nvl
    return (val && val != 'null' && val != 'NULL' && val != 'undefined')?val:defaultVal
};
export function orderPublish(storeNo:number, orderNo:number, type:string, deviceId:string){
    //fcm 관련 변수들
    let url = 'https://fcm.googleapis.com/fcm/send';
    let storeName = null;
    let topic = null;
    topic = `/topics/order_${storeNo}`; 

    let result = null;
    let fcm = new FCM(CP_SERVER_KEY);
    let message = null;
    message = {
        data: {
            type: type=='changed'?TYPE.ORDER_CHANGED:TYPE.ORDER_DELETED,
            orderNo: orderNo,
            deviceId
        },
        to: topic,
        priority: "high"
    };
    
    return fcm.send(message);
}

export function paidSucceed(storeNo:number, userNo:number, title:string, cashAmt:number, cardAmt:number, pointAmt:number, mileageAmt:number){
    //fcm 관련 변수들
    let url = 'https://fcm.googleapis.com/fcm/send';
    let storeName = null;
    let topic = null;
    topic = `/topics/u${userNo}`; 
    let pushText = ` ${cashAmt!=0?'현금 '+cashAmt+'원':''} 
                     ${cardAmt!=0?'카드 '+cardAmt+'원':''} 
                     ${pointAmt!=0?'포인트 '+pointAmt+'점':''} 
                     ${mileageAmt!=0?'마일리지 '+mileageAmt+'점':''} `;

    let result = null;
    let fcm = new FCM(PP_SERVER_KEY);
    let message = null;
    let get_storeName = `
        select store_name "storeName" from store_info where store_no = :storeNo `;
    return db.query(get_storeName,null,[storeNo])
    .then(name=>{
        storeName = name[0].storeName;
        message = {
            notification: {
                title: `[${storeName}] ${title}`,
                body: pushText,
                sound: "default",
                icon: "fcm_push_icon"
            },
            data: {
                title: `[${storeName}] ${title}`,
                body: pushText,
                type:TYPE.PAID
            },
            to: topic,
            priority: "high"
        };
        return fcm.send(message);
    })
    .catch(err=>{
        return Promise.reject('fcm error');
    });
    
}

export function doRelease(connection){
    connection.release(
        function(err) {
            if (err) {
                console.error(err.message);
            }
        });
}
export function inflating(data) {
    return data.rows;
}
export function oracleData(query, callback) {

    oracledb.getConnection(
        function(err, connection) {
            if (err) {
                callback(null);
            }else
            connection.execute(
                query,
                [],function (err, result) {
                    if (err) {
                        console.log(err.message);
                        doRelease(connection);
                        callback(null);
                    }
                    doRelease(connection);
                    callback(result);
                }
            );
        }
    );
}