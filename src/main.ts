if (process.env.NODE_ENV == "production")  
  var WhatapAgent = require('whatap').NodeAgent;
import * as moment from 'moment';

// let log = console.log;
// let error = console.error;

// console.log = function(arg){
//   log.apply(console, [moment().add(9,'hours').format('YYYY.MM.DD HH:mm:ss')].concat(`: ${arg}`));
// };

// console.error = function(arg) {
//   error.apply(console, [new Date().toDateString()].concat(`: ${arg}`));
// }

import * as http from 'http';
import * as https from 'https';
import * as fs from 'fs';
import * as express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import {graphqlExpress, graphiqlExpress} from 'graphql-server-express';
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import { db } from './data-base/db';
import { SubscriptionServer } from 'subscriptions-transport-ws';

//route
import { Schema, subscriptionManager, pubsub } from './schema';
import { GraphQLOptions } from "graphql-server-core/dist";
import { jwt } from "./jwt/index";

//graphql engine
import * as compression from 'compression';
import { Engine } from 'apollo-engine';

import { SSL, APOLLO_API_KEY } from './config';

// Default port or given one.
export const GRAPHQL_ROUTE = "/graphql";
export const GRAPHIQL_ROUTE = "/graphiql";
export const SUBSCRIPTIONS_PATH = '/subscriptions';

interface IMainOptions {
  enableGraphiql: boolean;
  env: string;
  port: number;
  ssl_port: number;
  verbose?: boolean;
}

/* istanbul ignore next: no need to test verbose print */
function verbosePrint(port, enableGraphiql) {
  console.log(`GraphQL Server is now running on http://localhost:${port}${GRAPHQL_ROUTE}`);
  if (true === enableGraphiql) {
    console.log(`GraphiQL Server is now running on http://localhost:${port}${GRAPHIQL_ROUTE}`);
  }
  console.log(`GraphQL Subscriptions server is now running on ws://localhost:${port}${SUBSCRIPTIONS_PATH}`);
}

class TestConnector {
  public get testString() {
    return "it works from connector as well!";
  }
}

function haltOnTimedout(req,res,next){
  if(!req.timedout) 
    next();
}
export function main(options: IMainOptions) {
  let unless = require('express-unless');
  let timeout = require('connect-timeout');
  let app = express();
  let to = timeout(15000);
  to.unless = unless;

  app.use(to.unless({ path: [ { url: '/subscriptions' } ] })); //30초안에 응답 없으면 response 종료
  app.use(haltOnTimedout);

  process.on('uncaughtException', err=>{
    //예상치 못한 예외 처리
    console.log('uncaughtException occurred! : '+err);
  });
  
  app.use(helmet({
    frameguard:false
  }));
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(morgan(options.env));
  app.use(cors());
  app.use((req:any,res,next)=>{
    console.log(`call from [${req.get('user-agent')}] `);
    console.log(`url from [${req.headers.origin}] `);
    next();
  })
  app.use('/',jwt,require('./routes/index'));
  
  let testConnector = new TestConnector();

  app.use(GRAPHQL_ROUTE, bodyParser.json(), graphqlExpress((req: any, res: any) => {
    // Get the query, the same way express-graphql does it
    // https://github.com/graphql/express-graphql/blob/3fa6e68582d6d933d37fa9e841da5d2aa39261cd/src/index.js#L257
    const query = req.query.query || req.body.query;
    if (query && query.length > 2000) {
      // None of our app's queries are this long
      // Probably indicates someone trying to send an overly expensive query
      throw new Error('Query too large.');
    }
    
    var options: GraphQLOptions =  {
      schema: Schema,
      context: {
        user: req.user,
        testConnector,
        db,
        pubsub,
        token: req.headers.authorization,
        tracing:true,
        cacheControl:true
      }
    }
    return options;
  }));
  if (true === options.enableGraphiql) {
    app.use(GRAPHIQL_ROUTE, graphiqlExpress({
      endpointURL: GRAPHQL_ROUTE
    }));
  }
  return new Promise((resolve, reject) => {
    app.set('port', options.port);
    var server = http.createServer(app);
    if(SSL){
      var ssl = https.createServer({
        key: fs.readFileSync('/home/ubuntu/ssl/_wildcard_handong_edu_SHA256WITHRSA.key'),
        cert: fs.readFileSync('/home/ubuntu/ssl/_wildcard_handong_edu.crt'),
        ca: fs.readFileSync('/home/ubuntu/ssl/ChainCA/rsa-dv.chain-bundle.pem')
      }, app);
      ssl.listen(options.ssl_port, ()=>{
        console.log("https handong");
      });
    }
    server.listen(options.port, () => {
      /* istanbul ignore if: no need to test verbose print */
      if (options.verbose) {
        verbosePrint(options.port, options.enableGraphiql);
      }

      const subscriptionsServer = new SubscriptionServer(
        {
          subscriptionManager,
          onConnect: (connectionParams, webSocket) => {
            console.log("connected");
          },
          onSubscribe: (message, params, webSocket) => {
            console.log("subscribe");
            return params;
          },
          onDisconnect: (webSocket) => {
            console.log("disconnected");
          },
          onUnsubscribe: (webSocket) => {
            console.log("unsubscribe");
          }
        },
        {
          path: SUBSCRIPTIONS_PATH,
          server,
        },
      );

      resolve({server});
    }).on("error", (err: Error) => {
      reject(err);
    });
  });

}

/* istanbul ignore if: main scope */
if (require.main === module) {
  const PORT = process.env.PORT || 3000;
  const SSL_PORT = process.env.SSL_PORT || 9444;

  // Either to export GraphiQL (Debug Interface) or not.
  const NODE_ENV = process.env.NODE_ENV !== "production" ? "dev" : "production";

  const EXPORT_GRAPHIQL = NODE_ENV !== "production";

  main({
    enableGraphiql: EXPORT_GRAPHIQL,
    env: NODE_ENV,
    port: PORT,
    ssl_port: SSL_PORT,
    verbose: true,
  });
}
