import { JWT_SECRET, JWT_ISSUER, JWT_AUDIENCE, SALT } from '../../../config';
/**
 * Created by sdh077 on 17. 3. 6.
 */

import * as crypto from 'crypto';
import { Router } from 'express';
import { db } from '../../../data-base/db';
let router = Router();
let oracledb = require('oracledb');
let jwt  = require("jsonwebtoken");
let lib = require('../../../lib');
let doRelease = lib.doRelease;
let oracleData = lib.oracleData;
let inflating = lib.inflating;

router.post('/token',(req, res) =>  {
    if(req.body.grant_type=='password')
        tokenPub(req,res);
    else
        refresh(req,res);
});
function tokenPub(req, res) {
    console.log('pcp login post')
    let grant_type = req.body.grant_type;
    let id = req.body.id;
    let pwd = req.body.pwd;
    console.log(typeof id);
    console.log(typeof pwd)
    let pwdSalted = pwd + SALT;
    console.log(id+","+pwd);
    console.log('sha: '+crypto.createHash('sha256').update(pwdSalted).digest('hex'))
    console.log('md5: '+crypto.createHash('md5').update(pwd).digest('hex'));
    if(grant_type == 'password'){
        let query =
            `select 
                store_no as "storeNo", user_no as "userNo" 
             from user_store 
             where 
                login_account = :id
                and (LOGIN_PASSWORD='${crypto.createHash('sha256').update(pwdSalted).digest('hex')}' 
                    or LOGIN_PASSWORD='${crypto.createHash('md5').update(pwd).digest('hex')}'
                    or LOGIN_PASSWORD='${pwd}')`;
        db.query(query,null,[id])
        .then(data=>{
            if(data.length == 0) {
              console.log(`login error: id = ${id}, pwd = ${pwd}`);
              res.status(404).send({state:"fail"})
              return;
            }else{
                let payLoad  = {'uid':data[0].storeNo,'no':data[0].userNo,userType:'store'};
                console.log(payLoad);
                let access_token = jwt.sign(payLoad,JWT_SECRET,{
                    issuer:JWT_ISSUER,
                    audience:JWT_AUDIENCE,
                    algorithm : 'HS256', //"HS256", "HS384", "HS512", "RS256", "RS384", "RS512" default SHA256
                    expiresIn : 3600//1000*10000 //expires in 24 hours
                })
                let refresh_token = jwt.sign(payLoad,JWT_SECRET,{
                    issuer:JWT_ISSUER,
                    audience:JWT_AUDIENCE,
                    algorithm : 'HS256', //"HS256", "HS384", "HS512", "RS256", "RS384", "RS512" default SHA256
                    expiresIn : 3600*24 //expires in 24 days
                })
                jwt.verify(access_token, JWT_SECRET, (err, decoded) => {
                    if(err || !decoded){
                        console.log(err);
                        res.sendStatus(401);
                    }else{
                        console.log(JSON.stringify(decoded));
                        res.json({
                            token_type:"JWT",
                            expires_in:decoded.exp*1000,
                            access_token:access_token,
                            refresh_token:refresh_token
                        });
                    }
                });
            }
        })
        .catch(err=>{
            res.status(500).send({state:"fail"});
        })
    } else {
        res.status(404).send({state:"fail"})
    }
}
function refresh(req, res) {

    let grant_type = req.body.grant_type;
    let refresh_token = req.body.refresh_token;
    console.log(refresh_token);
    if(refresh_token.substr(0,5)=='Bearer'){
        refresh_token = refresh_token.substr(7,refresh_token.length-7);
    }
    let client_id = (!req.body.client_id?'dp':req.body.client_id);
    let client_secret = (!req.body.client_secret?'dp':req.body.client_secret);
    let scope = (!req.body.scope?'all':req.body.scope);

    jwt.verify(refresh_token, JWT_SECRET, (err, decoded) => {
        if(err || !decoded){
            console.log(err);
            res.sendStatus(401);
            return;
        }
        if(grant_type == 'refresh_token'&&client_id == 'dp'&&client_secret == 'dp'&&scope=='all'){
            let payLoad  = {'uid':decoded.uid,'no':decoded.no,userType:decoded.userType};
            let access_token = jwt.sign(payLoad,JWT_SECRET,{
                issuer:JWT_ISSUER,
                audience:JWT_AUDIENCE,
                algorithm : 'HS256', //"HS256", "HS384", "HS512", "RS256", "RS384", "RS512" default SHA256
                expiresIn : 3600 //expires in 24 hours
            })
            let refresh_token = jwt.sign(payLoad,JWT_SECRET,{
                issuer:JWT_ISSUER,
                audience:JWT_AUDIENCE,
                algorithm : 'HS256', //"HS256", "HS384", "HS512", "RS256", "RS384", "RS512" default SHA256
                expiresIn : 3600*24 //expires in 24 hours
            })
            jwt.verify(access_token, JWT_SECRET, (err, decoded) => {
                if(err || !decoded){
                    console.log(err);
                    res.sendStatus(401);
                    return;
                }
                res.json({
                    token_type:"JWT",
                    expires_in:decoded.exp*1000,
                    access_token:access_token,
                    refresh_token:refresh_token
                })
            });
        } else {
            res.status(404).send({refresh:"refresh error"});
        }
    });
}

router.get('/info', (req, res)=>{
    let storeNo=req.user.uid;
    let userNo=req.user.no;
    let getUserInfo = `
        select 
            a.USER_NO "userNo", a.AREA_NO "areaNo",a.EMP_TYPE "empType",a.STORE_NO "storeNo",b.user_type "userType",c.area_name "areaName",
            (select sum(tran_point) from membership_point WHERE TRAN_TYPE in (1,2) and  STORE_NO = a.STORE_NO GROUP BY STORE_NO) as "point", 
            (select sum(CURRENT_CNT) from STAMP_PUBLISH WHERE STORE_NO = a.STORE_NO GROUP BY STORE_NO) as "stamp", 
            NVL(d.MILEAGE_SAVE_CASH,0) "mileageSaveCash", NVL(d.MILEAGE_SAVE_CARD,0) "mileageSaveCard", NVL(d.MILEAGE_STAMP_OPT,0) "mileageStampOpt" , 
            NVL(d.STAMP_SAVING_TYPE,0) "stampSavingType",NVL((select sum(MILEAGE_TOTAL) from user_mileage where store_no = d.store_no),0) "mileage", 
            d.POS_TYPE "posType", d.STORE_NAME as "storeName" 
        from
            user_store a ,user_master b,SERVICE_AREA c, store_info d 
        where 
            a.store_no= :storeNo  
            and a.user_no = :userNo  
            and b.user_no = a.user_no 
            and c.area_no = a.AREA_NO 
            and d.store_no = a.store_no`;
    let getStoreImage = `
        select 
            THUMBNAIL_URL_S as "thumbnail" 
         from STORE_IMAGE 
         where STORE_NO = :storeNo and ROWNUM <= 1 
         order by DISP_ORD asc`;

    let userInfo = null;
    db.query(getUserInfo, null, [storeNo, userNo])
    .then(user_info=>{
        userInfo = user_info[0];
        return db.query(getStoreImage, null, [storeNo]);
    })
    .then(images=>{
        userInfo["thumbnail"] = null;
        userInfo.thumbnail = (images.length != 0 ? images[0].thumbnail : null);
        res.status(200).send(userInfo);
    })
    .catch(err=>{
        res.status(500).send(err.toString());
    });
});
module.exports = router;