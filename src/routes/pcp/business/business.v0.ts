/**
 * Created by sdh077 on 17. 3. 8.
 */
let express = require('express');
let router = express.Router();
let oracledb = require('oracledb');
let db = require('../../../data-base/db').db;
let dateFormat = require('dateformat');
let lib = require('../../../lib');
let doRelease = lib.doRelease;
let oracleData = lib.oracleData;
let inflating = lib.inflating;

/**
 * 시제금과 영업 개시 날짜를 선택하고 영업을 개시한다. 영업 개시는 한 매장당 하루만 활성 되어있을 수 있다.
 */
router.post('/begin',(req, res) => {
    // error list
    // not closed : 이전에 마감 안되있는 게 있으면 에러
    // duplication
    // past 

    let openCash = req.body.openCash;
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let salesDt = req.body.salesDt;
    let salesDttm = salesDt;
    salesDttm += ' '+ dateFormat("isoTime"); //날짜에 시간붙이기


    //이전에 마감 안되있는 게 있으면 에러 "not closed"
    let check_before_closed = `
      select count(close_dttm) "cnt" from store_sales where store_no = ${storeNo} and close_dttm is null`;
    //1일전보다 더 과거일때 "past"에러 보내기
    let check_past = `
      select 1 "pastYN" from dual where to_char(sysdate-1,'yyyy-mm-dd hh24:mi:ss') <= '${salesDttm}'`;
    //이미 개시했는지 체크
    let check_dup = `
      select sales_dttm from store_sales where store_no = ${storeNo}`;

    let query = `
      insert into store_sales 
          (STORE_NO, SALES_DTTM, OPEN_CASH,TOTAL_CASH,TOTAL_CARD,TOTAL_POINT
          ,ISSUE_POINT,TOTAL_MILEAGE,ISSUE_MILEAGE,BALANCE_CASH,REG_DTTM,REG_USER_NO) 
      values
         (:STORE_NO,to_date(:openDt,'yyyy/mm/dd hh24:mi:ss'),:OPEN_CASH,:TOTAL_CASH,:TOTAL_CARD
         ,:TOTAL_POINT,:ISSUE_POINT,:TOTAL_MILEAGE,:ISSUE_MILEAGE,:BALANCE_CASH,sysdate,:REG_USER_NO)`;

    db.query(check_before_closed)
    .then(count=>{
      if(count[0].cnt == 0)
        return db.query(check_past);
      else
        throw "not closed";
    })
    .then(pastYN=>{
      if(pastYN.length != 0 && pastYN[0].pastYN == 1){
        return db.query(check_dup);
      }else{
        throw "past";
      }
    })
    .then(dttm=>{
      let dupYN = false;
      let date = null;
      for(let dt of dttm){
        date = dateFormat(dt.SALES_DTTM,'yyyy-mm-dd');
        if(date == salesDt){
          dupYN = true;
          break;
        }
      }
      if(dupYN)
        throw "duplication";
      else{
        return db.query(query,null,[storeNo,salesDttm,openCash,0,0,0,0,0,0,openCash,userNo]);
      }
    })
    .then(result=>{
      res.sendStatus(201);
    })
    .catch(err=>{
      console.log(err);
      if(err.message != undefined && err.message.includes("ORA-00001")){
        res.status(400).send({message:"duplication"});
      } else if(err == "past"){
        res.status(400).send({message:"past"});
      } else if(err == "duplication"){
        res.status(400).send({message:"duplication"});
      }else if(err == "didn't closed"){
        res.status(400).send({message:"not closed"});
      }else{
        res.status(500).send(err.toString());
      }
    });
});

/**
 * 마감된 현금 매출 금액, 카드 매출 금액, 포인트 결제 금액, 
 * 포인트 발행 금액, 마감 현금 잔액, 시제금을 모두 보여준다
 */
router.get('/end',(req, res) => {
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let pageNo = req.query.pageNo;
    let pageSize = req.query.pageSize;
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let endYn = req.query.endYn | 0; // 0 : 마감x 1: 마감o
    let get_sale_info = `
      select x.* 
      from 
          (SELECT 
              CEIL(ROWNUM/${pageSize}) AS "page", y.* 
          FROM 
              (select 
                  to_char(SALES_DTTM,'yyyy-mm-dd hh24:mi:ss') as "salesDttm",
                  total_cash as "totalCash", 
                  total_card as "totalCard",
                  total_point as "totalPoint",
                  balance_Cash as "balanceCash",
                  TOTAL_MILEAGE as "totalMileage",
                  ISSUE_MILEAGE as "issueMileage",
                  to_char(CLOSE_DTTM,'yyyy-mm-dd hh24:mi:ss') as "closeDttm",
                  open_cash as "openCash" 
              from store_sales 
              where to_char(SALES_DTTM,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
                    and store_no = ${storeNo} `
      if(endYn == 0){                    
        get_sale_info += `
                    and CLOSE_DTTM IS NOT NULL`; 
      }else{
        get_sale_info += `
                    and CLOSE_DTTM IS NULL`; 
      }
        get_sale_info += `
              order by SALES_DTTM desc, CLOSE_DTTM desc 
              ) y 
          ) x 
      where x."page" = ${pageNo} `;
    
    let get_total = `
      select count(*) "total" 
      from store_sales 
      where to_char(SALES_DTTM,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
            and store_no = ${storeNo} `;
      if(endYn == 0){                    
        get_total += `
            and close_dttm IS NOT NULL `; 
      }else{
        get_total += `
            and close_dttm IS NULL `; 
      }
      
    let saleInfos = null;
    db.query(get_sale_info)
    .then(saleInfo=>{
      saleInfos = saleInfo;
      return db.query(get_total);
    })
    .then(total=>{
      res.status(200).send({
        list:saleInfos,
        total:total[0].total
      });
    })
    .catch(err=>{
      res.status(500).send(err.toString());
    });
});
/**
 * 해당 매장의 영업을 마감한다.
 */
router.post('/end',(req,res)=>{
    // error list
    // not enough params
    
    let salesDttm = req.body.salesDttm;
    let storeNo=req.user.uid;
    let userNo=req.user.no;

    if(!salesDttm){
      res.status(400).send({message:'not enough params'});
      return;
    }
 
    let get_order_status = `
        select count(*) "cnt" 
        from order_master 
        where 
            store_no = ${storeNo} 
            and upd_dttm <= sysdate
            and order_status = '1'`;
    let upd_close =`
        UPDATE store_sales 
        set 
            CLOSE_DTTM = sysdate 
        where STORE_NO =${storeNo} 
            and REG_USER_NO =${userNo} 
            and to_char(SALES_DTTM,'yyyy-mm-dd hh24:mi:ss') = '${salesDttm}' `;

    db.query(get_order_status)
    .then(count=>{
      //마감은 주문 남아있어도 마감 가능!!!!
      //마감 되있으면 주문 및 결제 안됨!!
      return db.query(upd_close);
    })
    .then(result=>{
      console.log(result);
      if(result.rowsAffected < 1){
        throw "not opened";
      }
      res.sendStatus(201);
    })    
    .catch(err=>{
      if(err == 'order left'){
        res.status(400).send({message:err});
      }else{
        res.status(500).send(err.toString());
      }
    });
});

/**
 * balance cash 수정
 */
router.put('/end',(req, res) => {
    // error list
    // not enough params

    let storeNo = req.user.uid;
    let salesDttm = req.body.salesDttm;
    let balanceCash = req.body.balanceCash;

    if(!salesDttm || !balanceCash){
      res.status(400).send({message:'not enough params'});
      return;
    }

    let upd_balanceCash = `
      update STORE_SALES set BALANCE_CASH = ${balanceCash} 
      where 
        STORE_NO = ${storeNo} 
        and to_char(SALES_DTTM,'yyyy-mm-dd hh24:mi:ss') = '${salesDttm}' `;
    db.query(upd_balanceCash)
    .then(result=>{
      res.status(201).send("update success");
    })
    .catch(err=>{
      res.status(500).send(err.toString());
    });
});

/**
 * 가장 최근의 마감을 취소한다.
 */
router.delete('/end',(req, res) => {

  let storeNo = req.user.uid;
  let userNo = req.user.no;
  let check_end = `
    select * 
    from (
      select close_dttm 
      from store_sales 
      where 
        store_no=${storeNo} 
      order by sales_dttm desc
    )
    where rownum <= 1`;
  let dlt_end =`
      update store_sales 
      set close_dttm = NULL 
      where 
          sales_dttm =(select max(sales_dttm) from store_sales where store_no = ${storeNo}) and store_no=${storeNo}`;
  db.query(check_end)
  .then(dttm=>{
    if(dttm.length == 0){
      throw "not opened";
    }else if(!dttm[0].CLOSE_DTTM){
      throw "not closed";
    }else{
      return db.query(dlt_end);
    }
  })
  .then(result=>{
    res.status(201).send("delete success");
  })
  .catch(err=>{
    if(err == 'not opened'){
      res.status(404).send({message:"개시 내역이 없습니다."});
    }else if(err == 'not closed'){
      res.status(400).send({message:"마감되어있지 않습니다."});
    }else{
      res.status(500).send(err.toString());
    }
  });
});

/**
 * 가장 최근 영업 개시한 정보의 영업 일자 
 */
router.get('/end/no',(req, res) => {
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let get_lastest =`
      select 
          to_char(SALES_DTTM,'yyyy-mm-dd hh24:mi:ss') "salesDttm" 
      from store_sales 
      where 
          CLOSE_DTTM is null 
          and store_no = ${storeNo} 
          and REG_USER_NO = ${userNo} 
      order by sales_dttm desc `;

    db.query(get_lastest)
    .then(dttm=>{
      if(!dttm.length)
        throw "not opened";
      else
        res.status(200).send(dttm[0].salesDttm);
    })
    .catch(err=>{
      if(err=="not opened")
        res.status(404).send({message:"오픈한 매장이 없습니다."});
      else
        res.status(500).send(err.toString());
    });
});

module.exports = router;