/**
 * Created by sdh077 on 17. 3. 15.
 */
import * as express from 'express';
let router = express.Router();

let db = require('../../../../data-base/db').db;

let jwt  = require("jsonwebtoken");

/**
 * orderStatus를 통해 취소, 주문, 결제, 전체 주문 내역을 선택적으로 가져온다
 */
router.get('/',(req, res) => {
    console.log('get order!!!!!!!!');
    let storeNo = req.user.uid;
    let orderNo = (req.query.orderNo == undefined ? -1 : req.query.orderNo);
    let orderStatus = (req.query.orderStatus == undefined ? '3' : req.query.orderStatus);
    let query =
    `select 
        b.order_no as "orderNo",
        b.ORDER_NAME as "orderName" ,
        b.order_dttm as "orderDttm",
        b.ORDER_AMT as "orderAmt",
        b.ORDER_NAME as "orderName" ,
        b.ORDER_TYPE as "orderType",
        b.PAY_DTTM as "payDttm",
        b.WAITING_NO as "watingNo" ,
        b.ORDER_STATUS as "orderStatus",
        b.REG_USER_NO as "regUserNo",
        b.UPD_DTTM as "updDttm",
        b.STORE_NO as "storeNo" ,
        b.BOOKING_NO as "bookingNo",
        b.table_no_array as "tables",
        b.floor_no as "floorNo",
        b.dc_name as "dcName",
        b.pay_total as "payTotal",
        b.credit_tran_yn as "creditTranYn",
        b.credit_title as "credit_title",
        b.tax_total as "taxTotal"  
     from 
        order_master b 
     where b.store_no=${storeNo} `;

    if(orderStatus !='3'){
        query += ` 
            and b.order_status = '${orderStatus}' `;
    }
    if(orderNo != -1){
        query += ` 
            and b.order_no = ${orderNo} `;
    // query += ` 
    //     ) B left outer join 
    //         (select 
    //             a.order_no,
    //             LISTAGG (a.MENU_NAME, '!@#') WITHIN GROUP (ORDER BY a.MENU_NO)  AS MENU_NAME, 
    //             LISTAGG (a.ITEM_CNT, '!@#') WITHIN GROUP (ORDER BY a.MENU_NO)  AS ITEM_CNT 
    //         from order_detail a group by a.order_no) A
    //      on B.order_no = A.order_no `;
    
    }
    db.query(query,null,[])
    .then(orderInfo=>{
        res.status(200).send(orderInfo);
    })
    .catch(err=>{
        res.status(500).send(err.toString());
    });
});

/**
 * 주문의 이름, 타입, 대기번호, 상태를 수정한다.
 */
router.put('/master/:orderNo',(req, res) => {
    let orderNo =req.params.orderNo;
    let orderName =req.body.orderName;
    let orderType =req.body.orderType;
    let waitingNo =req.body.waitingNo;
    let orderStatus =req.body.orderStatus;
    let tables =req.body.tables;
    let floorNo = req.body.floorNo;
    let upd_order_master = `
        update order_master 
        set
            ORDER_NAME = :orderName, 
            ORDER_TYPE = :orderType,
            WAITING_NO = :waitingNo, 
            ORDER_STATUS = :orderStatus, 
            TABLE_NO_ARRAY = :tables,
            FLOOR_NO = :floorNo  
        where 
            order_no = :orderNo `;
    db.query(upd_order_master,null,[orderName,orderType,waitingNo,orderStatus,tables, floorNo, orderNo])
    .then(upd_result=>{
        res.sendStatus(201);
    })
    .catch(err=>{
        res.status(500).send(err.toString());
    });
});

/**
 * 주문 내역 하나를 선택하여 메뉴 정보 하나 하나를 모두 본다
 */
router.get('/detail',(req, res) => {
    console.log(req.query)
    let orderNo = req.query.orderNo;

    let get_order_detail = `
        SELECT y.* 
        FROM 
            (select 
                order_no as "orderNo",
                MENU_NO as "menuNo",
                MENU_NAME as "menuName",
                ITEM_AMT as "itemAmt",
                ITEM_CNT as "itemCNT",
                DC_NAME as "dcName",
                DC_AMT as "dcAmt",
                ORDER_MEMO as "orderMemo"
            from order_detail 
            where ORDER_NO=:orderNo 
            ) y `;
    db.query(get_order_detail,null,[orderNo])
    .then(details =>{
        res.status(200).send({
            list:(details?details:[])
        });
    })
    .catch(err=>{
        res.status(500).send(err.toString());
    });
});
module.exports = router;