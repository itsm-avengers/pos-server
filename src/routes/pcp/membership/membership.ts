import * as express from "express";
import * as oracledb from "oracledb";
import * as FCM from "fcm-push";
import { db } from "../../../data-base/db";
import { PP_SERVER_KEY } from "../../../config";
let router = express.Router();

/**
 * 모든 공지사항을 가져온다
 */
router.get('/notice',(req,res)=>{
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let areaNo = (req.query.areaNo ? req.query.areaNo : -1);
    let search = (req.query.search ? req.query.search : '');
    if(!pageSize || !pageNo){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let get_notice = `
        select x.* 
        from (
            select CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            from (
                select 
                    notice_no "noticeNo", notice_title "noticeTitle", 
                    notice_text "noticeText", to_char(reg_dttm,'yyyy-mm-dd') "regDttm" 
                from notice 
                where 1=1 
                    and (notice_title like '%${search}%' or notice_text like '%${search}%')`;
    if(areaNo != -1){
        get_notice += `
                    and area_no = ${areaNo} `;
    }
        get_notice += `         
            ) y
        ) x
        where x."page" = ${pageNo} `;
    
    let get_total = `   
        select 
            count(*) "total"  
        from notice 
        where 1=1 
            and (notice_title like '%${search}%' or notice_text like '%${search}%')`;
    if(areaNo != -1){
        get_total += `
            and area_no = ${areaNo} `;
    }
    let notices = null;
    db.query(get_notice)
    .then(notice=>{
        notices = notice;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:notices,
            total:total[0].total
        });
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 공지사항 읽음 여부를 수정한다.
 */
router.post('/notice/read',(req:any,res)=>{
    let noticeNo = req.body.noticeNo;
    let userNo = req.user.no;
    if(!noticeNo){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let check_read = `
        select count(*) "cnt" from notice_read where notice_no = ${noticeNo} and user_no = ${userNo} `;
    let insert_read = `
        insert into notice_read 
            (notice_no, user_no, read_dttm) 
        values 
            (${noticeNo}, ${userNo}, sysdate) `;
    db.query(check_read)
    .then(cnt=>{
        if(cnt[0].cnt > 0){
            throw "exist";
        }else{
            return db.query(insert_read);
        }
    })
    .then(insert_result=>{
        res.status(201).send("insert read success");
    })
    .catch(err=>{
        if(err == "exist"){
            res.status(400).send({message:"이미 읽었습니다."});
        }else{
            res.status(500).send({message:err.toString()});
        }
    });
});

/**
 * 포항포인트 포스의 faq를 가지고 온다. 
 */
router.get('/faq',(req,res)=>{
    res.sendStatus(200);
});

/**
 * 포항포인트 포스의 약관내용을 가지고 온다. 
 */
router.get('/term',(req,res)=>{
    res.sendStatus(200);
});

/**
 * 해당 매장에서 포항포인트 사용자에게 푸시를 보낸 내역을 가지고 온다
 */
router.get('/push',(req:any,res)=>{
    console.log('pos push ');
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let searchTxt = req.query.searchTxt;
    if(!pageSize || !pageNo || !startDt || !endDt){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let get_push = `
        select x.* 
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            from (
                select 
                    a.push_no as "pushNo",
                    a.user_no as "userNo",
                    a.sender as "sender",
                    a.push_title as "pushTitle",
                    a.push_send_type as "pushType",
                    (select count(push_no) from push_receiver where push_no = a.push_no) as "receive",
                    REGEXP_REPLACE(a.push_text, '<[^>]*>|\&([^;])*;', '')  as "pushTxt2",
                    a.push_text  as "pushTxt",
                    to_char(a.send_dttm,'yyyy-mm-dd hh24:mi') as "sendDttm",
                    a.read_cnt as "readCnt",
                    f_get_codename('400',a.push_result) as "pushResult"
                    from 
                        push_message a, user_master b, user_store c
                    where 1=1
                        and a.user_no = b.user_no
                        and b.user_no = c.user_no
                        and c.store_no = ${storeNo}
                        and to_char(a.send_dttm,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
                        and (a.push_text like '%${searchTxt}%' or a.push_title like '%${searchTxt}%')
                    order by a.send_dttm desc 
                )y
            )x
        where x."page" = ${pageNo}`;
    
    var get_total = `
        select count(a.push_no) as "total"
        from 
            push_message a, user_master b, user_store c
        where 1=1
            and a.user_no = b.user_no
            and b.user_no = c.user_no
            and c.store_no = ${storeNo} 
            and to_char(a.send_dttm,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
            and (a.push_text like '%${searchTxt}%' or a.push_title like '%${searchTxt}%') `;    
    let pushes = null;
    db.query(get_push)
    .then(push=>{
        pushes = push;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:pushes,
            total: total[0].total
        });
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 포항포인트 사용자에게 푸시를 보낸다.
 */
router.post('/push',(req:any, res)=>{
    console.log('push post');
    let storeNo = req.user.uid;
    let userNo=req.user.no;
    let title = req.body.pushTitle;
    let pushText = req.body.pushTxt;
    let publicParm = req.body.publicParm;
    let sendType = req.body.sendType;
    let saveYN = req.body.saveYN | 0;

    //fcm 관련 변수들
    let url = 'https://fcm.googleapis.com/fcm/send';
    let storeName = null;
    let serverKey = PP_SERVER_KEY;
    let topic = null;
    let dangol = null;
    if(sendType == '1'){
        topic = "/topics/all"; //포항 포인트  모든 사용자
        dangol = false;
    }else{
        topic = `/topics/${storeNo}`; //해당 매장의 단골로 등록된 사용자
        dangol = true;
    }
    

    let push_proc = `
        BEGIN PROC_PUSH_SEND_01(
            :i_userNo,
            :i_sender,
            :i_title,
            :i_pushText,
            :i_publicParm,
            :i_sendType,
            :o_result
        ); END; `;

    let result = null;
    let get_storeName = `
        select store_name "storeName" from store_info where store_no = ${storeNo}`;
    db.query(get_storeName)
    .then(name=>{
        storeName = name[0].storeName;
        let fcm = new FCM(serverKey);
        let message = null;
        if(saveYN == 1){
            message = {
                notification: {
                    title: `[${storeName}]`,
                    body: title,
                    sound: "default",
                    click_action: "FCM_PLUGIN_ACTIVITY", //필
                    icon: "fcm_push_icon"
                },
                data: {
                    title: `[${storeName}]`,
                    body: title,
                    content_available: "true",
                    isDangol: dangol // 이거 왜 있는지는 잘 모르겠음 ... 
                },
                to: topic,
                priority: "high"
            };
        }else{
            message = {
                notification: {
                    title: `[${storeName}]`,
                    body: title,
                    sound: "default",
                    icon: "fcm_push_icon"
                },
                data: {
                    title: `[${storeName}]`,
                    body: title,
                    content_available: "true",
                    isDangol: dangol // 이거 왜 있는지는 잘 모르겠음 ... 
                },
                to: topic,
                priority: "high"
            };
        }
        

        return fcm.send(message);
    })
    .then((res)=>{
        console.log(res)
        if(saveYN == 1){
            return db.query(push_proc,null,[userNo,storeName,title,pushText,publicParm,sendType,
                                            {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 }]);
        }else{
            return [];
        }
    })
    .then((proc_result:any)=>{
        if(saveYN != 1){
            res.status(200).send("push send success(not saved)");
        }else{
            result = null;
            if(proc_result && proc_result.outBinds)
                result = proc_result.outBinds;            
            if(result == "OK" ){
                res.status(201).send("push send success");
            }else{
                throw "proc error";
            }
        } 
    })
    .catch(err=>{
        if(err == "proc error"){
            res.status(400).send({message:result});
        }else{
            res.status(500).send({message:err.toString()});
        }
    });
});
  

/**
 * 매장의 스탬프 관리 항목 추가 및 수정
 */
router.post('/stampSet',(req, res)=>{
    let storeNo = req.user.uid;
    let stampSavingAmt = req.body.stampSavingAmt;
    let stampText = req.body.stampText;
    
    let merge = `
        merge into STORE_STAMP_SET using dual on (store_no = ${storeNo}) 
        when matched then 
            update set 
                stamp_saving_amt = :stampSavingAmt,
                saving_text = :stampText 
        when not matched then 
            insert (store_no, stamp_saving_amt, saving_text) 
            values (${storeNo}, :stampSavingAmt, :stampText) `;
    db.query(merge,null,[stampSavingAmt,stampText,stampSavingAmt,stampText])
    .then(result=>{
        res.status(201).send("merge success");
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});


/**
 * 매장의 스탬프 관리 항목 새로 추가 및 수정
 * modified on 17.11.14 by mj
 */
router.post('/stampAttr', (req, res)=>{
    
    console.log('pos stampAttr post')
    let storeNo = req.user.uid;
    let stampUnit = req.body.stampUnit;
    let exchangeText = req.body.exchangeText;

    if(!stampUnit){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let check_exist = `select count(1) "cnt" from store_stamp_attr where store_no = ${storeNo} and stamp_unit = :stampUnit`;

    let merge = `
        insert into store_stamp_attr 
            (store_no, stamp_unit, exchange_text) 
        values 
            (${storeNo}, :stampUnit, :exchangeText) `;
    db.query(check_exist,null,[stampUnit])
    .then(cnt=>{
        if(cnt[0].cnt == 1){
            throw "duplicated";
        }else{
            return db.query(merge,null,[stampUnit,exchangeText]);
        }
    })
    .then(merge_result=>{
        res.status(201).send("insert success");
    })
    .catch(err=>{
        if(err == "duplicated"){
            res.status(400).send({message:err})
        }else{
            res.status(500).send({message:err.toString()});    
        }
        
    });
});

/**
 * 매장의 스탬프 관리 항목 수정
 * added on 17.11.14 by mj
 */
router.put('/stampAttr',(req, res) => {
    
    console.log('pos stampAttr post')
    let storeNo = req.user.uid;
    let stampUnit = req.body.stampUnit;
    let exchangeText = req.body.exchangeText;

    if(!stampUnit){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let check = `
        select count(*) "cnt" from store_stamp_attr where store_no = :storeNo and stamp_unit = :stampUnit`;
    let merge = `
        update store_stamp_attr set 
            exchange_text = :exchangeText 
        where store_no = :storeNo and stamp_unit = :stampUnit `;
    db.query(check,null,[storeNo,stampUnit])
    .then(cnt=>{
        if(cnt[0].cnt == 0){
            throw "empty";
        }else{
            return db.query(merge,null,[exchangeText, storeNo, stampUnit]);
        }
    })
    .then(merge_result=>{
        res.status(201).send("update success");
    })
    .catch(err=>{
        if(err == 'empty'){
            res.status(400).send({message:err});
        }else{
            res.status(500).send({message:err.toString()});
        }
    });
});

/**
 * 매장의 스탬프 관리 항목 삭제
 */
router.delete('/stampAttr', (req, res) => {

    let storeNo = req.user.uid;
    let stampUnit = req.query.stampUnit;

    let dlt_query = `
        delete from store_stamp_attr 
        where store_no = ${storeNo} and stamp_unit = ${stampUnit}`;
    db.query(dlt_query)
    .then(result=>{
        res.status(201).send("delete success");
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 매장의 스탬프 관리, 속성 항목 가져오기
 */
router.get('/stampAll', (req, res)=>{
    
    let storeNo = req.user.uid;

    let get_stampAttr = `
        select 
            stamp_unit as "stampUnit", 
            exchange_text as "exchangeText" 
        from store_stamp_attr 
        where store_no = ${storeNo} 
        order by stamp_unit asc `;
    let get_stampSet = `
        SELECT 
            a.STAMP_SAVING_AMT as "stampSavingAmt", 
            a.SAVING_TEXT as "stampText",
            b.point_saving_ratio as "pointRatio" 
        FROM STORE_STAMP_SET a, store_info b 
        WHERE 
            b.store_no = a.store_no 
            and a.STORE_NO = ${storeNo} `;
    let attrList = null;
    let setInfo = null;
    db.query(get_stampAttr)
    .then(attr_result=>{
        attrList = attr_result;
        return db.query(get_stampSet);
    })
    .then(set_result=>{
        setInfo = set_result[0];
        res.status(200).send({
            stampSavingAmt:setInfo.stampSavingAmt,
            stampText:setInfo.stampText,
            pointRatio:setInfo.pointRatio, 
            attr:attrList
        });
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});
module.exports = router;