import * as express from 'express';
let router = express.Router();
let jwt  = require("jsonwebtoken");
import { db } from '../../../../data-base/db';

/**
 * 기간내에 카드 이용 내역을 조회한다. 
 * 이때 카드사명을 주어 해당 카드사의 결제 내역을 가져온다
 */
router.get('/',(req, res) => {
    // error list
    // not enough params

    let storeNo = req.user.uid;
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let company = (req.query.company == undefined ? null : req.query.company);
    
    if(!startDt || !endDt || !pageSize || !pageNo){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let get_cardApproval =`
        select x.*
        from (
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            FROM (
                select 
                    APPROVAL_KEY "approvalKey", CARD_ID "cardID", CARD_COMPANY "cardCompany", 
                    to_char(APPROVAL_DTTM,'yyyy-mm-dd hh24:mi:ss') "approvalDttm", 
                    APPROVAL_AMT "approvalAmt", INSTALLMENT_MM "installmentMm" 
                from card_approval a, order_pay b, order_master c
                where 
                    to_char(APPROVAL_DTTM,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
                    and c.store_no = ${storeNo} 
                    and a.pay_no = b.pay_no 
                    and b.order_no = c.order_no `;
    if(company == null){
        get_cardApproval += `    
                    and a.card_company is null 
                ) y 
            ) x 
        where x."page" = ${pageNo} `;
    }else if(company != '-1'){
        get_cardApproval += `
                    and a.card_company = ${company} 
                ) y
            ) x
        where x."page" = ${pageNo} `;
    }else{
        get_cardApproval += `
                ) y 
            ) x 
        where x."page" = ${pageNo} `;
    }

    let get_total = `
        select count(*) "cnt"  
        from card_approval a, order_pay b, order_master c
        where 
            to_char(APPROVAL_DTTM,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
            and c.store_no = ${storeNo} 
            and a.pay_no = b.pay_no 
            and b.order_no = c.order_no `;
    if(company == null){
        get_total += `
            and a.card_company is null `;
    }else if(company != '-1'){
        get_total += `
            and a.card_company = ${company} `;
    }
    let cardApprovalInfo = null;
    db.query(get_cardApproval)
    .then(cardApproval=>{
        cardApprovalInfo = cardApproval;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:cardApprovalInfo,
            total:total[0].cnt
        });
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 카드결제를 했을때 사용한 카드사 목록을 가져온다
 */
router.get('/company',(req, res) => {
    let storeNo = req.user.uid;
    let get_company =`
        select CARD_COMPANY "cardCampany" 
        from card_approval a, order_pay b, order_master c 
        where 
            c.store_no=${storeNo} 
            and a.pay_no = b.pay_no 
            and b.order_no = c.order_no 
            group by CARD_COMPANY `;
    db.query(get_company)
    .then(companies=>{
        res.status(200).send({list:companies});
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

module.exports = router;