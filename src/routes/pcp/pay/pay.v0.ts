/**
 * Created by sdh077 on 17. 3. 20.
 */
import * as express from 'express';
let router = express.Router();

let jwt  = require("jsonwebtoken");
let db = require('../../../data-base/db').db;
let pubsub = require('../../../schema/index').pubsub;

let oracledb = require('oracledb');
let lib = require('../../../lib');
let doRelease = lib.doRelease;
let oracleData = lib.oracleData;
let inflating = lib.inflating;
let paidSucceed = lib.paidSucceed;
let orderPublish = lib.orderPublish;

import { nvl } from '../function';
let iconv = require('iconv-lite');
/**
 * test
 */
// router.get('/abc',(req,res)=>{
//     let a = iconv.encode("머시여","euc-kr");
//     console.log(a);
//     let b = iconv.decode(new Buffer(a),'euc-kr')
//     console.log(b);
//     res.sendStatus(201);
// })
/**
 * 매장의 기간 내의 결제 내역
 */
router.get('/',(req, res) => {
    let storeNo = req.user.uid;
    let orderNo = req.query.orderNo;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;

    let get_pay = `
        select x.* 
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            FROM (  
                select 
                    a.PAY_NO as "payNo",  
                    a.PAY_DTTM as "payDttm",
                    a.PAY_AMT as "payAmt",
                    a.PAY_TAX as "payTax", 
                    a.PAY_TOTAL as "payTotal",
                    a.CASH_AMT as "cashAmt",
                    a.CARD_AMT as "cardAmt",
                    a.POINT_AMT as "pointAmt",
                    a.MILEAGE_AMT as "mileageAmt",
                    a.CASH_PRINT as "cashPrint",
                    a.ORDER_NO as "orderNo",
                    a.MILEAGE_NO as "mileageNo" 
                from order_pay  a, order_master b   
                where a.order_no = b.order_no and b.store_no = ${storeNo} `;
    if (orderNo != -1) {
        get_pay += ` and a.order_no=${orderNo} `; 
    }
    get_pay += `
                ) y 
            ) x WHERE x."page" = ${pageNo} `;
            
    let get_total = `
        select 
            count(a.PAY_NO) as "total" 
        from order_pay  a, order_master b 
        where a.order_no = b.order_no and b.store_no = ${storeNo}`;
    if (orderNo != -1) {
        get_total += ` and a.order_no=${orderNo} `;
    }
    let pay_info = null
    let connection = null;
    let total = null;
    db.getConnection()
    .then(conn=>{
        connection = conn;
        return db.query(get_pay,connection);
    })
    .then(pay_list=>{
        pay_info = pay_list;
        return db.query(get_total,connection);
    })
    .then(total_result=>{
        total = total_result[0].total;
        return connection.commit();    
    })
    .then(out=>{
        connection.close().then();
        res.status(200).send({
            list : pay_info,
            total: total
        });
    })
    .catch(err=>{
        res.status(500).send(err.toString());
    });
});

/**
 * 해당 주문의 결제 내역을 삭제한다
 */
router.delete('/',(req, res) => {
    // error list
    // parameter err : payNo or orderNo undefined
    let storeNo=req.user.uid;
    let payNo = req.query.payNo;

    let cardId = nvl(req.body.cardID);
    let cardType = null;//nvl(req.body.cardType);
    let cardName = null;//nvl(req.body.cardName);
    let cardCompany = null;
    let installmentMm = null;//nvl(req.body.installmentMm); //할부 개월수
    let cardYymm = null;//nvl(req.body.cardYymm);
    // let approvalDttm = nvl(req.body.approvalDttm);
    let approvalKey = nvl(req.body.approvalKey);
    let terminalId = nvl(req.body.terminalID);
    let tranId = req.body.tranID | 0;

    let get_remain = `
        select (sum(a.PAY_TOTAL)/count(*) - sum(b.PAY_TOTAL)) "remain" 
        from ORDER_MASTER a, ORDER_PAY b 
        where 
            a.ORDER_NO = (select ORDER_NO from ORDER_PAY where PAY_NO = ${payNo})  
            and b.PAY_NO in (select PAY_NO from ORDER_PAY where ORDER_NO = a.ORDER_NO) `;
    let cancel_proc = `
        BEGIN PROC_CANCEL_PAY(
            :i_pay_no, :i_store_no, :o_result 
        );END;`;
    let result = null;
    db.query(cancel_proc,null,[payNo, storeNo,
                            {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 1000 }])
    .then(proc_result=>{
        result = proc_result.outBinds;
        if(result == "OK"){
            return db.query(get_remain);
        }else{
            throw "proc error";
        }
    })
    .then(remain=>{
        pubsub.publish("payDeleted", { storeNo: storeNo });
        res.status(201).send(remain);
    })
    .catch(err=>{
        if(err == "proc error"){
            res.status(400).send({message:result});
        }else{
            res.status(500).send({message:err.toString()});
        }
    });
});

/**
 * 카드, 현금, 포인트, 마일리지 단일 or 복합 결제한다.
 * added on 17.09.14 by mjLee
 */
router.post('/',async (req,res)=>{
    // error list
    // not enough params
    // not equal payTotal and amt
    // proc error
    console.log('===결제내역 저장===');
    let cardCompany = null;//req.body.cardCompany;
    let payAmt = 0;
    let payTax = 0;
    let payTotal = req.body.payTotal;
    let cashAmt = req.body.cashAmt | 0;
    let cardAmt = req.body.cardAmt | 0;
    let mileageAmt = req.body.mileageAmt | 0;
    let pointAmt = req.body.pointAmt | 0;
    let cashPrint = nvl(req.body.cashPrint);
    let orderNo = req.body.orderNo;
    let storeNo = req.user.uid;
    let storeUserNo = req.user.no; // 매장 주인 유저넘버
    let cardId = nvl(req.body.cardID);
    let cardType = null;//nvl(req.body.cardType);
    let cardName = nvl(req.body.cardName);
    // let cardCompany = !cardData?null:cardData;
    let installmentMm = null;//nvl(req.body.installmentMm); //할부 
    let cardYymm = null;//nvl(req.body.cardYymm);
    let approvalDttm = nvl(req.body.approvalDttm);
    approvalDttm = parse(approvalDttm);
    let approvalKey = nvl(req.body.approvalKey);
    let terminalId = nvl(req.body.terminalID);
    let tranId = req.body.tranID | 0;
    let barcode = nvl(req.body.barcode);
    let userNo = nvl(req.body.userNo); // 매장 회원의 유저 번호(토큰 유저넘버 아님)
    let curPoint = nvl(req.body.curPoint);
    let mileageTranType = '2';
    let deviceId = req.body.deviceId;

    //선불결제의 경우 주문 추가와 함께 결제
    let posType = req.body.posType?req.body.posType : 2;//1: 선불 2: 후불
    // + addOrder 변수들
    let input = req.body.input;
    let details = req.body.details;
    if(input && input.orderNo)
        delete input.orderNo;
    console.log(JSON.stringify(req.body));
    if((posType == '2' && !orderNo) || payTotal == undefined || (cardAmt != 0 && !approvalKey) || (userNo && !barcode)){
        res.status(400).send({message:"not enough params"});
        return;
    }
    if(payTotal != (cardAmt+cashAmt+mileageAmt+pointAmt)){
        res.status(400).send({message:"not equal payTotal and amt"});
        return;
    }

    // 계산금액을 11로 나눈 후 소수 1째자리에서 반올림
    let after_dc = payTotal - (mileageAmt + pointAmt);
    payTax = (after_dc / 11) * 10;
    payTax = Math.round((Math.floor(payTax) / 10));
    payAmt = payTotal - payTax;
    let pay_proc = `
        BEGIN PROC_TOTAL_PAY(
            :i_pay_tax, :i_pay_amt, :i_pay_total, :i_cash_amt, :i_card_amt, :i_cash_print,
            :i_order_no, :i_store_no, :i_user_no, :i_card_id, :i_card_type,
            :i_card_name, :i_card_company, :i_installment_mm, :i_card_yymm,
            :i_approval_dttm, :i_approval_key, :i_terminal_id, :i_tran_id, :i_barcode, :i_cur_point,
            :i_point_amt, :i_mileage_tran_type, :i_mileage_amt, :o_result
        ); END;`;
    let proc_error = null;

    let endCheck = await db.query(`select count(*) "cnt" from store_sales where store_no = :storeNo and close_dttm is null`, null, [storeNo]);
    if(endCheck[0].cnt == 0){
        console.error("마감 후 결제 에러 business ended");
        res.status(400).send({message:"business ended"});
        return;
    }
    beforePay(posType, storeUserNo, storeNo, input, details)
    .then(data=>{
        if(data){
            orderNo = data;
        }
        return db.query(pay_proc,null,[payTax, payAmt, payTotal, cashAmt, cardAmt, cashPrint,
                                        orderNo, storeNo, userNo, cardId, cardType,
                                        cardName, cardCompany, installmentMm, cardYymm,
                                        approvalDttm, approvalKey, terminalId, tranId, barcode, curPoint,
                                        pointAmt, mileageTranType, mileageAmt,
                                        {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 1000 }])
    })
    .then((proc_result)=>{
        if(proc_result.outBinds == "OK"){
            pubsub.publish("paid", { storeNo: storeNo });
            if(userNo && (pointAmt != 0 || mileageAmt != 0 )){
                paidSucceed(storeNo, userNo, '결제완료', cashAmt,cardAmt,pointAmt,mileageAmt).then(_=>console.log('결제완료 fcm send success')).catch(err=>{console.log(err.toString());console.log('ERROR: 결제완료 fcm send failed')});
            }
            orderPublish(storeNo, orderNo, 'deleted', deviceId).then(_=>console.log('fcm send success')).catch(err=>console.log('fcm_err: '+err));    
            res.status(201).send({cardCompany});
        }else{
            proc_error = proc_result.outBinds;
            throw "proc error"
        }
    })
    .catch(err=>{
        console.error(err.toString());
        if(err == "proc error"){
            console.log(proc_error);
            res.status(400).send({message:proc_error});
        }else if(typeof err == "string"){
            res.status(400).send({message:err});
        }else{
            res.status(500).send(err.toString());
        }
    });
});
function beforePay(posType, userNo, storeNo, input, details){
    return new Promise((resolve, reject)=>{
        //주문 + 결제 동시에
        if(posType == '1'){
            db.addOrder(input, details, userNo, storeNo)
                .then(_=>{resolve(_.id)})
                .catch(err=>{console.log(err);reject('add order failed')});
        }
        //주문 결제 따로
        else{
            resolve();
        }
    });
}
function parse(str) {
    if(!/^(\d){8}$/.test(str)) return "invalid date";
    var y = str.substr(0,4),
        m = str.substr(4,2),
        d = str.substr(6,2);
    return new Date(y,m,d);
}
/**
 * 현금영수증 정보를 수정한다.
 */
router.put('/cash',(req,res)=>{
    // error list
    // not enough params
    let storeNo = req.user.uid;
    let payNo = req.body.payNo;
    let cashPrint = req.body.cashPrint;

    if(!payNo || !cashPrint){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let check_cashPrint = `
        select CASH_PRINT "cashPrint" from ORDER_PAY where PAY_NO = ${payNo}`;
    let upd_cashPrint = `
        update ORDER_PAY set
            CASH_PRINT = '${cashPrint}',
            UPD_DTTM = sysdate 
        where 
            PAY_NO = ${payNo} `;
    db.query(check_cashPrint)
    .then(cashPrint=>{
        if(cashPrint[0].cashPrint){
           throw "exist"; 
        }else{
            return db.query(upd_cashPrint);
        }
    })
    .then(result=>{
        pubsub.publish("cashPrintRevised", { storeNo: storeNo });
        res.status(201).send({message:"update cashPrint success"});
    })
    .catch(err=>{
        if(err == "exist"){
            res.status(400).send({message:"이미 현금영수증이 발행되었습니다."});
        }else{
            res.status(500).send({message:err.toString()});
        }
    });
});

/**
 * 이전 결제 방법의 결제 내역과 카드 결제의 경우 매입사, 승인번호, 카드 번호까지 한번에 가져온다
 */
router.get('/detail',(req, res) => {

    let orderNo = req.query.orderNo;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let data = null;
    let total = null;

    let get_detail = `
        select x.*
        from (
            SELECT CEIL(ROWNUM/${pageSize}) AS "page",y.* 
            FROM 
                (select 
                    a.PAY_NO "payNo", to_char(a.PAY_DTTM, 'yyyy-mm-dd hh24:mi:ss') "payDttm", 
                    a.PAY_TOTAL "payTotal", a.CASH_AMT "cashAmt", a.CARD_AMT "cardAmt", a.POINT_AMT "pointAmt", 
                    a.MILEAGE_AMT "mileageAmt", a.POINT_NO "pointNo", a.MILEAGE_NO "mileageNo",
                    b.CARD_COMPANY "cardCompany", b.APPROVAL_NO "approvalNo", b.CARD_ID "cardID" 
                from order_pay a 
                     left outer join CARD_APPROVAL b on b.pay_no = a.pay_no where a.ORDER_NO = ${orderNo} 
                order by a.PAY_DTTM desc 
                ) y
            ) x 
        WHERE x."page" = ${pageNo}`;
    let get_total = `
        select count(*) "cnt" 
        from order_pay a 
                left outer join CARD_APPROVAL b on b.pay_no = a.pay_no where a.ORDER_NO = ${orderNo} `;
    let returnVal_arr = [];
    db.query(get_detail)
    .then(detail=>{
        for(let item of detail){
            let returnVal = {payNo:item.payNo, payDttm:item.payDttm, payTotal:item.payTotal, detail: []};
            if(item.cardAmt != 0){
                let card = {payType:"카드", amt:item.cardAmt, cardCompany:item.cardCompany, approvalNo:item.approvalNo,cardID:item.cardID};
                returnVal.detail.push(card);
            }
            if(item.cashAmt != 0){
                let cash = {payType:"현금", amt:item.cashAmt};
                returnVal.detail.push(cash);
            }
            if(item.pointAmt != 0){
                let point = {payType:"포인트", amt:item.pointAmt};
                returnVal.detail.push(point);
            }
            if(item.mileageAmt != 0){
                let mileage = {payType:"마일리지", amt:item.mileageAmt};
                returnVal.detail.push(mileage);
            }
            returnVal_arr.push(returnVal);
        }
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:returnVal_arr,
            total:total[0].cnt
        });
    })
    .catch(err=>{
        res.status(500).send(err.toString());
    });
});

/**
 * 기간내에 결제가 완료된 주문 내역의 결제 방법을 가져온다
 */
router.get('/history',(req, res) => {
    //approvalKey, cardID, approvalDttm
    let storeNo = req.user.uid;
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let filter = req.query.filter; // 1:카드, 2:현금, 3:포인트, 4:마일리지, 5:복합, else:전체 
    let payType = ['','카드','현금','포인트','마일리지','복합'];

    let kiosk = req.query.kiosk == '1' ? true : false; 

    let get_history = !kiosk?`
        select x.* 
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            FROM (
                select
                    distinct a.ORDER_NO as "orderNo",
                    a.ORDER_NAME as "orderName",
                    to_char(a.ORDER_DTTM,'yyyy-mm-dd hh24:mi:ss') as "orderDttm",
                    a.ORDER_AMT as "orderAmt",
                    a.ORDER_TYPE as "orderType",
                    to_char(a.PAY_DTTM,'yyyy-mm-dd hh24:mi:ss') as "payDttm",
                    a.WAITING_NO as "watingNo",
                    a.ORDER_STATUS as "orderStatus",
                    a.TABLE_NO_ARRAY as "tables",
                    (
                        select LISTAGG(table_name,',') within group (order by table_name) table_name  
                        from store_table 
                        where table_no in (
                            select distinct regexp_substr(a.TABLE_NO_ARRAY, '[^|]+', 1, LEVEL)
                            from dual 
                            connect by LEVEL <= length(regexp_replace(a.TABLE_NO_ARRAY,'[^|]+',''))+1 
                        ) 
                    ) "tableName", 
                    a.FLOOR_NO as "floorNo", 
                    a.PAY_TOTAL as "payTotal",
                    (CASE (select count(*) from order_pay where order_no = a.order_no) 
                    WHEN 1 THEN
                        (CASE 
                            WHEN b.CARD_AMT = 0 and b.CASH_AMT = 0 
                                THEN (CASE WHEN b.POINT_AMT = 0 and b.MILEAGE_AMT = 0 THEN '미결제' 
                                            WHEN b.POINT_AMT = 0 and b.MILEAGE_AMT != 0 THEN '마일리지' 
                                            WHEN b.POINT_AMT != 0 and b.MILEAGE_AMT = 0 THEN '포인트'
                                            WHEN b.POINT_AMT 
                                            
                                            != 0 and b.MILEAGE_AMT != 0 THEN '복합' END)
                            WHEN b.CARD_AMT = 0 and b.CASH_AMT != 0 
                                THEN (CASE WHEN b.POINT_AMT = 0 and b.MILEAGE_AMT = 0 THEN '현금' 
                                        ELSE '복합' END)
                            WHEN b.CARD_AMT != 0 and b.CASH_AMT = 0 
                                THEN (CASE WHEN b.POINT_AMT = 0 and b.MILEAGE_AMT = 0 THEN '카드' 
                                        ELSE '복합' END)
                            ELSE NULL
                        END) 
                    ELSE '복합' END) "payType", 
                    --(CASE (select count(*) from order_pay where order_no = a.order_no) 
                    --    WHEN 1 THEN (
                    --            CASE WHEN b.CARD_AMT != 0 and b.POINT_AMT = 0 and b.MILEAGE_AMT = 0 
                    --                 THEN (select card_name from card_approval where pay_no = b.pay_no) 
                    --                 ELSE NULL 
                    --            END
                    --            )
                    --    ELSE NULL 
                    --END) as "cardName",
                    (CASE (select count(*) from order_pay where order_no = a.order_no)
                        WHEN 1 THEN b.pay_no ELSE 0 
                    END) as "payNo"  
                from order_master a, order_pay b   
                where 
                    a.order_no=b.order_no 
                    and a.STORE_NO=${storeNo} 
                    and to_char(b.PAY_DTTM,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
                order by a.ORDER_NO desc 
                ) y `
        :
       `select x.* 
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            FROM (
                select
                    distinct a.ORDER_NO as "orderNo",
                    a.ORDER_NAME as "orderName",
                    to_char(a.ORDER_DTTM,'yyyy-mm-dd hh24:mi:ss') as "orderDttm",
                    a.ORDER_AMT as "orderAmt",
                    a.ORDER_TYPE as "orderType",
                    to_char(a.PAY_DTTM,'yyyy-mm-dd hh24:mi:ss') as "payDttm",
                    a.WAITING_NO as "watingNo",
                    a.ORDER_STATUS as "orderStatus",
                    a.TABLE_NO_ARRAY as "tables",
                    (
                        select LISTAGG(table_name,',') within group (order by table_name) table_name  
                        from store_table 
                        where table_no in (
                            select distinct regexp_substr(a.TABLE_NO_ARRAY, '[^|]+', 1, LEVEL)
                            from dual 
                            connect by LEVEL <= length(regexp_replace(a.TABLE_NO_ARRAY,'[^|]+',''))+1 
                        ) 
                    ) "tableName", 
                    a.FLOOR_NO as "floorNo", 
                    a.PAY_TOTAL as "payTotal",
                    c.CARD_NAME as "cardName", 
                    '카드' as "payType", 
                    (CASE (select count(*) from order_pay where order_no = a.order_no)
                        WHEN 1 THEN b.pay_no ELSE 0 
                    END) as "payNo",
                    c.card_id as "cardID",
                    to_char(c.approval_dttm,'yyyymmdd') as "approvalDttm",
                    c.approval_key as "approvalKey" 
                from order_master a, order_pay b, card_approval c    
                where 
                    a.order_no=b.order_no 
                    and a.STORE_NO=${storeNo}     
                    and to_char(b.PAY_DTTM,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
                    and b.pay_no = c.pay_no 
                order by a.ORDER_NO desc
            ) y `;
    if(filter >= 1 && filter <= 5){
        get_history += `
                where y."payType" = '${payType[filter]}' `;
    }
        get_history += `
            ) x 
        WHERE x."page" = ${pageNo}`;
    let get_total = `
        select count(*) "cnt" 
        from (
            select count(*)   
            from order_master a, order_pay b  
            where 
                a.order_no = b.order_no 
                and a.store_no = ${storeNo} 
                and to_char(b.PAY_DTTM,'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
                group by a.order_no 
            ) `;
    let history = null;
    db.query(get_history)
    .then(history_result=>{
        history = history_result;
        return db.query(get_total);
    })
    .then(total =>{
        res.status(200).send({
            list:history,
            total:total[0].cnt
        });
    })
    .catch(err=>{
        console.log(err.toString());
        
        res.status(500).send(err.toString());
    });
});

router.post('/pointPay', (req,res) => {
    let barcode = (req.body.barcode);
    let userNo = req.body.userNo;
    let storeNo = req.user.uid;
    let curPoint = req.body.curPoint;
    let payAmt = Number(req.body.payAmt);
    let pointUse = req.body.pointUse;
    let pointSave = req.body.pointSave;
    let stampCnt = req.body.stampCnt;
    let relNo = req.body.relNo;
  
    let mileageTranType = req.body.mileageTranType;
    let mileagePoint = req.body.mileagePoint;
    let saveRatio = req.body.saveRatio;
    let sales=null;
    let id=req.user.uid;
    let get_salesDttm = `
        select to_char(SALES_DTTM,'yyyy-mm-dd') "sales" from store_sales where STORE_NO=:id and CLOSE_DTTM is NULL `;
    let upd_issue_mileage = `
        update store_sales 
        set ISSUE_MILEAGE = ISSUE_MILEAGE + :mileagePoint  
        where to_char(SALES_DTTM,'yyyy-mm-dd') = :sales and store_no= :id `;
    let proc_point_pay = `
        BEGIN PROC_POINT_PAY(
            :i_barcode,
            :i_user_no,
            :i_store_no,
            :i_cur_point,
            :i_pay_amt,
            :i_point_use,
            :i_point_save,
            :i_stamp_cnt,
            :i_rel_no,
            :i_mileage_tran_type,
            :i_mileage_point,
            :i_save_ratio,
            :o_result
        ); END;`
    let result = null;
    console.log('point Pay');
    let connection  = null;
    db.getConnection()
    .then(conn=>{
        connection = conn;
        return db.query(get_salesDttm, connection, [id]);
    })
    .then(dttm=>{
        sales = dttm[0].sales;
        return db.query(proc_point_pay,connection,[ barcode,userNo,storeNo,curPoint,payAmt,pointUse,pointSave,
                                                    stampCnt,relNo,mileageTranType,mileagePoint,saveRatio,
                                                    {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 }]);
    })
    .then(res=>{
        result = res;
        if(result.outBinds == 'OK'){
            return db.query(upd_issue_mileage,connection,[mileagePoint,sales,id]);
        }else{ 
            throw 'proc_err';
        }
    })
    .then(_=>{
        return connection.commit();
    })
    .then(_=>{
        connection.close().then();
        pubsub.publish("pointPaid", { storeNo: storeNo });
        res.status(201).send({state:"success"});
    })
    .catch(err => {
        console.log(err)
        if (connection)
            connection.close().then(); 
        if(err == 'proc_err' && result){
            if(result.outBinds == 'exceed') {
                res.status(404).send({state:"exceed",message:result.outBinds});
            }else if(result.outBinds == 'min') {
                res.status(404).send({state:"min",message:result.outBinds});
            }else {
                res.status(404).send({state:"fail",message:result.outBinds});
            }
        }else{
            res.status(500).send({message:err});
        }
    });
});
/**
 * 주문 내역을 외상으로 처리한다. order_master의 credit_tran_yn을 1로 변경.
 */
router.put('/credit',(req,res)=>{
    //error list
    //orderNo undefined
    let orderNo = req.body.orderNo;
    let creditTitle = (req.body.creditTitle == undefined ? null : req.body.creditTitle);
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let posType = req.body.posType?req.body.posType : 2;//1: 선불 2: 후불
    let input = req.body.input;
    let details = req.body.details;
    if(posType == 2 && orderNo == undefined){
        res.status(400).send({message:"orderNo undefined"});
        return;
    }
    let upd_credit = `
        update ORDER_MASTER 
        set 
            CREDIT_TRAN_YN = '1', 
            CREDIT_TITLE = '${creditTitle}',
            ORDER_STATUS = '2'  
        where ORDER_NO = :orderNo`;
    beforePay(posType, userNo, storeNo, input, details)
    .then(data=>{
        if(data){
            orderNo = data;
        }
        return db.query(upd_credit,null,[orderNo]);
    })
    .then(upd_result=>{
        pubsub.publish("creditTran", { storeNo: storeNo });
        res.sendStatus(201);
    })
    .catch(err=>{
        res.status(500).send(err.toString());
    });
});

module.exports = router;