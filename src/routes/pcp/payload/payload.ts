import { Router } from 'express';
import { db } from '../../../data-base/db';
import { SALT } from '../../../config';
import * as crypto from 'crypto';
let router = Router();

router.post('/', async (req,res)=>{
    console.log('POST payload');
    console.log(JSON.stringify(req.body));
    let username = req.body.username;
    let password = req.body.password;
    let subject = req.body.subject;
    let pwdSalted = password + SALT;
    let findUser;
    let param = [];
    if(!subject){
        findUser = `
            select 
                store_no as "storeNo", user_no as "userNo" 
            from user_store 
            where 
                login_account = :id 
                and (LOGIN_PASSWORD='${crypto.createHash('sha256').update(pwdSalted).digest('hex')}'  
                    or LOGIN_PASSWORD='${crypto.createHash('md5').update(password).digest('hex')}' 
                    or LOGIN_PASSWORD='${password}') `;
        param.push(username);
    }else{
        findUser = `
                select store_no as "storeNo", user_no as "userNo"  from user_store where user_no = :subject`;
        param.push(subject)
    }

    try{
        let result:any = await db.query(findUser, null, param);
        if(result.length == 0) throw { code : 404, msg:'no user'};
        else{
            result = result[0];
            res.status(200).send({
                subject : result.userNo, 
                payload : { 
                    storeNo : result.storeNo 
                }
            });
        }
    }catch(err){
        console.error(err);
        if(typeof err =='object' && err.code){
            res.status(err.code).send({message:err.msg});
            return;
        }
        res.status(500).send({message:err});
    }
});

module.exports = router;