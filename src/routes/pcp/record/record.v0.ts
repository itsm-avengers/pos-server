/**
 * Created by sdh077 on 17. 3. 22.
 */
import * as express from 'express';
let router = express.Router();
let oracledb = require('oracledb');

let db = require('../../../data-base/db').db;
let lib = require('../../../lib');
let doRelease = lib.doRelease;
let oracleData = lib.oracleData;
let inflating = lib.inflating;

/**
 * 현금 영수증 발행 내역 정보를 가져온다.
 */
router.get('/cashPrint',(req, res) => {
    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    endDt += ' 23:59';
    let get_cashPrint = `
        select x.* 
            from (
                SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
                FROM (
                    select 
                        a.PAY_NO "payNo",
                        to_char(a.PAY_DTTM,'yyyy-mm-dd hh24:mi:ss') "payDttm",
                        a.CASH_AMT "cashAmt", 
                        a.CASH_PRINT "cashPrint" 
                    from order_pay a, order_master b 
                    where a.cash_print is not null 
                        and a.PAY_DTTM between to_date('${startDt}','yyyy-mm-dd') and to_date('${endDt}','YYYY-MM-DD hh24:mi') 
                        and a.order_no=b.order_no 
                        and b.store_no = ${storeNo} 
                    order by a.PAY_DTTM desc
                ) y 
            ) x WHERE x."page" = ${pageNo} `;
    let get_total = ` 
            select 
                count(a.PAY_NO) as "total" 
            from order_pay a, order_master b 
            where cash_print is not null 
                and a.PAY_DTTM between to_date('${startDt}','yyyy-mm-dd') and to_date('${endDt}','YYYY-MM-DD hh24:mi') 
                and a.order_no=b.order_no 
                and b.store_no = ${storeNo}`;
    let cash_print = null;
    db.query(get_cashPrint)
    .then(cash_print_result=>{
        cash_print = cash_print_result;
        return db.query(get_total);
    })
    .then(total =>{
        res.status(200).send({
            list:cash_print,
            total:total[0].total
        });
    })
    .catch(err=>{
        res.status(500).send({message:err});
    })
});


/**
 * 일자별 매출
 * 날짜만으로 구분
 */
router.get('/dailySales',(req, res) => {
    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;

    let query = `
        select 
            count(*) as "salesCNT" , sum(a.PAY_TOTAL+a.PAY_TAX) "totalSales" 
        from order_pay a , order_master b 
        where 
            a.order_no = b.order_no and b.store_no = ${storeNo} 
            and to_char(a.pay_dttm, 'yyyy-mm-dd') between '${startDt}' and '${endDt}' `;

    let query2 =`
        select x.*
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.*
            FROM 
                (select  
                    to_char(a.pay_dttm,'yyyy-mm-dd hh24:mi:ss') "salesDttm", a.PAY_TOTAL+a.PAY_TAX "salesAmt", a.PAY_TYPE "salesType" 
                from order_pay a , order_master b 
                where 
                    a.order_no = b.order_no 
                    and b.store_no = ${storeNo} 
                    and to_char(a.pay_dttm, 'yyyy-mm-dd') between '${startDt}' and '${endDt}' 
                ) y 
            ) x 
        WHERE x."page" = ${pageNo} `;
  
    let salesCNT = null;
    let totalSales = null;
    let list = null;
    let nul=0;
    db.query(query)
    .then(result=>{
        salesCNT = (result[0].salesCNT == null ? 0 : result[0].salesCNT);
        totalSales = (result[0].totalSales == null ? 0 : result[0].totalSales);
        return db.query(query2);
    })
    .then(salesList=>{
        res.status(200).send({
            salesCNT : salesCNT,
            totalSales:totalSales,
            list:salesList
        });
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 연도를 정하여 매달 매출 내역 정보를 가져온다
 */
router.get('/monthlySales',(req, res) => {
    let id = req.user.uid;
    let no = req.user.no;
    let pageSize = req.query.pageSize
    let page = req.query.pageNo

    let chooseYear = req.query.chooseYear;
    chooseYear = String(chooseYear);


    let query =
      'select x.*'+
      ' from (SELECT CEIL(ROWNUM/'+pageSize+') AS "page", y.* FROM ('+
      ' select b.log_dt as "salesMonth", nvl(a.PAY_TOTAL,0) as "totalSales", nvl(a.CASH_AMT,0) as "salesCash",' +
      ' nvl(a.CARD_AMT,0) as "salesCard", nvl(a.POINT_AMT,0) as "salesPoint" , nvl(a.MILEAGE_AMT,0) as "salesMileage" , nvl(a.salesCNT,0) as "salesCNT"' +
      ' from (' +
      ' select to_char(a.PAY_DTTM,\'yyyy-mm\') as acc_dt,' +
      ' sum(a.PAY_TOTAL+a.PAY_TAX) as PAY_TOTAL ,' +
      ' sum(a.CASH_AMT) as CASH_AMT ,' +
      ' sum(a.CARD_AMT) as CARD_AMT ,' +
      ' sum(a.POINT_AMT) as POINT_AMT ,' +
      ' sum(a.MILEAGE_AMT) as MILEAGE_AMT ,' +
      ' count(a.PAY_TOTAL) as salesCNT' +
      ' from order_pay a , order_master b' +
      ' where b.order_status = 2 and b.store_no = ' +id+
      ' and b.order_no =a.order_no ' +
      ' group by to_char(a.PAY_DTTM,\'yyyy-mm\')' +
      ' ) a,' +
      ' ( select \''+chooseYear+'-12\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-11\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-10\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-09\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-08\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-07\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-06\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-05\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-04\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-03\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-02\' as log_dt' +
      ' from dual' +
      ' union all' +
      ' select \''+chooseYear+'-01\' as log_dt' +
      ' from dual' +
      ') b' +
      ' where a.acc_dt(+) = b.log_dt' +
      ' order by b.log_dt'+
      ' ) y'+
      ' ) x'+
      ' WHERE x."page" = '+page;


    oracleData(query,(call) => {
        res.json(inflating(call));
    });
});

/**
 * 기간 설정에 따른 메뉴별 매출 정보를 가져온다
 */
// router.get('/menuSales', async (req, res) => {
//     let id = req.user.uid;
//     let no = req.user.no;

//     let startDt = req.query.startDt
//     let endDt = req.query.endDt
//     let pageSize = req.query.pageSize
//     let page = req.query.pageNo


//     let query = `
//         select x.* 
//         from (
//             SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
//             FROM (
//                 select  a.MENU_NO "menuNo", a.MENU_CODE "menuCode", b.MENU_NAME "menuName",
//                         NVL(
//                             (select sum(B.item_cnt) 
//                             from    order_detail b , order_master c 
//                             where   c.order_status = 2 
//                                     and b.menu_no = a.menu_no 
//                                     and b.order_no = c.order_no 
//                                     and c.ORDER_DTTM between to_date('${startDt}','yyyy-mm-dd hh24:mi') and to_date('${endDt} 23:59','yyyy-mm-dd hh24:mi')
//                             ), 0 
//                         ) "menuNums",
//                         NVL(
//                             (select sum(b.item_amt*b.item_cnt)  
//                             from    order_detail b, order_master c 
//                             where   c.order_status = 2 
//                                     and b.menu_no = a.menu_no 
//                                     and b.order_no = c.order_no 
//                                     and c.ORDER_DTTM between to_date('${startDt}','yyyy-mm-dd hh24:mi') and to_date('${endDt} 23:59','yyyy-mm-dd hh24:mi')
//                             ), 0
//                         ) "menuTotalAmt" 
//                 from    store_menu a, store_menu_nls b 
//                 where   a.store_no = ${id} 
//                         and b.menu_no = a.menu_no 
//                         and b.nls_code = 'ko' 
//                 order by "menuNums" desc 
//             ) y
//         ) x
//         WHERE x."page" = ${page} 
//     `;
//     console.log(query)
//     oracleData(query,(call) => {
//         res.json(inflating(call));
//     });
// });

/**
 * 고객통계
 */
router.get('/clientUseStat',(req,res)=>{

});
/**
 * 매장 이용 통계
 */
router.get('/storeUseStat',(req,res)=>{

});
/**
 * 매출 조회 올인원
 */
router.get('/salesAIO',(req,res)=>{

});
router.get('/',(req, res) => {
    let id = req.user.uid;
    let no = req.user.no;

    let query =
        'select count(*) ,sum(a.PAY_TOTAL) from order_pay a, order_master b ' +
        'where a.PAY_DTTM between to_date(:startDt,\'yyyy-mm-dd\') and to_date(:endDt,\'yyyy-mm-dd\') and a.order_no = b.order_no and b.store_no = :id'


    oracleData(query,(call) => {
        res.json(inflating(call));
    });
});
router.post('/',(req, res) => {


    let accountCode =req.body.accountCode
    let expDt =req.body.expDt
    let expRemark =req.body.expRemark
    let expAmt =req.body.expAmt
    let id=req.user.uid;
    let no=req.user.no;

    let query =
        'INSERT INTO STORE_EXPENSE' +
        ' (EXPENSE_NO ,REG_USER_NO ,REG_DTTM ,UPD_DTTM ,ACCOUNT_CODE ,EXP_AMT ,EXP_DT ,STORE_NO ,EXP_REMARK)' +
        ' VALUES' +
        ' (SEQ_EXPENSE_NO.NEXTVAL ,:REG_USER_NO ,sysdate ,sysdate ,:ACCOUNT_CODE ,:EXP_AMT ,to_date(:EXP_DT,\'yyyy-mm-dd\') ,:STORE_NO ,:EXP_REMARK)'
    console.log(query)
    oracledb.getConnection(
        (err,connection) => {
            if(err){
                console.log(err.message);
                doRelease(connection)
                res.send(err.message);
            }
            else
                connection.execute(
                    query
                    ,[no,accountCode,expAmt,expDt,id,expRemark],{ autoCommit: true },
                    (err)=>{
                        if(err){
                            console.log(err.message);
                            doRelease(connection)
                            res.status(404).send(err.message);
                        }
                        else
                            connection.commit((err)=>{
                                if(err){
                                    console.log(err.message);
                                    doRelease(connection)
                                    res.status(404).send({state:"fail"});
                                }
                                else {
                                    doRelease(connection)
                                    res.send({state:"success"});
                                }
                            })
                    });
        });
});
router.put('/',(req, res) => {
    let expensiveNo =req.body.expensiveNo
    let accountCode =req.body.accountCode
    let expDt =req.body.expDt
    let expRemark =req.body.expRemark
    let expAmt =req.body.expAmt
    let id=req.user.uid;
    let no=req.user.no;

    let query =
        'UPDATE STORE_EXPENSE' +
        ' SET UPD_DTTM       = sysdate ,' +
        ' ACCOUNT_CODE         = \''+accountCode+'\' ,' +
        ' EXP_AMT     = '+no+' ,' +
        ' EXP_DT         = to_date(\''+expDt+'\',\'yyyy-mm-dd\') ,' +
        ' EXP_REMARK     = \''+expRemark +'\''+
        ' WHERE EXPENSE_NO = '+expensiveNo
    console.log(query)
    oracledb.getConnection(
        (err,connection) => {
            if(err){
                console.log(err.message);
                doRelease(connection)
                res.send(err.message);
            }
            else
                connection.execute(
                    query
                    ,[],{ autoCommit: true },
                    (err)=>{
                        if(err){
                            console.log(err.message);
                            doRelease(connection)
                            res.status(404).send(err.message);
                        }
                        else
                            connection.commit((err)=>{
                                if(err){
                                    console.log(err.message);
                                    doRelease(connection)
                                    res.status(404).send({state:"fail"});
                                }
                                else {
                                    doRelease(connection)
                                    res.send({state:"success"});
                                }
                            })
                    });
        });
});
router.delete('/',(req, res) => {
    let expensiveNo =req.query.expensiveNo

    let id=req.user.uid;
    let no=req.user.no;

    let query =
        'DELETE FROM STORE_EXPENSE WHERE EXPENSE_NO = '+expensiveNo

    oracledb.getConnection(
        (err,connection) => {
            if(err){
                console.log(err.message);
                doRelease(connection)
                res.send(err.message);
            }
            else
                connection.execute(
                    query
                    ,[],{ autoCommit: true },
                    (err)=>{
                        if(err){
                            console.log(err.message);
                            res.send('err');
                        } else

                        connection.commit((err)=>{
                            if(err){
                                console.log(err.message);
                                doRelease(connection)
                                res.status(404).send({state:"fail"});
                            }
                            else {
                                doRelease(connection)
                                res.send({state:"success"});
                            }
                        })
                    });
        });
});


/**
 * created by mjLee 17.08.17 
 * modified on 17.09.18
 * 결제 수단 별 통계
 */
router.get('/salesPaytype',(req,res)=>{
    // error list
    // not enough params
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let pageNo = req.query.pageNo;
    let pageSize = req.query.pageSize;
    let filter = req.query.filter; // 1:카드별, else: 전체
    let storeNo = req.user.uid;

    if(!startDt || !endDt || !pageNo || !pageSize){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let get_salesPaytype = null;
    let get_total = null;
    if(filter == 1){
        get_salesPaytype = `
            select x.* 
            from ( 
                select CEIL(ROWNUM/${pageSize}) AS "page", y.* 
                from (
                    select 
                        b.card_name "cardName",
                        sum(a.card_amt) "cardAmt",
                        (count(case when a.card_amt > 0 then 1 end) - count(case when a.card_amt < 0 then 1 end)) "cardCNT",
                        sum(case when a.card_amt > 0 then a.pay_tax end) "cardTax"
                    from 
                        order_pay a, card_approval b
                    where 
                        a.order_no in (select order_no from order_master where store_no = ${storeNo})
                        and to_char(pay_dttm, 'yyyy-mm-dd') between '${startDt}' and '${endDt}'
                        and a.pay_no = b.pay_no 
                    group by b.card_name
                    order by b.card_name asc 
                ) y 
            ) x 
            where x."page" = ${pageNo} `;

        get_total = `
            select count(*) "total" 
            from (
                select b.card_name  
                from 
                    order_pay a, card_approval b 
                where 
                    a.order_no in (select order_no from order_master where store_no = ${storeNo})
                    and to_char(pay_dttm, 'yyyy-mm-dd') between '${startDt}' and '${endDt}'
                    and a.pay_no = b.pay_no
                group by b.card_name 
            ) `;
    }else{
        get_salesPaytype = `
            select *
            from (
                select 
                    sum(card_amt) "cardAmt", sum(cash_amt) "cashAmt", sum(point_amt) "pointAmt", sum(mileage_amt) "mileageAmt",
                    (count(case when card_amt > 0 then 1 end) - count(case when card_amt < 0 then 1 end)) "cardCNT",
                    (count(case when cash_amt > 0 then 1 end) - count(case when cash_amt < 0 then 1 end)) "cashCNT",
                    (count(case when point_amt > 0 then 1 end) - count(case when point_amt < 0 then 1 end)) "pointCNT",
                    (count(case when mileage_amt > 0 then 1 end) - count(case when mileage_amt < 0 then 1 end)) "mileageCNT",
                    sum(case when card_amt > 0 then pay_tax end) "cardTax",
                    sum(case when cash_amt > 0 then pay_tax end) "cashTax",
                    sum(0) "pointTax",
                    sum(0) "mileageTax" 
                from 
                    order_pay 
                where 
                    order_no in (select order_no from order_master where store_no = ${storeNo})
                    and to_char(pay_dttm, 'yyyy-mm-dd') between '${startDt}' and '${endDt}'
            )`;
    }
    let returnVal_arr = [];
    db.query(get_salesPaytype)
    .then(result=>{
        let salesResult = result[0];
        if(filter == 1){
            for(let item of result){
                returnVal_arr.push({
                    cardName: item.cardName,
                    payTotal:item.cardAmt,
                    payTax:item.cardTax,
                    payAmt:item.cardAmt - item.cardTax,
                    payCNT:item.cardCNT
                });
            }
            return db.query(get_total);
        }else{
            if(salesResult.cardAmt > 0){
                let card = {payType:null,payTotal:null,payTax:null,payAmt:null,payCNT:null};
                card.payType = "카드";
                card.payTotal = salesResult.cardAmt;
                card.payTax = salesResult.cardTax;
                card.payAmt = salesResult.cardAmt - salesResult.cardTax;
                card.payCNT = salesResult.cardCNT;
                returnVal_arr.push(card);
            }
            if(salesResult.cashAmt > 0){
                let cash = {payType:null,payTotal:null,payTax:null,payAmt:null,payCNT:null};
                cash.payType = "현금";
                cash.payTotal = salesResult.cashAmt;
                cash.payTax = salesResult.cashTax;
                cash.payAmt = salesResult.cashAmt - salesResult.cashTax;
                cash.payCNT = salesResult.cashCNT;
                returnVal_arr.push(cash);
            }
            if(salesResult.pointAmt > 0){
                let point = {payType:null,payTotal:null,payTax:null,payAmt:null,payCNT:null};
                point.payType = "포인트";
                point.payTotal = salesResult.pointAmt;
                point.payTax = 0;
                point.payAmt = salesResult.pointAmt;
                point.payCNT = salesResult.pointCNT;
                returnVal_arr.push(point);
            }
            if(salesResult.mileageAmt > 0){
                let mileage = {payType:null,payTotal:null,payTax:null,payAmt:null,payCNT:null};
                mileage.payType = "마일리지";
                mileage.payTotal = salesResult.mileageAmt;
                mileage.payTax = 0;
                mileage.payAmt = salesResult.mileageAmt;
                mileage.payCNT = salesResult.mileageCNT;
                returnVal_arr.push(mileage);
            }
            return [];
        }
        
    })
    .then(total=>{
        if(filter == 1){
            res.status(200).send({
                list:returnVal_arr,
                total:total[0].total
            });
        }else{
            res.status(200).send({
                list:returnVal_arr
            });
        }
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 메뉴 별 통계
 */
router.get('/salesMenu',(req,res)=>{
    // error list
    // not enough params

    let startDt = req.query.startDt;
    let endDt = req.query.endDt;
    let categoryNo = req.query.filter; // 0이면 전부, 아니면 카테고리 넘버에 해당하는 것만
    let storeNo = req.query.storeNo; 

    if (!storeNo) {
        storeNo = req.user.uid;
    }

    if(!startDt || !endDt || !categoryNo){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let get_salesMenu = `
        SELECT MENU_NM "menuName", sum(item_cnt) "menuCNT", sum((item_amt-dc_amt)*item_cnt) "payTotal"
        FROM ( 
        select b.MENU_NAME||' '||c.opt_nm MENU_NM, b.item_cnt, b.item_amt, b.dc_amt  
        from order_master a,
            order_detail b,
            (SELECT ORDER_ITEM_SEQ, substr(XMLAGG(XMLELEMENT(COL ,',', OPT_NAME) ORDER BY ORDER_ITEM_SEQ).EXTRACT('//text()' ).GETSTRINGVAL(),2) opt_nm 
                FROM order_opt
            GROUP BY  ORDER_ITEM_SEQ) c,
            store_menu d,
            store_menu_nls e   
        WHERE a.order_no = b.order_no
        AND b.ORDER_ITEM_SEQ = c.ORDER_ITEM_SEQ(+)
        AND b.menu_no = d.menu_no
        AND d.menu_no = e.menu_no
        AND e.nls_code = 'ko' 
       `;
        if(categoryNo != 0){
            get_salesMenu += `AND d.CATEGORY_NO = ${categoryNo}`;
        }   
        get_salesMenu += `
        AND a.store_no=${storeNo} 
        AND a.order_status='2' 
        AND to_char(pay_dttm,'yyyy-mm-dd') between '${startDt}' and '${endDt}')
        GROUP BY MENU_NM  
        ORDER BY 1
        `;
    let returnVal = [];    
    db.query(get_salesMenu)
    .then(menu_statistics=>{
        let idx = 0;
        for(let stat of menu_statistics){
            returnVal[idx] = stat;
            returnVal[idx].payAmt = null;
            returnVal[idx].payTax = null;

            //세금 계산 (할인된 금액이기 때문에 menu에 저장된 tax_amt를 활용 못함..ㅠㅠ)
            returnVal[idx].payTax = (stat.payTotal / 11) * 10;
            returnVal[idx].payTax = Math.round((Math.floor(returnVal[idx].payTax) / 10));
            returnVal[idx].payAmt = stat.payTotal - returnVal[idx].payTax;
            idx++;
        }
        res.status(200).send({list:returnVal});
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err});
    });
});
module.exports = router;