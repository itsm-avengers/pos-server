import * as express from "express";
import { db } from '../../../data-base/db';
import * as func from "../function";
let router = express.Router();

/**
 * 리뷰 목록 가져오기
 */
router.get('/',(req:any, res)=>{
    // error list
    // not enough params
    console.log('review get');
    
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let searchTxt = (!req.query.searchTxt ? '' : req.query.searchTxt);

    if(!pageSize || !pageNo){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let query =`
        select x.*  
        from (
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.*
            FROM (
                select 
                    a.review_no as "reviewNo",
                    a.store_no as "storeNo",
                    d.store_disp_name as "storeName",
                    b.thumbnail_url_s as "thumbnailURL",
                    to_char(a.upd_dttm,'yyyy-mm-dd hh24:mi:ss') as "modifiedDate",
                    a.status as "status",
                    (CASE WHEN a.status = '1' then a.review_text 
                        WHEN a.status = '2' then a.review_text 
                        WHEN a.status = '3' then '[삭제된 리뷰입니다]' 
                        WHEN a.status = '4' then '[신고접수된 리뷰입니다]' 
                        END) as "reviewTxt", 
                    to_char(a.upd_dttm,'yyyy-mm-dd hh24:mi') as "publishDate",
                    a.estimation_point as "estimationPoint",
                    a.comment_cnt as "commentCNT",
                    a.image_cnt as "imageCNT",
                    b.download_url as "imageURL",
                    a.read_cnt as "readCNT",
                    a.recommended_cnt as "likeCNT",
                    (select nvl(count(1),0) from REVIEW_JOIN where REVIEW_NO = a.review_no and USER_NO = '${userNo}') as "joined",
                    e.user_name as "userName",
                    e.user_no as "userNo"
                from review a, review_file b, store_info d, user_master e 
                where 1=1 
                    and a.review_no = b.review_no  
                    and a.store_no = d.store_no 
                    and a.user_no = e.user_no 
                    and a.status in ('1','2') 
                    and a.review_text like '%${searchTxt}%' 
                    and d.store_no = ${storeNo} 
                ) y 
            ) x 
        where x."page" = ${pageNo} `;
    let get_total = `
        select 
            count(*) "total", sum(a.estimation_point) "totalPoint" 
        from review a, review_file b, store_info d, user_master e 
        where 1=1 
            and a.review_no = b.review_no  
            and a.store_no = d.store_no 
            and a.user_no = e.user_no 
            and a.status in ('1','2') 
            and a.review_text like '%${searchTxt}%' 
            and d.store_no = ${storeNo}`;
    let reviews = null;
    db.query(query)
    .then(review=>{
        reviews = review;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:reviews,
            total:total[0].total,
            totalPoint:total[0].totalPoint 
        });
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 리뷰의 코멘트 가져오기
 */
router.get('/comment',(req:any, res)=>{

    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let pageSize = req.query.pageSize;
    let reviewNo = req.query.reviewNo;
    let pageNo = req.query.pageNo;
    
    let get_comment = `
        select x.* 
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            FROM ( 
                select 
                    a.comment_no as "commentNo",
                    a.user_no as "userNo",
                    a.status as "status",
                    CASE WHEN a.status = '1' then a.comment_text 
                         WHEN a.status = '2' then a.comment_text || '[' || to_char(a.upd_dttm,'yyyy-mm-dd hh24:mi') || '에 수정됨]' 
                         WHEN a.status = '3' then '[삭제된 댓글입니다]' 
                         WHEN a.status = '4' then '[신고접수된 댓글입니다]' 
                    END as "commentTxt",
                    CASE WHEN a.user_no = ${userNo} THEN b.ceo_name 
                         ELSE c.user_name  
                    END as "displayName",
                    to_char(a.upd_dttm,'yyyy-mm-dd hh24:mi') as "publishDate" 
                from review_comment a, store_info b, user_master c  
                where 
                    a.review_no = ${reviewNo} 
                    and b.store_no = ${storeNo} 
                    and c.user_no = a.user_no 
                order by a.upd_dttm desc 
            ) y
        ) x
        WHERE x."page" = ${pageNo}`;
    let get_total = `
        select count(*) "total" 
        from review_comment a, store_info b, user_master c  
        where 
            a.review_no = ${reviewNo} 
            and b.store_no = ${storeNo} 
            and c.user_no = a.user_no `;
    let comments = null;
    db.query(get_comment)
    .then(comment=>{
        comments = comment;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:comments,
            total:total[0].total
        });
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 리뷰 코멘트 생성하기
 */
router.post('/comment',(req:any,res)=>{
    // error list
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let reviewNo = req.body.reviewNo;
    let commentText = req.body.commentText;

    if(!commentText){
        res.status(400).send({message:"no commentText"});
        return;
    }

    let insert_query = `
        insert into REVIEW_COMMENT 
            (COMMENT_NO, REVIEW_NO, USER_NO, COMMENT_TEXT, REG_DTTM, UPD_DTTM, DEL_DTTM, REP_DTTM, STATUS)
        values
            (SEQ_COMMENT_NO.NEXTVAL, ${reviewNo}, ${userNo}, '${commentText}', sysdate, sysdate, null, null, '1')`;
    db.query(insert_query)
    .then(result=>{
        res.status(201).send({message:"insert success"});
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 코멘트 수정
 */
router.put('/comment',(req:any,res)=>{
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let commentNo = req.body.commentNo;
    let commentText = req.body.commentText;
    let reportYn = req.body.reportYn;

    let upd_query = null;
    if(reportYn == '1'){
        upd_query = `
            update REVIEW_COMMENT
            set 
                REP_DTTM = sysdate,
                STATUS = '4' 
            where COMMENT_NO = ${commentNo}`
    }else{
        upd_query = `
            update REVIEW_COMMENT 
            set 
                COMMENT_TEXT = '${commentText}',
                UPD_DTTM = sysdate,
                STATUS = '2'
            where COMMENT_NO = ${commentNo}`;
    }
    db.query(upd_query)
    .then(result=>{
        res.status(201).send({message:"upd success"});
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 코멘트 삭제
 */
router.delete('/comment',(req:any,res)=>{
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let commentNo = req.query.commentNo;

    let upd_query = `
        update REVIEW_COMMENT 
        set 
            DEL_DTTM = sysdate,
            STATUS = '3'
        where COMMENT_NO = ${commentNo}`;
    db.query(upd_query)
    .then(result=>{
        res.status(201).send({message:"delete success"});
    })
    .catch(err=>{
        res.status(500).send({message:err.toString()});
    });
});
module.exports = router;