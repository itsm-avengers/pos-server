/**
 * Created by sdh077 on 17. 3. 21.
 */
import * as express from 'express';
import { db } from '../../../../data-base/db';
let router = express.Router();
let jwt  = require("jsonwebtoken");

/**
 * store_info에 없는 부가적인 옵션들(ex 어린이방 유무, 주차장 유무 )을 가지고 온다.
 * useYN => 0:없음 1:있음
 */
router.get('/',(req, res) => {
  let storeNo = req.user.uid;

  let query =`
        select 
            a.DETAIL_CODE "detailCode", a.CODE_NAME "codeName", 
            (select NVL2(b.ATTR_CODE,b.ATTR_VALUE,'0') from store_attr b where b.ATTR_CODE=a.DETAIL_CODE and b.STORE_NO=${storeNo}) "useYN" 
        from code_master a 
        where 
          a.MASTER_CODE=120 
          and a.CODE_TYPE=2`;
  db.query(query)
  .then(result=>{
    for(let data of result){
      if(data.useYN == null){
        data.useYN = '0';
      }
    }
    res.status(200).send(result);
  })
  .catch(err=>{
    res.status(500).send({message:err});
  });
});

/**
 * attr_value(useYN)을 토글한다.
 */
router.post('/',(req, res) => {
    let detailCode = req.body.detailCode;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let toggle = `
      MERGE INTO STORE_ATTR USING DUAL ON (ATTR_CODE = '${detailCode}' and store_no = ${storeNo})
      WHEN MATCHED THEN
          update set ATTR_VALUE = (CASE (select attr_value from store_attr where 
                                          ATTR_CODE = '${detailCode}' and store_no = ${storeNo}) 
                                  WHEN '0' then '1' 
                                  ELSE '0' END) 
      WHEN NOT MATCHED THEN 
          INSERT (store_no,ATTR_CODE,ATTR_VALUE) 
          VALUES (${storeNo}, '${detailCode}', '1')`;
  db.query(toggle)
  .then(result=>{
    res.sendStatus(201);
  })
  .catch(err=>{
    res.status(500).send({message:err});
  });
});
module.exports = router;