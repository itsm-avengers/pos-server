/**
 * Created by sdh077 on 17. 3. 9.
 */
import * as express from 'express';
import { db } from '../../../../data-base/db';
let router = express.Router();
let jwt  = require("jsonwebtoken");

/**
 * 거래처 리스트를 가져온다
 */
router.get('/',(req, res) => {
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let query = `
        select x.*
        from (
          SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.*
          FROM (
            select 
              CUSTOMER_NO as "customerNo",
              CUSTOMER_NAME as "customerName",
              CUSTOMER_ID as "customerID",
              SUPPLY_ITEM as "supplyItem",
              CEO_NAME as "ceoName", 
              CEO_HP as "ceoHP", 
              TEL as "tel", 
              FAX as "fax" 
            from customer
            ) y
          ) x 
        WHERE x."page" = ${pageNo}`;
    let get_total = `
        select count(*) as "total" from customer`;
    let customer = null;
    db.query(query)
    .then(customerInfo=>{
      customer = customerInfo;
      return db.query(get_total);
    })
    .then(total=>{
      res.status(200).send({
        list:customer,
        total:total[0].total
      });
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
  
});

/**
 * 거래처를 추가한다.
 */
router.post('/',(req, res) => {
    // error list
    // not enough params

    let customerName =req.body.customerName;
    let customerID =req.body.customerID;
    let supplyItem =req.body.supplyItem;
    let ceoName =req.body.ceoName;
    let ceoHP =(req.body.ceoHP == undefined ? null : req.body.ceoHP);
    let tel =(req.body.tel == undefined ? null : req.body.tel);
    let fax =(req.body.fax == undefined ? null : req.body.fax);


    let storeNo = req.user.uid;
    let userNo = req.user.no;

    if(!customerName || !customerID || !supplyItem || !ceoName){
      res.status(400).send({message:"not enough params"});
      return;
    }

    let query =`
        INSERT
        INTO CUSTOMER
          (CUSTOMER_NAME ,CUSTOMER_NO ,CEO_NAME ,CUSTOMER_ID ,TEL ,SUPPLY_ITEM ,CEO_HP ,FAX)
        VALUES
          (:customerName ,seq_customer_no.nextval ,:ceoName ,:customerID ,:tel ,:supplyItem ,:ceoHP ,:fax)`;
    db.query(query,null,[customerName,ceoName,customerID,tel,supplyItem,ceoHP,fax])
    .then(insert_result=>{
      res.sendStatus(201);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 거래처 정보를 수정한다.
 */
router.put('/',(req, res) => {
    // error list
    // not enough params

    let customerNo =req.body.customerNo;
    let customerName =req.body.customerName;
    let customerID =req.body.customerID;
    let supplyItem =req.body.supplyItem;
    let ceoName =req.body.ceoName;
    let ceoHP =(req.body.ceoHP == undefined ? null : req.body.ceoHP);
    let tel =(req.body.tel == undefined ? null : req.body.tel);
    let fax =(req.body.fax == undefined ? null : req.body.fax);

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    if(!customerName || !customerID || !supplyItem || !ceoName){
      res.status(400).send({message:"not enough params"});
      return;
    }

    let query = `
        UPDATE CUSTOMER
        SET 
          CUSTOMER_NAME = '${customerName}',
          CEO_NAME = '${ceoName}',
          CUSTOMER_ID = '${customerID}',
          TEL = '${tel}',
          SUPPLY_ITEM = '${supplyItem}',
          CEO_HP = '${ceoHP}',
          FAX = '${fax}' 
        WHERE 
          CUSTOMER_NO = ${customerNo}`;
    db.query(query)
    .then(result=>{
      res.sendStatus(201);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 거래처 삭제한다.
 */
router.delete('/',(req, res) => {
    // error list
    // not enough params

    let customerNo =req.query.customerNo;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    if(!customerNo){
      res.status(400).send({message:"not enough params"});
      return;
    }

    let query = `
        DELETE FROM CUSTOMER WHERE CUSTOMER_NO = ${customerNo}`;
    db.query(query)
    .then(result=>{
      res.sendStatus(201);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

module.exports = router;