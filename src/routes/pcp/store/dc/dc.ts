import * as express from "express";
import * as oracledb from 'oracledb';

let router = express.Router();
import { db } from "../../../../data-base/db";

/**
 * dc 목록
 */
router.get('/',(req:any,res)=>{
    // error list

    let storeNo = req.user.uid;
    let get_dc = `
        select 
            DC_SET_NO "dcNo", DC_TYPE "dcType", DC_NAME "dcName", DC_VAL "dcAmt" 
        from STORE_DC_SET 
        where STORE_NO = ${storeNo} 
        order by DC_TYPE desc, DC_VAL asc`;
    
    db.query(get_dc)
    .then(dcInfo=>{
        res.status(200).send({list:dcInfo});
    })
    .catch(err=>{
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * dc 목록 추가 및 수정
 */
router.post('/',(req:any,res)=>{
    // error list
    // not enough params
    
    let storeNo = req.user.uid;
    let dcNo = req.body.dcNo | 0;
    let dcType = req.body.dcType;//할인종류(1:금액,2:퍼센트)
    let dcName = req.body.dcName;
    let dcAmt = req.body.dcAmt;

    if(!dcType || !dcName || (dcAmt == undefined)){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let merge_dc = `
        merge into STORE_DC_SET using dual on (DC_SET_NO = ${dcNo}) 
        when matched then 
            update set 
                DC_TYPE = '${dcType}',
                DC_NAME = '${dcName}',
                DC_VAL = ${dcAmt},
                REG_DTTM = sysdate 
        when not matched then 
            insert (DC_SET_NO, STORE_NO, DC_TYPE, DC_NAME, DC_VAL, REG_DTTM) 
            values (SEQ_DC_SET_NO.NEXTVAL, ${storeNo}, '${dcType}', '${dcName}', ${dcAmt}, sysdate) `;
    db.query(merge_dc)
    .then(result=>{
        res.status(201).send({message:`merge ${dcName} success`});
    })
    .catch(err=>{
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * dc 목록 삭제
 */
router.delete('/',(req:any,res)=>{
    // error list
    // not enough params

    let storeNo = req.user.uid;
    let dcNo = req.query.dcNo;

    if(!dcNo){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let dlt_dc = `
        delete from STORE_DC_SET where DC_SET_NO = ${dcNo} `;
    
    db.query(dlt_dc)
    .then(result=>{
        res.status(201).send({message:`delete No. ${dcNo} success`});
    })
    .catch(err=>{
        console.log(err);
        res.sendStatus(500);
    });
});

module.exports = router;