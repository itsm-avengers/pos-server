/**
 * Created by sdh077 on 17. 3. 9.
 */
import { Router } from 'express';
let router = Router();
let db = require("../../../../data-base/db").db;

/**
 * 가맹점 번호에 따른 층 정보를 가져온다
 */
router.get('/',(req, res) => {
    let storeNo = req.user.uid;
    let query = `
      select 
          FLOOR_NO as "floorNo",
          floor_name as "floorName",
          TABLE_CNT as "tableCNT",
          DISP_ORDER as "dispOrder" 
      from store_floor 
      where STORE_NO=${storeNo} 
      order by DISP_ORDER asc `;
    db.query(query)
    .then(result=>{
      res.status(200).send(result);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 테이블 수와 층이름을 통해 층을 추가한다.
 */
router.post('/',(req, res) => {
    let floorName = req.body.floorName;
    let tableCNT = req.body.tableCNT;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let query = `
        INSERT INTO STORE_FLOOR 
          (FLOOR_NO,FLOOR_NAME ,TABLE_CNT ,STORE_NO,DISP_ORDER)
        VALUES 
          (SEQ_FLOOR_NO.NEXTVAL, :floorName , :tableCNT , :storeNo,(select coalesce(max(DISP_ORDER),1) from store_floor where STORE_NO=:storeNo)+1)`;

    db.query(query,null,[floorName,tableCNT,storeNo,storeNo])
    .then(result=>{
      res.sendStatus(201);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 층이름, 순서, 테이블 수를 수정한다.
 */
router.put('/',(req, res) => {
    // error list
    // not enough params 
    let floorName = req.body.floorName;
    let tableCNT = req.body.tableCNT;
    let dispOrder = req.body.dispOrder;
    let floorNo = req.body.floorNo;
    let storeNo = req.user.uid;
    let userNo = req.user.no;

    if(!floorName || tableCNT == undefined || !dispOrder || !floorNo){
      res.status(400).send({message:"not enough params"});
      return;
    }

    let query = `
        UPDATE STORE_FLOOR SET 
            TABLE_CNT  = :tableCNT,
            DISP_ORDER  = :dispOrder,
            FLOOR_NAME  = :floorName
        WHERE STORE_NO = :storeNo
        AND FLOOR_NO = :floorNo`;
    db.query(query,null,[tableCNT,dispOrder,floorName,storeNo,floorNo])
    .then(result=>{
      res.sendStatus(201);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 해당하는 테이블과 층을 모두 삭제한다
 */
router.delete('/',(req, res) => {
    let floorNo = req.query.floorNo;

    let storeNo = req.user.uid;
    let userNo = req.user.no;
     
    //아직 해당 층에 오더가 남아있는지 체크
    let order_check = `
        select count(*) "cnt" from ORDER_MASTER where FLOOR_NO = ${floorNo} and ORDER_STATUS = '1' `;
    let delete_table = `
        DELETE FROM STORE_TABLE
        WHERE 
          STORE_NO = ${storeNo} 
          AND FLOOR_NO = ${floorNo} `;
    let delete_floor = `
        DELETE FROM STORE_FLOOR
        WHERE 
          STORE_NO = ${storeNo} 
          AND FLOOR_NO = ${floorNo}`;
  let connection = null;
  db.getConnection()
  .then(conn=>{
    connection = conn;
    return db.query(order_check,connection);
  }).then(cnt=>{
    if(cnt[0].cnt != 0){
      throw "order exist";
    }else{
      return db.query(delete_table,connection);
    }
  })
  .then(dlt_table=>{
    return db.query(delete_floor,connection);
  })
  .then(dlt_floor=>{
    return connection.commit();
  })
  .then(end=>{
    connection.close().then();
    res.sendStatus(201);
  })
  .catch(err=>{
    if(connection)
      connection.close().then();
    if(err == "order exist"){
      res.status(400).send({message:"해당 층에 주문이 남아있습니다."});
    }
    res.status(500).send(err.toString());
  });
});

module.exports = router;