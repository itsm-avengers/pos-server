
import { Router } from 'express';
let router = Router();
import { db } from "../../../../data-base/db";


/**
 * 매장 이미지 가져오기
 */
router.get('/',(req:any, res) => {
    let storeNo = req.user.uid;
    let query = `
        select 
            image_url as "url" ,disp_ord as "dispOrder", 
            thumbnail_url_s as "thumbnailS",thumbnail_url_m as "thumbnailM" ,thumbnail_url_l as "thumbnailL" 
        from store_image 
        where store_no=${storeNo} 
        order by disp_ord asc `;

    db.query(query)
    .then(result=>{
        res.status(200).send(result);
    })
    .catch(err=>{
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * 매장 이미지 추가, 수정
 */
router.post('/', (req:any,res) => {
    let storeNo = req.user.uid;
    let dispOrder = req.body.dispOrder;
    let url = req.body.url;
    let thumbnailS = req.body.thumbnailS;
    let thumbnailM = req.body.thumbnailM;
    let thumbnailL = req.body.thumbnailL;
    let merge_image = `
        MERGE INTO store_image USING DUAL ON (store_no = ${storeNo} and disp_ord = :dispOrder) 
        WHEN MATCHED THEN  
            UPDATE SET image_url = :url, thumbnail_url_s = :thumbnailS  
            , thumbnail_url_m = :thumbnailM, thumbnail_url_l = :thumbnailL  
            ,UPD_DTTM=sysdate 
        WHEN NOT MATCHED THEN 
            INSERT   
                (store_no, disp_ord, image_url,thumbnail_url_s,thumbnail_url_m,thumbnail_url_l,UPD_DTTM) 
            VALUES 
                (${storeNo},:dispOrder,:url,:thumbnailS,:thumbnailM,:thumbnailL,sysdate)`;
    
    db.query(merge_image, null, [dispOrder,url,thumbnailS,thumbnailM,thumbnailL,
                                dispOrder,url,thumbnailS,thumbnailM,thumbnailL])
    .then(merge_result=>{
        res.status(201).send({message:"merge success"});
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 매장 이미지 삭제
 */
router.delete('/',(req:any, res) => {
    let storeNo = req.user.uid;
    let dispOrder = req.query.dispOrder;
    
    if(dispOrder == undefined){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let dlt_img = `
        delete from store_image where store_no = ${storeNo} and disp_ord = ${dispOrder} `;

    db.query(dlt_img)
    .then(result=>{
        res.status(201).send({message:"delete success"});
    })
    .catch(err=>{
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * 매장 이미지 전체 삭제
 */
router.delete('/all',(req:any,res)=>{
    let storeNo = req.user.uid;
    let dlt_img_all = `
        delete from store_image where store_no = ${storeNo} `;
    db.query(dlt_img_all)
    .then(result=>{
        res.status(201).send({message:"delete all success"});
    })
    .catch(err=>{
        console.log(err);
        res.sendStatus(500);
    });
});
module.exports = router;