/**
 * Created by sdh077 on 17. 3. 9.
 */
// import * as express from "express";
import * as express from 'express';
let router = express.Router();

let jwt  = require("jsonwebtoken");
import { db } from "../../../../data-base/db";
import { valueFromAST } from 'graphql/utilities/valueFromAST';
import { createConnection } from 'net';

router.post('/init',(req,res)=>{
    let deleteMenuAndCategory = [];
    let storeNo = req.user.uid;
    deleteMenuAndCategory.push(`delete from store_menu_nls where menu_no in (select menu_no from store_menu where store_no = ${storeNo})`);
    deleteMenuAndCategory.push(`delete from store_menu_category_nls where store_no = ${storeNo}`);
    deleteMenuAndCategory.push(`delete from store_menu_category where store_no = ${storeNo}`);
    deleteMenuAndCategory.push(`delete from order_detail where order_no in (select order_no from order_master where store_no = ${storeNo})`);
    deleteMenuAndCategory.push(`delete from card_approval where pay_no in (select pay_no from order_pay where order_no in (select order_no from order_master where store_no = ${storeNo}))`);
    deleteMenuAndCategory.push(`delete from order_pay where order_no in (select order_no from order_master where store_no = ${storeNo})`);
    deleteMenuAndCategory.push(`delete from order_master where store_no = ${storeNo}`);
    deleteMenuAndCategory.push(`delete from store_set_menu where set_menu_no in (select menu_no from store_menu where store_no=${storeNo})`);
    deleteMenuAndCategory.push(`delete from store_menu_opt where menu_no in (select menu_no from store_menu where store_no = ${storeNo})`);
    deleteMenuAndCategory.push(`delete from order_detail where menu_no in (select menu_no from store_menu where store_no = ${storeNo})`);
    deleteMenuAndCategory.push(`delete from store_menu where store_no = ${storeNo}`);
    deleteMenuAndCategory.push(`delete from store_dc_set where store_no = ${storeNo}`);

    // insert into store_menu_category (STORE_NO,CATEGORY_NO,DISP_ORDER,USE_YN) values (4,SEQ_CATEGORY_NO.NEXTVAL,'1','1');
    // insert into store_menu_category_nls (store_no, category_no, nls_code, category_name) values (4, (select category_no from store_menu_category where store_no = 4 and disp_order = '1'), 'ko', 'Appetizer');

    // insert into store_menu_category (STORE_NO,CATEGORY_NO,DISP_ORDER,USE_YN) values (4,SEQ_CATEGORY_NO.NEXTVAL,'2','1');
    // insert into store_menu_category_nls (store_no, category_no, nls_code, category_name) values (4, (select category_no from store_menu_category where store_no = 4 and disp_order = '2'), 'ko', 'Noodle');

    // insert into store_menu_category (STORE_NO,CATEGORY_NO,DISP_ORDER,USE_YN) values (4,SEQ_CATEGORY_NO.NEXTVAL,'3','1');
    // insert into store_menu_category_nls (store_no, category_no, nls_code, category_name) values (4, (select category_no from store_menu_category where store_no = 4 and disp_order = '3'), 'ko', 'Rice');

    // insert into store_menu_category (STORE_NO,CATEGORY_NO,DISP_ORDER,USE_YN) values (4,SEQ_CATEGORY_NO.NEXTVAL,'4','1');
    // insert into store_menu_category_nls (store_no, category_no, nls_code, category_name) values (4, (select category_no from store_menu_category where store_no = 4 and disp_order = '4'), 'ko', 'Bowl of rice');

    // insert into store_menu_category (STORE_NO,CATEGORY_NO,DISP_ORDER,USE_YN) values (4,SEQ_CATEGORY_NO.NEXTVAL,'5','1');
    // insert into store_menu_category_nls (store_no, category_no, nls_code, category_name) values (4, (select category_no from store_menu_category where store_no = 4 and disp_order = '5'), 'ko', 'Main');

    let deletePromise = [];
    let connection = null;
    db.getConnection()
    .then(conn =>{
        connection = conn;
        deleteMenuAndCategory.forEach(_=>{
            deletePromise.push(db.query(_,connection));
        });
        return Promise.all(deleteMenuAndCategory);
    })
    .then(_=>{
        return connection.commit();
    })
    .then(_=>{
        connection.close().then();
        res.sendStatus(201);
    })
    .catch(err=>{
        if(connection)
            connection.close().then();
        console.log(err);
        res.status(500).send(err);
    });
});
router.get('/init',(req,res)=>{
    let storeNo = req.user.uid;
    let get_category = `
        select category_no "categoryNo", category_name "categoryName" from store_menu_category where store_no = ${storeNo} `;
    let insert_category = `
        insert into store_menu_category_nls (store_no, category_no, nls_code, category_name) 
        values (:storeNo,:categoryNo,:nlsCode,:categoryName)`;
    let get_menuList = `
        select menu_no "menuNo", menu_name "menuName" from store_menu where store_no = ${storeNo} `;
    let insert_menu = `
        insert into store_menu_nls (menu_no, nls_code, menu_name) 
        values (:menuNo, :nlsCode, :menuName) `;
    let connection = null;
    db.getConnection()
        .then(conn=>{
            connection = conn;
            return db.query(get_category);
        })
        .then((list:any)=>{
            let promiseArr = [];
            list.forEach(_=>{
                promiseArr.push(db.query(insert_category,connection,[storeNo, _.categoryNo, 'ko',_.categoryName]));
            });
            return Promise.all(promiseArr);
        })
        .then(_=>{
            return db.query(get_menuList);
        })
        .then((list:any)=>{
            let promiseArr = [];
            list.forEach(data=>{
                promiseArr.push(db.query(insert_menu, connection, [data.menuNo, 'ko', data.menuName]));
            });
            return Promise.all(promiseArr);
        })
        .then(_=>{
            return connection.commit();
        })
        .then(_=>{
            connection.close().then();
            res.sendStatus(201);
        })
        .catch(err=>{
            if(connection) connection.close().then();
            res.sendStatus(500);
        });
});

/**
 * 매장에 메뉴를 추가한다.
 */
router.post('/',function (req:any, res) {
    // error list
    // not enough params
    // Incorrect menuType
    console.log('POST menu');
    //수정시 필요한 parameter
    let upd_menuNo = req.body.menuNo? req.body.menuNo : null;
    let dispOrder = req.body.dispOrder;
    let storeNo = req.user.uid;
    let userNo=req.user.no;
    let menuCode=req.body.menuCode;
    let menuName=req.body.menuName;
    let nlsCode = req.body.nlsCode;
    let taxType=req.body.taxType;
    let taxAmt = 0;
    let unitPrice=req.body.unitPrice | 0; //setmenu일 경우는 보내지 않기 때문에
    let saleAmt=req.body.saleAmt;
    let categoryNo=req.body.categoryNo; //알아서 올리기
    let useYN=req.body.useYN;
    let stampUseYN=req.body.stampUseYN;
    let menuType=req.body.menuType; //1:싱글메뉴, 2: 세트메뉴
    let startDt=req.body.startDt;
    let endDt=req.body.endDt;
    let menuBgColor=(req.body.menuBgColor == undefined ? null : req.body.menuBgColor);
    let soldOutYN=(req.body.soldOutYN == undefined ? null : req.body.soldOutYN);
    let thumbnailS = (req.body.thumbnailS == undefined ? null : req.body.thumbnailS);
    let thumbnailM = (req.body.thumbnailM == undefined ? null : req.body.thumbnailM);
    let thumbnailL = (req.body.thumbnailL == undefined ? null : req.body.thumbnailL);
    let stockSaleYN = (req.body. stockSaleYN == undefined ? null : req.body. stockSaleYN);
    let stockCNT = (req.body.stockCNT == undefined ? null : req.body.stockCNT);
    let originTxt = (req.body.originTxt == undefined ? null : req.body.originTxt);
    let subList = (req.body.subList == undefined ? [] : req.body.subList); //subMenuNo, menuText, menuCNT
    let checkList = [menuCode,menuName,taxType,saleAmt, categoryNo, useYN, stampUseYN, menuType, startDt, endDt];

    for(let item of checkList){        
        if(item == undefined){
            res.status(400).send({message:"not enough params"});
            return;
        }
    }

    if(!Number.isSafeInteger(saleAmt) || !Number.isSafeInteger(unitPrice)){
        res.status(400).send({message:"가격이 부적절합니다."});
        return;
    }

    if(taxType == '1'){ // 세금 계산
        taxAmt = (saleAmt / 11) * 10;
        taxAmt = Math.round((Math.floor(taxAmt) / 10));
    }

    if(menuType != '1' && menuType != '2'){
        res.status(400).send({message:"Incorrect menuType"});
    }
    if(req.body.menuNo && !dispOrder){
        res.status(400).send({message:"업데이트시, menuNo와 함께 dispOrder도 추가로 보내주세요."});
        return;
    }
    
    // if(stockSaleYN == '0')
    //     soldOutYN = '0';
        
    let menuQ= `select SEQ_MENU_NO.NEXTVAL "menuNo" from dual `;
    let mergeMenu =`
        merge into STORE_MENU using dual on (MENU_NO = :upd_menuNo) 
        when matched then
        update set 
            MENU_CODE = :menuCode, 
            TAX_TYPE = :taxType, 
            TAX_AMT = :taxAmt, 
            UNIT_PRICE = :unitPrice, 
            SALE_AMT = :saleAmt, 
            USE_YN = :useYN, 
            STAMP_USE_YN = :stampUseYN, 
            MENU_TYPE = :menuType, 
            START_DT = to_date(:startDt,'yyyy-mm-dd'),
            END_DT = to_date(:endDt,'yyyy-mm-dd'), 
            CATEGORY_NO = :categoryNo, 
            DISP_ORDER = :dispOrder,
            REG_USER_NO = :userNo, 
            MENU_BG_COLOR = :menuBgColor, 
            SOLDOUT_YN = :soldOutYN, 
            UPD_DTTM = sysdate,
            THUMBNAIL_S = :thumbnailS,
            THUMBNAIL_M = :thumbnailM,
            THUMBNAIL_L = :thumbnailL,
            STOCK_SALE_YN = :stockSaleYN, 
            STOCK_CNT = :stockCnt, 
            ORIGIN_TXT = :originTxt 
        when not matched then 
            insert 
                (MENU_NO, MENU_CODE, TAX_TYPE, TAX_AMT, UNIT_PRICE, SALE_AMT, USE_YN, STAMP_USE_YN, MENU_TYPE, 
                START_DT, END_DT, STORE_NO, CATEGORY_NO,REG_USER_NO, MENU_BG_COLOR, REG_DTTM, UPD_DTTM, DISP_ORDER, 
                SOLDOUT_YN, THUMBNAIL_S, THUMBNAIL_M, THUMBNAIL_L, STOCK_SALE_YN, STOCK_CNT, ORIGIN_TXT) 
            VALUES 
                (:menuNo, :menuCode, :taxType, :taxAmt, :unitPrice, :saleAmt, :useYN, :stampUseYN, :menuType, 
                to_date(:startDt,'yyyy-mm-dd'),to_date(:endDt,'yyyy-mm-dd'), :storeNo, :categoryNo, :regUserNo,
                :menuBgColor, sysdate, sysdate, (select coalesce(max(DISP_ORDER),1) from STORE_MENU where STORE_NO=:storeNo and CATEGORY_NO=:categoryNo)+1, 
                :soldoutYN, :thumbnailS, :thumbnailM, :thumbnailL,:stockSaleYN, :stockCNT, :originTxt)`;

    let merge_menuNls = `
        merge into store_menu_nls using dual on (menu_no = :upd_menuNo and nls_code = :nlsCode) 
        when matched then 
            update set menu_name = :menuName
        when not matched then 
            insert (MENU_NO, NLS_CODE, MENU_NAME) 
            values (:menuNo, :nlsCode, :menuName) `;

    let update_unitprice = `
        update store_menu 
        set UNIT_PRICE = nvl((select sum(a.SALE_AMT*b.menu_cnt) 
                          from 
                             store_menu a, store_set_menu b 
                          where b.SET_MENU_NO= :menuNo and b.SUB_MENU_NO = a.menu_no and a.use_yn='1'), 0) 
        where menu_no =:menuNo`;

    let menuNo = null;
    let connection = null;
    let returnVal = null;
    res.setHeader('Content-Type', 'application/json');
    db.getConnection()
    .then(conn=>{
        connection = conn;
        if(req.body.menuNo)
            return [];
        else
            return db.query(menuQ, connection);
    })
    .then(menuQ_result =>{      
        if(!req.body.menuNo)
            menuNo = menuQ_result[0].menuNo;
        return db.query(mergeMenu, connection,[upd_menuNo,  menuCode,   taxType,    taxAmt,     unitPrice,  saleAmt,    useYN,
                                            stampUseYN,     menuType,   startDt,    endDt,      categoryNo, dispOrder,  userNo,
                                            menuBgColor,    soldOutYN,  thumbnailS, thumbnailM, thumbnailL, stockSaleYN,stockCNT,
                                            originTxt,      menuNo,     menuCode,   taxType,    taxAmt,     unitPrice,  saleAmt, 
                                            useYN,          stampUseYN, menuType,   startDt,    endDt,      storeNo,    categoryNo, 
                                            userNo,         menuBgColor,storeNo,    categoryNo, soldOutYN,  thumbnailS, thumbnailM, 
                                            thumbnailL,     stockSaleYN,stockCNT,   originTxt]);
    })
    .then(result=>{
        return db.query(merge_menuNls, connection, [upd_menuNo, nlsCode, menuName, menuNo, nlsCode, menuName]);
    })
    .then(result=>{
        let setInsertQueries = [];
        if(menuType == '1'){
            return result;
        }else if(menuType == '2'){
            for(let subMenu of subList){
                subMenu["menuText"] = (subMenu.menuText == undefined ? null : subMenu.menuText);    
                var new_menuNo;
                if(upd_menuNo)
                    new_menuNo = upd_menuNo;
                else   
                    new_menuNo = menuNo;
                let mergeSetMenu = `
                    merge into STORE_SET_MENU using dual on (SET_MENU_NO = ${new_menuNo} and SUB_MENU_NO = ${subMenu.subMenuNo})
                    when matched then 
                    UPDATE SET 
                        menu_cnt = :menuCNT,
                        menu_text = :menuText,
                        upd_dttm = sysdate  
                    when not matched then
                        insert 
                            (SET_MENU_NO, SUB_MENU_NO, MENU_CNT, MENU_TEXT, REG_DTTM, UPD_DTTM) 
                        values 
                            (${new_menuNo}, ${subMenu.subMenuNo}, :menuCNT, :menuText, sysdate, sysdate)`;             
                setInsertQueries.push(db.query(mergeSetMenu, connection, [subMenu.menuCNT,subMenu.menuText,subMenu.menuCNT,subMenu.menuText]));
            }
            return Promise.all(setInsertQueries);
        }else{
            throw "unexpected menuType err";
        }
    })
    .then(promise_all_result=>{
        if(menuType == '1'){
            return promise_all_result;
        }else{
            var new_menuNo;
            if(upd_menuNo)
                new_menuNo = upd_menuNo;
            else   
                new_menuNo = menuNo;
            return db.query(update_unitprice,connection,[new_menuNo, new_menuNo]);
        }
    })
    .then(upd_result=>{
        return connection.commit();
    })
    .then(end=>{
        connection.close().then();
        res.status(201).send({message:"insert set menu success"});
    })
    .catch(err=>{
        console.log(err.toString());
        if(connection)
            connection.close().then();
        res.status(500).send({message:err.toString()});
    });
});


/**
 * 매장에 메뉴 정보를 수정한다.
 */
router.put('/', (req:any, res) => {
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let menuNo=req.body.menuNo;
    let menuCode=req.body.menuCode;
    let menuName=req.body.menuName;
    let taxType=req.body.taxType;
    let taxAmt = 0;
    let unitPrice=req.body.unitPrice | 0;
    let saleAmt=req.body.saleAmt;
    let stampUseYN=req.body.stampUseYN;
    let menuType=req.body.menuType;
    let startDt=req.body.startDt;
    let endDt=req.body.endDt;
    let categoryNo=req.body.categoryNo;
    let dispOrder = req.body.dispOrder;
    let menuBgColor=(req.body.menuBgColor == undefined ? null : req.body.menuBgColor);
    let soldOutYN=(req.body.soldOutYN == undefined ? null : req.body.soldOutYN);
    let useYN=req.body.useYN;
    let thumbnailS = (req.body.thumbnailS == undefined ? null : req.body.thumbnailS);
    let thumbnailM = (req.body.thumbnailM == undefined ? null : req.body.thumbnailM);
    let thumbnailL = (req.body.thumbnailL == undefined ? null : req.body.thumbnailL);
    let stockSaleYN = (req.body. stockSaleYN == undefined ? null : req.body. stockSaleYN);
    let stockCNT = (req.body.stockCNT == undefined ? null : req.body.stockCNT);
    let originTxt = (req.body.originTxt == undefined ? null : req.body.originTxt);
    let parameterList = [storeNo,userNo, menuNo, menuCode, menuName, taxType, unitPrice, saleAmt, stampUseYN, 
                        menuType, startDt, endDt, categoryNo, dispOrder, menuBgColor, soldOutYN, useYN, thumbnailL,
                        thumbnailM, thumbnailS, stockCNT, originTxt];
    for(let parameter of parameterList){
        if(parameter === undefined){
            res.status(400).send({message:"parameter missing"});
            return;
        }
    }
    
    if(!Number.isSafeInteger(saleAmt) || !Number.isSafeInteger(unitPrice)){
        res.status(400).send({message:"가격이 부적절합니다."});
        return;
    }

    if(taxType == '1'){ // 세금 계산
        taxAmt = (saleAmt / 11) * 10;
        taxAmt = Math.round((Math.floor(saleAmt) / 10));
    }
    
    let updateMenu =
        `UPDATE STORE_MENU 
         SET
            MENU_CODE = '${menuCode}', 
            MENU_NAME = '${menuName}', 
            TAX_TYPE = '${taxType}', 
            TAX_AMT = ${taxAmt}, 
            UNIT_PRICE = ${unitPrice}, 
            SALE_AMT = ${saleAmt}, 
            USE_YN = '${useYN}', 
            STAMP_USE_YN = '${stampUseYN}', 
            MENU_TYPE = '${menuType}', 
            START_DT = to_date('${startDt}','yyyy-mm-dd'),
            END_DT = to_date('${endDt}','yyyy-mm-dd'), 
            CATEGORY_NO = ${categoryNo}, 
            DISP_ORDER = '${dispOrder}',
            REG_USER_NO = ${userNo}, 
            MENU_BG_COLOR = :menuBgColor, 
            SOLDOUT_YN = :soldOutYN, 
            UPD_DTTM = sysdate,
            THUMBNAIL_S = :thumbnailS,
            THUMBNAIL_M = :thumbnailM,
            THUMBNAIL_L = :thumbnailL,
            STOCK_SALE_YN = :stockSaleYN, 
            STOCK_CNT = :stockCnt, 
            ORIGIN_TXT = :originTxt  
        where MENU_NO = ${menuNo} and STORE_NO = ${storeNo}`;

    db.query(updateMenu,null,[menuBgColor,soldOutYN,thumbnailS,thumbnailM,thumbnailL,stockSaleYN,stockCNT,originTxt])
    .then(()=>{
        res.status(201).send({message:"menu update success"});
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err});
    });
});

/**
 * 메뉴 수정 v2
 */
router.put('/v2', async (req,res)=>{
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let body = req.body;
    body.saleAmt = +body.saleAmt;
    body.taxAmt = (body.saleAmt / 11) * 10;
    body.taxAmt = Math.round((Math.floor(body.taxAmt) / 10));
    let upd_sql = `
        update  store_menu 
        set     upd_dttm = sysdate `;
    let param = [];
    if(body.saleAmt){
        param.push(body.saleAmt);
        upd_sql += ` , sale_amt = :saleAmt `;
    }
    if(body.taxAmt){
        param.push(body.taxAmt);
        upd_sql += ` , tax_amt = (case tax_type when '1' then :taxAmt else 0 end) `
    }
    if(body.dispOrder){
        param.push(body.dispOrder);
        upd_sql += ` , disp_order = :dispOrder `
    }
    if(body.soldOutYN){
        param.push(body.soldOutYN);
        upd_sql += ` , soldout_yn = :soldOutYN `
    }
    if(body.thumbnailS){
        param.push(body.thumbnailS);
        param.push(body.thumbnailM);
        param.push(body.thumbnailL);
        upd_sql += ` , thumbnail_s = :thumbnailS, thumbnail_m = :thumbnailM, thumbnail_l = :thumbnailL `;
    }
    upd_sql += ` where menu_no = :menuNo `;
    param.push(body.menuNo?body.menuNo:body.id);
        
    let upd_name = `
        update store_menu_nls 
        set     menu_name = :menuName 
        where   menu_no = :menuNo and nls_code = :nlsCode `;

    let connection;
    try{
        connection = await db.getConnection();
        
        if(param.length > 1){
            await db.query(upd_sql, connection, param);
        }
        await db.query(upd_name, connection, [body.name, body.menuNo?body.menuNo:body.id, 'ko']);
        await connection.commit();
        await connection.close();
        res.sendStatus(201);
    }catch(err){
        console.error(err);
        if(connection) await connection.close();
        res.status(500).send({message:err});
    }
});
/**
 * 메뉴 추가 v2
 */
router.post('/v2', async (req,res)=>{
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let body = req.body;
    body.saleAmt = +body.saleAmt;
    body.taxAmt = (body.saleAmt / 11) * 10;
    body.taxAmt = Math.round((Math.floor(body.taxAmt) / 10));

    let get_menuNo = `select SEQ_MENU_NO.NEXTVAL "menuNo" from dual `;
    let insert_sql = `
        insert into store_menu 
            (MENU_NO,   MENU_CODE,    TAX_TYPE,     TAX_AMT,    UNIT_PRICE, 
            SALE_AMT,   USE_YN,       STAMP_USE_YN, MENU_TYPE,  START_DT, 
            END_DT,     STORE_NO,     REG_USER_NO,  REG_DTTM,   UPD_DTTM, 
            thumbnail_s,thumbnail_m,  thumbnail_l,  soldout_yn, category_no) 
        values 
            (:MENU_NO, :MENU_CODE,    :TAX_TYPE,    :TAX_AMT,   :UNIT_PRICE, 
            :SALE_AMT, :USE_YN,       :STAMP_USE_YN,:MENU_TYPE, sysdate, 
            sysdate + (INTERVAL '50' YEAR),:STORE_NO,:REG_USER_NO,sysdate, sysdate,
            :thumbnailS,:thumbnailM,:thumbnailL,:soldOutYN ,:categoryNo) `;
    let insert_menu_nls = `
        insert into store_menu_nls 
            (menu_no, nls_code, menu_name) 
        values 
            (:menuNo, :nlsCode, :menuName) `;
    let connection = null;
    try{
        connection = await db.getConnection();
        let menuNo = await db.query(get_menuNo,connection);
        menuNo = menuNo[0].menuNo;
        await db.query(insert_sql, connection, [menuNo, menuNo, '1', body.taxAmt, body.saleAmt, 
                                         body.saleAmt, '1', '0', '1', storeNo, userNo, body.thumbnailS, body.thumbnailS, body.thumbnailS, '0', body.categoryNo]);
        await db.query(insert_menu_nls, connection, [menuNo, 'ko', body.name]);
        await connection.commit();
        connection.close().then();
        res.sendStatus(201);
    }catch(err){
        console.error(err);
        if(connection) connection.close().then();
        res.status(500).send({message:err});
    }
    
});

/**
 * 메뉴 휴지통에 있는 메뉴(useYN ==0) 에 있는 메뉴들만 가지고 온다.
 */
router.get('/bin',(req:any,res)=>{
    let storeNo = req.user.uid;
    let getBinMenu = `
        select 
            MENU_NO as "menuNo", MENU_NAME as "menuName", SALE_AMT as "saleAmt",
            THUMBNAIL_S "thumbnailS", THUMBNAIL_M "thumbnailM", THUMBNAIL_L "thumbnailL"     
         from STORE_MENU 
         where 
            STORE_NO = :storeNo 
            and USE_YN = '0' 
        order by UPD_DTTM desc`;
    db.query(getBinMenu,null,[storeNo])
    .then(binMenus=>{
        res.status(200).send(binMenus);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 메뉴의 useYN을 toggle 한다 (use_yn == 0 휴지동 use_yn == 1 사용 use_yn == 2 메뉴잠금 use_yn == 3 완전 삭제)
 */
router.put('/bin',(req:any,res)=>{
    let storeNo = req.user.uid;
    let menuNo = req.body.menuNo;
    let idDuplicatedReq = `
        select 24*60*60*(sysdate - upd_dttm) as diffTime from store_menu where store_no = :storeNo and menu_no=:menuNo`;
    let changeUseYn = `
        update STORE_MENU 
        set 
            USE_YN = case when USE_YN = '0' then '1' else '0' end, 
            UPD_DTTM = sysdate 
        where STORE_NO = :storeNo and MENU_NO = :menuNo`;
    db.query(idDuplicatedReq,null,[storeNo, menuNo])
    .then(time=>{
        if(time[0].diffTime < 10)
            throw "already deleted menu";
        else 
            return db.query(changeUseYn,null,[storeNo, menuNo])
    })    
    .then(()=>{
        res.status(201).send({message:"update success"});
    })
    .catch(err=>{
        console.log(err);
        if(typeof err == 'string')
            res.status(400).send({message:err});
        else
            res.status(500).send({message:err});
    });
});

/**
 * 메뉴를 완전 삭제한다 (휴지통 비우기)
 */
router.delete('/bin',(req:any,res)=>{
    let storeNo = req.user.uid;
    let menuNo = req.query.menuNo;
    let dlt_menu = `
        update STORE_MENU 
        set 
            USE_YN = '3' 
        where 
            STORE_NO = ${storeNo} and MENU_NO = ${menuNo}`;
        
    db.query(dlt_menu)
    .then(dlt_result=>{
        res.status(201).send({message:"delete success"});
    })
    .catch(err=>{
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * 현재 주문 가능한 메뉴를 보여준다. 
 * categoryNo가 -1일 경우 분류 없이 모두 가져온다
 */
router.get('/order', (req:any, res) => {
    let storeNo = req.user.uid;
    let categoryNo = req.query.categoryNo;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let get_menu =
        `   select x.* 
                from 
                (SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
                 FROM 
                    (select 
                        MENU_NO as "menuNo",
                        MENU_CODE as "menuCode",
                        MENU_NAME as "menuName", 
                        MENU_NAME_EN as "menuNameEn", 
                        TAX_TYPE as "taxType", 
                        TAX_AMT as "taxAmt", 
                        UNIT_PRICE  as "unitPrice", 
                        SALE_AMT  as "saleAmt", 
                        USE_YN as "useYN", 
                        STAMP_USE_YN as "stampUseYN", 
                        MENU_TYPE as "menuType",  
                        SOLDOUT_YN as "soldOutYN",
                        to_char(START_DT,'yyyy-mm-dd')  as "startDt", 
                        to_char(END_DT,'yyyy-mm-dd') as "endDt", 
                        CATEGORY_NO as "categoryNo", 
                        to_char(REG_DTTM,'yyyy-mm-dd hh24:mi:ss') as "regDttm",
                        REG_USER_NO as "regUserNo", 
                        MENU_BG_COLOR as "menuBgColor", 
                        DISP_ORDER as "dispOrder",
                        STOCK_SALE_YN as "stockSaleYN",
                        STOCK_CNT as "stockCNT",
                        THUMBNAIL_S as "thumbnailS",
                        THUMBNAIL_M as "thumbnailM",
                        THUMBNAIL_L as "thumbnailL", 
                        ORIGIN_TXT as "originTxt" 
                    from store_menu 
                    where 
                        store_no = ${storeNo} 
                        and use_yn= '1' 
                        and SOLDOUT_YN = '0' 
                        and sysdate between START_DT and END_DT `;
                        
    if(categoryNo !="-1"){
        get_menu += `   and CATEGORY_NO = ${categoryNo} `;
    }
    get_menu += `   ) y 
                ) x 
            WHERE x."page" = ${pageNo}` ;

    let get_total =
        `select count(MENU_NO) as "total" 
         from store_menu 
         where 
            store_no = ${storeNo} 
            and use_yn= '1' 
            and SOLDOUT_YN = '0' 
            and sysdate between START_DT and END_DT `;
    if(categoryNo !="-1"){
        get_total += ` 
            and CATEGORY_NO =${categoryNo} `;
    }
    
    let orderList : any = [];
    db.query(get_menu)
    .then(menus=>{
        orderList = menus;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:orderList,
            total:total[0].total
        });
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * soldout_yn을 수정한다.
 */
router.put('/soldout', (req:any, res) => {
    // error list
    // not enough params
    let menuNo=req.body.menuNo
    let soldOutYN=req.body.soldOutYN;
    
    let storeNo = req.user.uid;
    let userNo = req.user.no;

    if(!menuNo || !soldOutYN){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let query = `
        UPDATE STORE_MENU SET
            soldout_yn = '${soldOutYN}',
            UPD_DTTM = sysdate  
        where menu_no = ${menuNo}`;
    db.query(query)
    .then(result=>{
        res.sendStatus(201);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 한가지 메뉴 정보만 가져온다.
 */
router.get('/:menuNo',(req:any,res)=>{
    console.log(req.params)
    let menuNo = req.params.menuNo;
    let storeNo = req.user.uid;
    
    let query = `
        select 
            MENU_NO as "menuNo", 
            MENU_CODE as "menuCode", 
            MENU_NAME as "menuName",  
            MENU_NAME_EN as "menuNameEn",  
            TAX_TYPE as "taxType", 
            TAX_AMT as "taxAmt", 
            UNIT_PRICE  as "unitPrice", 
            SALE_AMT  as "saleAmt", 
            USE_YN as "useYN", 
            STAMP_USE_YN as "stampUseYN", 
            MENU_TYPE as "menuType",  
            SOLDOUT_YN as "soldOutYN",  
            to_char(START_DT,'yyyy-mm-dd')  as "startDt", 
            to_char(END_DT,'yyyy-mm-dd') as "endDt", 
            CATEGORY_NO as "categoryNo", 
            to_char(REG_DTTM,'yyyy-mm-dd hh24:mi') as "regDttm", 
            to_char(UPD_DTTM,'yyyy-mm-dd hh24:mi') as "updDttm", 
            REG_USER_NO  as "regUserNo", 
            MENU_BG_COLOR  as "menuBgColor", 
            DISP_ORDER  as "dispOrder", 
            THUMBNAIL_S as "thumbnailS", 
            THUMBNAIL_M as "thumbnailM", 
            THUMBNAIL_L as "thumbnailL", 
            STOCK_SALE_YN as "stockSaleYN", 
            STOCK_CNT as "stockCNT" 
        from store_menu 
        where menu_no = ${menuNo}`;
    db.query(query)
    .then(menuInfo=>{
        res.status(200).send(menuInfo);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

module.exports = router;