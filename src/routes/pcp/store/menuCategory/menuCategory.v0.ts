/**
 * Created by sdh077 on 17. 3. 9.
 */
import { Router } from 'express';
let router = Router();
let jwt  = require("jsonwebtoken");
import { db } from  '../../../../data-base/db';
import { DEFAULT_NLS } from '../../../../config';
import { nvl } from '../../../../lib';
/**
 * 매장의 메뉴 분류 정보를 가져온다
 */
router.get('/',(req, res) => {
    let storeNo = req.user.uid;
    let nlsCode = req.query.nlsCode;
    let get_category = `
        select 
            category_no "categoryNo",
            tab_color_rgb "tabColorRgb",
            disp_order "dispOrder" 
        from store_menu_category 
        where 
            store_no=:storeNo 
            and use_yn = '1' 
        order by disp_order`;
    let get_name = `
        select  category_name "name", nls_code "nlsCode" 
        from    store_menu_category_nls 
        where   store_no = :storeNo 
                and nls_code = (case when :nlsCode is null 
                                     then (select nvl(nls_code,'ko') from store_info where store_no = :storeNo)
                                     else :nlsCode end)
                and category_no = :categoryNo `;
    let categorys = null;
    db.query(get_category, null, [storeNo])
    .then(category=>{
        categorys = category;
        let promiseArr = [];
        categorys.forEach(_=>{
            promiseArr.push(db.query(get_name,null,[storeNo,nlsCode,storeNo,nlsCode,_.categoryNo]));
        })
        return Promise.all(promiseArr);
    })
    .then(name => {
        for(let idx = 0; idx < categorys.length; idx++){
            categorys[idx].categoryName = name[idx];
        }
        res.status(200).send(categorys);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err.toString()});
    });
});

/**
 * 카테고리 가져오기 (토큰 없이)
 */
router.get('/simple',(req, res) => {
    let storeNo = req.query.storeNo;
    let nlsCode = req.query.nlsCode?req.query.nlsCode:'ko';
    let get_category = `
        select 
            a.category_no "categoryNo",
            a.disp_order "dispOrder",
            b.category_name "categoryName" 
        from store_menu_category a, store_menu_category_nls b  
        where 
            a.store_no=:storeNo 
            and a.store_no = b.store_no 
            and a.use_yn = '1' 
            and a.category_no = b.category_no 
            and b.nls_code  = (case when :nlsCode is null 
                                    then (select nvl(nls_code,'ko') from store_info where store_no = :storeNo)
                                    else :nlsCode end)
        order by a.disp_order`;
    let categorys = null;
    db.query(get_category, null, [storeNo, nlsCode, storeNo, nlsCode])
    .then(category=>{
        res.status(200).send(category);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err.toString()});
    });
});
/**
 * 매장에 메뉴 분류를 추가한다.
 */
router.post('/',(req, res) => {
    console.log('POST menu category')
    let categoryName = (!req.body.categoryName ? null : req.body.categoryName);
    let tabColorRgb = (!req.body.tabColorRgb ? null : req.body.tabColorRgb);
    let nlsCode = req.body.nlsCode; //ko(한국어), en(영어), ja(일본어), cn(중국어)
    let categoryNo = req.body.categoryNo;
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let get_categoryNo = `
        select SEQ_CATEGORY_NO.NEXTVAL "categoryNo" from dual `;
    let insert_category = `
        INSERT 
            INTO STORE_MENU_CATEGORY 
            ( CATEGORY_NO, CATEGORY_NAME, STORE_NO, DISP_ORDER, TAB_COLOR_RGB, USE_YN) 
        VALUES 
            ( :categoryNo, :categoryName, :id, 
            (select coalesce(max(DISP_ORDER),1) from store_menu_category where STORE_NO=:id)+1, :tabColorRgb, '1')`;
    let insert_nls = `
        insert into store_menu_category_nls 
            (store_no, category_no, nls_code, category_name) 
        values 
            (:storeNo, :categoryNo, :nlsCode, :categoryName) `;
    let connection = null;
    db.getConnection()
        .then(conn=>{  
            connection = conn;
            if(categoryNo) return;
            else return db.query(get_categoryNo);
        })
        .then(no=>{
            if(no) categoryNo = no[0].categoryNo;
            return db.query(insert_category, connection, [categoryNo, categoryName,storeNo, storeNo, tabColorRgb]);
        })
        .then(result=>{
            if(!nlsCode){//nlsCode == null이면 store_info값으로 default
                return db.query(`select nls_code "nlsCode" from store_info where store_no = :storeNo `, connection, [storeNo]);
            }else{//nlsCode가 맞는 코드인지 확인해야할듯
                return;
            }
        })
        .then(code =>{
            if(code){
                nlsCode = code[0].nlsCode?code[0].nlsCode : DEFAULT_NLS;
            }
            return db.query(insert_nls, connection, [storeNo, categoryNo, nlsCode, categoryName]);
        })
        .then(_=>{
            return connection.commit();
        })
        .then(_=>{
            connection.close().then();
            res.sendStatus(201);
        })
        .catch(err=>{
            console.error(err);
            if(connection)
                connection.close().then();
            res.status(500).send({message:err.toString()});
        });
});
 
/**
 * 매장의 한가지 메뉴 분류를 수정한다.
 */
router.put('/',(req, res) => {
    console.log('PUT menu category')
    // error list
    // not enough params
    let categoryNo = nvl(req.body.categoryNo);
    let nlsCode = nvl(req.body.nlsCode);
    let categoryName = nvl(req.body.categoryName);
    let dispOrder = nvl(req.body.dispOrder);
    console.log(JSON.stringify(req.body));
    console.log(req.user.uid);
    // let categoryNameEN = req.body.categoryNameEN;

    let storeNo = req.user.uid;
    let userNo=req.user.no;

    if(!categoryNo || !categoryName){
        res.status(400).send({message:"not enough params"});
        return;
    }
    let upd_category = null;
    if(dispOrder){
        upd_category = `
            update store_menu_category set disp_order = :dispOrder where store_no = :storeNo and category_no = :categoryNo `;
    }
    let upd_cateName = null;
    if(categoryName){
        upd_cateName = `
            update store_menu_category_nls set category_name = :categoryName where store_no =:storeNo and category_no = :categoryNo `;
    }
    let connection = null;
    db.getConnection()
    .then(conn=>{
        connection = conn;
        if(categoryName && !nlsCode){
            return db.query('select nls_code "nlsCode" from store_info where store_no = :storeNo ');
        }else{
            return;
        }
    })
    .then(code =>{
        if(code){
            nlsCode = code[0].nlsCode?code[0].nlsCode:DEFAULT_NLS;
        }
        let promiseArr = [];
        if(dispOrder){
            promiseArr.push(db.query(upd_category,connection,[dispOrder, storeNo, categoryNo]));
        }
        if(categoryName){
            promiseArr.push(db.query(upd_cateName, connection,[categoryName, storeNo, categoryNo]));
        }
        return Promise.all(promiseArr);
    })
    .then(result=>{
        return connection.commit();
    })
    .then(_=>{
        connection.close().then();
        res.sendStatus(201);
    })
    .catch(err=>{
        if(connection) connection.close().then();
        res.status(500).send({message:err});
    });
});

/**
 * 매장의 한가지 메뉴 분류를 삭제한다
 */
router.delete('/',(req, res) => {

    let categoryNo = req.query.categoryNo;
    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let dlt_menu = `
        UPDATE STORE_MENU set USE_YN = '0' WHERE STORE_NO = :storeNo and CATEGORY_NO = :categoryNo`;
    let dlt_category = `
        update STORE_MENU_CATEGORY set use_yn = '0' WHERE STORE_NO = :storeNo and CATEGORY_NO = :categoryNo`;

    let connection = null;
    db.getConnection()
    .then(conn=>{
        connection=conn;
        return db.query(dlt_menu,connection,[storeNo,categoryNo]);
    })
    .then(dlt_cate_result=>{
        return db.query(dlt_category,connection,[storeNo,categoryNo]);
    })
    .then(dlt_menu_result=>{
        return connection.commit();
    })
    .then(end=>{
        connection.close().then();
        res.sendStatus(201);
    })
    .catch(err=>{
        console.log(err);
        if(connection)
            connection.close().then();
        res.status(500).send({message:err.toString()});
    });
});

module.exports = router;