# Pohang point > pos > point

## 내용
1. 포인트조회 GET /pos/point
   
    > 매장에서 발행,사용한 포인트 거래내역을 가져온다
   
    ###request
        가맹점번호	storeNo
        조회시작일	startDt
        조회종료일	endDt
        페이지	pageNo
        페이지당행수	pageSize
        
    ###response
        총수	total
        배포	publish
        사용	payment
        페이지	list.page
        내역	list.pointLog
        사용자번호	list.userNo
        발생일자	list.tranDttm
        성명	list.userName
        전화번호	list.tel
        적립포인트	list.savingPoint
        사용포인트	list.usingPoint
        
2. 포인트거래
   POST /pos/point
   
    >매장에서 포인트 발행,결제내역을 등록한다.
   
    ###request
        사용자번호	userNo
        전화번호	tel
        거래유형	tranType
        거래포인트	tranPoint
        사업지구번호	areaNo
        가맹점번호	storeNo    
        
  
3. 포인트 조회 발행 pos/point/publish
   
    > 매장에서 발행 포인트 거래내역을 가져온다
   
    ###request
        가맹점번호	storeNo
        조회시작일	startDt
        조회종료일	endDt
        페이지	pageNo
        페이지당행수	pageSize
        
    ###response
        총수	total
        배포	publish
        사용	payment
        페이지	list.page
        내역	list.pointLog
        사용자번호	list.userNo
        발생일자	list.tranDttm
        성명	list.userName
        전화번호	list.tel
        적립포인트	list.savingPoint
        사용포인트	list.usingPoint
        
4. 포인트 조회 발행 pos/point/payment
   
    > 매장에서 사용 포인트 거래내역을 가져온다
   
    ###request
        가맹점번호	storeNo
        조회시작일	startDt
        조회종료일	endDt
        페이지	pageNo
        페이지당행수	pageSize
        
    ###response
        총수	total
        배포	publish
        사용	payment
        페이지	list.page
        내역	list.pointLog
        사용자번호	list.userNo
        발생일자	list.tranDttm
        성명	list.userName
        전화번호	list.tel
        적립포인트	list.savingPoint
        사용포인트	list.usingPoint
            
4. 포인트 조회 발행 pos/point/payment
   
    > 매장에서 사용 포인트 거래내역을 가져온다
   
    ###request
        가맹점번호	storeNo
        조회시작일	startDt
        조회종료일	endDt
        페이지	pageNo
        페이지당행수	pageSize
        
    ###response
        총수	total
        배포	publish
        사용	payment
        페이지	list.page
        내역	list.pointLog
        사용자번호	list.userNo
        발생일자	list.tranDttm
        성명	list.userName
        전화번호	list.tel
        적립포인트	list.savingPoint
        사용포인트	list.usingPoint
        