/**
 * Created by genesis on 2017. 2. 9..
 */
/**
 * Created by genesis on 2017. 1. 23..
 */
import { Router } from 'express';
let router = Router();
let oracledb = require('oracledb');

let lib = require('../../../../lib');
let doRelease = lib.doRelease;
let oracleData = lib.oracleData;
let inflating = lib.inflating;

router.get('/',(req, res) => {
    console.log('pos point get')
    let d1, d2, d3;

    let pageSize = req.query.pageSize;
    let page = req.query.pageNo;
    let storeNo = req.user.uid;
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;

    let query1 =
        'select x.* from (' +
        ' (select sum(MILEAGE_POINT) as "publish" from user_mileage_log where store_no=' + storeNo + ' and tran_type=1 and tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\'))  union all' +
        ' (select sum(MILEAGE_POINT) as "payment" from user_mileage_log where store_no=' + storeNo + ' and tran_type=2 and tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\'))  )x'



    let query2 =
        'select x.*' +
        ' from ( SELECT CEIL(ROWNUM/' + pageSize + ') AS "page", y.*' +
        ' FROM ( ' +
        ' select' +
        ' b.user_no as "userNo", ' +
        ' to_char(a.tran_dttm,\'yyyy-mm-dd hh24:mi\') as "tranDttm",' +
        ' b.user_name as "userName",b.tel as "tel",' +
        ' case when a.tran_type = \'1\' then a.MILEAGE_POINT' +
        ' when a.tran_type = \'2\' then 0 end as "savingMileage",' +
        ' case when a.tran_type = \'1\' then 0' +
        ' when a.tran_type = \'2\' then a.MILEAGE_POINT end as "usingMileage"' +
        ' from user_mileage_log a,' +
        ' user_master b' +
        ' where 1=1' +
        ' and a.TRAN_TYPE in (1,2) ' +
        ' and a.store_no = ' + storeNo +
        ' and a.user_no = b.user_no(+)' +
        ' and a.tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\')' +
        ' order by a.tran_dttm desc ) y) x' +
        ' WHERE x."page" = ' + page;

    let query3 =
        'select count(*) as "total" ' +
        ' from user_mileage_log a,' +
        ' user_master b' +
        ' where 1=1' +
        ' and a.tran_type in (1,2) ' +
        ' and a.store_no = ' + storeNo +
        ' and a.user_no = b.user_no(+)' +
        ' and a.tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\')' +
        ' order by a.tran_dttm desc '


    oracleData(query1, (call) => {
        d1 = inflating(call);
        oracleData(query2, (call) => {
            d2 = inflating(call);
            oracleData(query3, (call) => {
                d3 = inflating(call);
                res.json(
                    {
                        total:d3[0].total,
                        publish: d1[0].publish,
                        payment: d1[1].publish,
                        list: d2
                    }
                )
            });
        });
    });
});
router.post('/',(req, res) => {

    console.log('pcp mileage post')

    let userNo = req.body.userNo;
    let storeNo = req.user.uid;
    let mileagePoint = req.body.mileagePoint;
    let tranType = req.body.tranType;
    let salesAmt = req.body.salesAmt;
    let saveRatio = req.body.saveRatio;
    let payType = req.body.payType;


    let query = 'insert into user_mileage_log (mileage_no, user_no, store_no,TRAN_DTTM,TRAN_TYPE,SALES_AMT, PAY_TYPE,SAVE_RATIO,MILEAGE_POINT) values ' +
        '(SEQ_MILEAGE_NO.NEXTVAL,'+userNo+','+storeNo+',sysdate,'+tranType+','+salesAmt+','+payType+','+saveRatio+','+mileagePoint+')';
    let query1 =
        'MERGE INTO user_mileage' +
        ' USING DUAL' +
        ' ON (user_no = '+userNo+' and store_no = '+storeNo+')' +
        ' WHEN MATCHED THEN' +
        ' UPDATE SET mileage_total        = NVL(((select sum(MILEAGE_POINT) from user_mileage_log where user_no = '+userNo+' and store_no = '+storeNo +' and tran_type=1)-(select sum(MILEAGE_POINT) from user_mileage_log where user_no = '+userNo+' and store_no = '+storeNo +' and tran_type=2),0)+ '+mileagePoint+
        ' WHEN NOT MATCHED THEN' +
        ' INSERT   (' +
        ' user_no, store_no,mileage_total)' +
        ' VALUES (' +
        ' '+userNo+','+storeNo+',0'+
        ' )';
    let query2 =
      'MERGE INTO store_favorite' +
      ' USING DUAL' +
      ' ON (user_no = '+userNo+' and store_no = '+storeNo+')' +
      ' WHEN MATCHED THEN' +
      ' UPDATE SET USE_YN        = 1 '+
      ' WHEN NOT MATCHED THEN' +
      ' INSERT   (' +
      ' user_no, store_no, reg_dttm,use_yn,tran_cnt)' +
      ' VALUES (' +
      ' '+userNo+','+storeNo+',sysdate,1,0'+
      ' )';
    let connection = null;
    oracledb.getConnection()
        .then((conn) => {
            connection = conn;
            return connection.execute(query1);
        })
        .then(() => {
            return connection.execute(query);
        })
        .then(() => {
            return connection.execute(query2);
        })
        .then(() => {
            return connection.commit().then();
        })
        .then(() => {
            return connection.close();
        })
        .then(() => {
            res.sendStatus(200)
        })
        .catch((err) => {
            console.log(err)
            if (connection) {
                connection.close().then(() => {
                    res.status(404).send(err);
                });
            } else {
                res.status(404).send(err);
            }

        });

});

router.get('/publish',(req, res) => {

    let d1, d2, d3;
    let pageSize = req.query.pageSize;
    let page = req.query.pageNo;
    let storeNo = req.user.uid;
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;

    console.log(req.query);


    console.log(Date());

    let query1 =
        'select x.* from (' +
        ' (select sum(tran_point) as "publish" from membership_point where store_no=' + storeNo + ' and tran_type=1 and tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\'))  union all' +
        ' (select sum(tran_point) as "payment" from membership_point where store_no=' + storeNo + ' and tran_type=2 and tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\'))  )x'

    // res.send(query1);


    let query2 =
        'select x.*' +
        ' from ( SELECT CEIL(ROWNUM/' + pageSize + ') AS "page", y.*' +
        ' FROM ( select a.POINT_LOG as "pointLog" , b.user_no as "userNo", ' +
        ' to_char(a.tran_dttm,\'yyyy-mm-dd hh24:mi\') as "tranDttm",' +
        ' b.user_name as "userName",nvl(a.tel,b.tel) as "tel",' +
        ' case when a.tran_type = \'1\' then a.tran_point' +
        ' when a.tran_type = \'2\' then 0 end as "savingPoint",' +
        ' case when a.tran_type = \'1\' then 0' +
        ' when a.tran_type = \'2\' then a.tran_point end as "usingPoint"' +
        ' from membership_point a,' +
        ' user_master b' +
        ' where 1=1' +
        ' and a.TRAN_TYPE in (1) ' +
        ' and a.store_no = ' + storeNo +
        ' and a.user_no = b.user_no(+)' +
        ' and a.tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\')' +
        ' order by a.tran_dttm desc ) y) x' +
        ' WHERE x."page" = ' + page;

    let query3 =
        'select count(a.point_no) as "total" ' +
        ' from membership_point a,' +
        ' user_master b' +
        ' where 1=1' +
        ' and a.tran_type in (1) ' +
        ' and a.store_no = ' + storeNo +
        ' and a.user_no = b.user_no(+)' +
        ' and a.tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\')' +
        ' order by a.tran_dttm desc '

    // res.send(query3);

    oracleData(query1, (call) => {
        d1 = inflating(call);
        oracleData(query2, (call) => {
            d2 = inflating(call);
            oracleData(query3, (call) => {
                d3 = inflating(call);
                res.json(
                    {
                        total:d3[0].total,
                        publish: d1[0].publish,
                        payment: d1[1].publish,
                        list: d2
                    }
                )
            });
        });
    });
});
router.get('/payment',(req, res) => {


    let d1, d2, d3;

    let pageSize = req.query.pageSize;
    let page = req.query.pageNo;
    let storeNo = req.user.uid;
    let startDt = req.query.startDt;
    let endDt = req.query.endDt;

    console.log(req.query);


    console.log(Date());

    let query1 =
        'select x.* from (' +
        ' (select sum(tran_point) as "publish" from membership_point where store_no=' + storeNo + ' and tran_type=1 and tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\'))  union all' +
        ' (select sum(tran_point) as "payment" from membership_point where store_no=' + storeNo + ' and tran_type=2 and tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\'))  )x'

    // res.send(query1);


    let query2 =
        'select x.*' +
        ' from ( SELECT CEIL(ROWNUM/' + pageSize + ') AS "page", y.*' +
        ' FROM ( select a.POINT_LOG as "pointLog" , b.user_no as "userNo", ' +
        ' to_char(a.tran_dttm,\'yyyy-mm-dd hh24:mi\') as "tranDttm",' +
        ' b.user_name as "userName",nvl(a.tel,b.tel) as "tel",' +
        ' case when a.tran_type = \'1\' then a.tran_point' +
        ' when a.tran_type = \'2\' then 0 end as "savingPoint",' +
        ' case when a.tran_type = \'1\' then 0' +
        ' when a.tran_type = \'2\' then a.tran_point end as "usingPoint"' +
        ' from membership_point a,' +
        ' user_master b' +
        ' where 1=1' +
        ' and a.TRAN_TYPE in (2) ' +
        ' and a.store_no = ' + storeNo +
        ' and a.user_no = b.user_no(+)' +
        ' and a.tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\')' +
        ' order by a.tran_dttm desc ) y) x' +
        ' WHERE x."page" = ' + page;

    let query3 =
        'select count(a.point_no) as "total" ' +
        ' from membership_point a,' +
        ' user_master b' +
        ' where 1=1' +
        ' and a.tran_type in (2) ' +
        ' and a.store_no = ' + storeNo +
        ' and a.user_no = b.user_no(+)' +
        ' and a.tran_dttm between to_date(\'' + startDt + ' 00:00\', \'yyyy-mm-dd hh24:mi\') and ' +
        'to_date(\'' + endDt + ' 23:59\',\'yyyy-mm-dd hh24:mi\')' +
        ' order by a.tran_dttm desc '

    // res.send(query3);

    oracleData(query1, (call) => {
        d1 = inflating(call);
        oracleData(query2, (call) => {
            d2 = inflating(call);
            oracleData(query3, (call) => {
                d3 = inflating(call);
                res.json(
                    {
                        total:d3[0].total,
                        publish: d1[0].publish,
                        payment: d1[1].publish,
                        list: d2
                    }
                )
            });
        });
    });
});

module.exports = router;