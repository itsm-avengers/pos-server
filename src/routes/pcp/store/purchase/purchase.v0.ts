/**
 * Created by sdh077 on 17. 3. 14.
 */
import { Router } from 'express';
import { db } from '../../../../data-base/db';
let router = Router();
let jwt  = require("jsonwebtoken");

/**
 * 매입 관리 내역을 가져온다
 */
router.get('/',(req, res) => {
    // error list
    // not enough params

    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;

    if(!pageNo || !pageSize){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let get_data = `
        select x.* 
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            FROM ( 
                select 
                    PURCHASE_NO as "purchaseNo",
                    IN_DTTM as "inDttm",
                    IN_CONTEXT as "inContext",
                    IN_AMT as "inAmt",
                    IN_TAX as "inTax",
                    IN_CASH as "inCash",
                    IN_CARD as "inCard",
                    SLIP_NO as "slipNo",
                    REG_DTTM as "regDttm", 
                    CUSTOMER_NO as "customerNo" 
                from store_purchase 
                where store_no = ${storeNo} and REG_USER_NO=${userNo} 
                ) y 
             ) x 
        WHERE x."page" = ${pageNo}`;

    let get_total = `
        select 
            count(PURCHASE_NO) as "total"
        from store_purchase
        where store_no = ${storeNo} and REG_USER_NO=${userNo}`;

    let list = [];
    db.query(get_data)
    .then(data=>{
        list = data;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:list,
            total:total[0].total
        });
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 매입 관리 내역을 추가한다.
 */
router.post('/',(req, res) => {
    // error list

    let inDttm = req.body.inDttm;
    let inContext = req.body.inContext;
    let inAmt = req.body.inAmt;
    let inTax = req.body.inTax;
    let inCash = req.body.inCash;
    let inCard = req.body.inCard;
    let slipNo = req.body.slipNo;
    let regDttm = req.body.regDttm;
    let customerNo = req.body.customerNo;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let query = `
        INSERT INTO STORE_PURCHASE 
            ( PURCHASE_NO,IN_DTTM,IN_CONTEXT,IN_AMT,IN_TAX,IN_CASH,IN_CARD,SLIP_NO,REG_DTTM,REG_USER_NO,CUSTOMER_NO,STORE_NO) 
         VALUES 
            (SEQ_PURCHASE_NO.NEXTVAL,to_date('${inDttm}','yyyy-mm-dd hh24:mi:ss'),'${inContext}',
            ${inAmt},${inTax},${inCash},${inCard},'${slipNo}',SYSDATE,${userNo},${customerNo},${storeNo})`;
    console.log(query);
    db.query(query)
    .then(result=>{
        res.sendStatus(201);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 매입 관리 내역을 수정한다.
 */
router.put('/',(req, res) => {
    // error list

    let purchaseNo =req.body.purchaseNo;
    let inDttm =req.body.inDttm;
    let inContext =req.body.inContext;
    let inAmt =req.body.inAmt;
    let inTax =req.body.inTax;
    let inCash =req.body.inCash;
    let inCard =req.body.inCard;
    let slipNo =req.body.slipNo;
    let customerNo =req.body.customerNo;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let query =`
        UPDATE STORE_PURCHASE SET 
            SLIP_NO       = '${slipNo}',
            IN_CARD         = ${inCard},
            REG_USER_NO     = ${userNo},
            IN_CASH         = ${inCash} ,
            CUSTOMER_NO     = ${customerNo},
            IN_AMT          = ${inAmt},
            IN_DTTM         = to_date('${inDttm}','yyyy-mm-dd hh24:mi:ss'),
            IN_CONTEXT      = '${inContext}',
            IN_TAX          = ${inTax} 
        WHERE PURCHASE_NO = ${purchaseNo}`;
    console.log(query);
    db.query(query)
    .then(result=>{
        res.sendStatus(201);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 매입 관리를 삭제한다.
 */
router.delete('/',(req, res) => {
    let purchaseNo =req.query.purchaseNo;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let query =`
        DELETE FROM STORE_PURCHASE WHERE PURCHASE_NO = ${purchaseNo}`;
    db.query(query)
    .then(result=>{
        res.sendStatus(201);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

module.exports = router;