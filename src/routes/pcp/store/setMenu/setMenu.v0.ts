/**
 * Created by sdh077 on 17. 3. 17.
 */
import * as express from 'express';
import * as oracledb from 'oracledb';

let router = express.Router();
let db = require('../../../../data-base/db').db;
let jwt  = require("jsonwebtoken");

/**
 * 세트메뉴안에 포함된 서브 메뉴들의 목록을 가져온다
 */
router.get('/sub',(req:any, res) => {
    let storeNo = req.user.uid;
    let menuNo = req.query.menuNo;
    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;

    if(pageSize == undefined || pageSize == 0){
      res.status(400).send({message:"division by zero err"});
      return;
    }

    let query =
        `select x.*
         from  
            (SELECT CEIL(ROWNUM/:pageSize) AS "page", y.* 
            FROM 
                (select 
                    a.SET_MENU_NO as "setMenuNo", a.SUB_MENU_NO as "subMenuNo", 
                    a.MENU_TEXT as "menuText", a.menu_cnt as "menuCNT", 
                    to_char(a.REG_DTTM,'yyyy-mm-dd hh24:mi') as "regDttm",
                    to_char(a.UPD_DTTM,'yyyy-mm-dd hh24:mi') as "updDttm" 
                from store_set_menu a, store_menu b 
                where 
                    b.store_no = :storeNo 
                    and a.SUB_MENU_NO = b.menu_no 
                    and a.set_menu_no =:menuNo
                ) y  
            ) x 
          WHERE x."page" = :pageNo`;

    let cnt_query = `
          select 
              count(a.SET_MENU_NO) as "total" 
          from store_set_menu a, store_menu b 
          where 
              b.store_no = :storeNo 
              and a.SUB_MENU_NO = b.menu_no 
              and a.set_menu_no =:menuNo`;

    let setmenu = null;
    
    db.query(query,null,[pageSize,storeNo,menuNo,pageNo])
    .then(result=>{
      setmenu = result;
      db.query(cnt_query,null,[storeNo,menuNo]);
    })
    .then(cnt=>{
      res.status(200).send({
        list:setmenu,
        total:cnt[0].total
      });
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 세트메뉴에 서브 메뉴를 추가한다.
 */
router.post('/sub',(req, res) => {
    let menuNo = req.body.menuNo;
    let list = req.body.list;
    // let subMenuNo = req.body.subMenuNo;
    // let menuCNT = (req.body.menuCNT == undefined? null : req.body.menuCNT);
    // let menuText = (req.body.menuText == undefined? null : req.body.menuText);
    
    //세트메뉴의 단가는 메뉴의 가격 합한거, 판매가는 실제로 판매할 금액(세트라서 할인된 가격)
    let update_unitprice = `
        update store_menu 
        set 
            UNIT_PRICE = (select sum(a.SALE_AMT*b.menu_cnt) 
                            from 
                            store_menu a, store_set_menu b 
                            where b.SET_MENU_NO= :menuNo and b.SUB_MENU_NO = a.menu_no) 
        where menu_no =:menuNo`;
    let connection = null;
    db.getConnection()
    .then(conn=>{
        connection = conn;
        let mergeArr = [];
        for(let item of list){
            let merge_query = `
                MERGE INTO store_set_menu using dual on (set_menu_no = ${menuNo} and sub_menu_no = ${item.subMenuNo}) 
                WHEN MATCHED THEN 
                    UPDATE SET 
                        menu_cnt = ${item.menuCNT},
                        menu_text = '${item.menuText}',
                        upd_dttm = sysdate        
                WHEN NOT MATCHED THEN 
                    INSERT (set_menu_no, sub_menu_no, menu_cnt, menu_text, reg_dttm, upd_dttm) 
                    VALUES (${menuNo}, ${item.subMenuNo}, ${item.menuCNT}, '${item.menuText}', sysdate, sysdate)`;
            mergeArr.push(db.query(merge_query,connection));
        }
        return Promise.all(mergeArr);
    })
    .then(mergeAll_result=>{
        return db.query(update_unitprice,connection,[menuNo]);
    })  
    .then(upd_unitprice_result=>{
        return connection.commit();
    })
    .then(end=>{
        connection.close().then();
        res.sendStatus(201);
    })
    .catch(err=>{
        if(connection)
            connection.close().then();
        res.status(500).send({message:err});
    });
});

/**
 * 세트메뉴에 서브 메뉴를 제거한다.
 */
router.delete('/sub',(req, res) => {

    let menuNo = req.query.menuNo;
    let subMenuNo = req.query.subMenuNo;

    let dlt_submenu = `
        DELETE 
        FROM STORE_set_MENU 
        WHERE 
            SET_MENU_NO =:menuNo 
            and sub_menu_no =:subMenuNo`;
    let update_unitprice = `
        update store_menu 
        set UNIT_PRICE = (select sum(a.SALE_AMT*b.menu_cnt) 
        from 
            store_menu a, store_set_menu b where b.SET_MENU_NO= :menuNo and b.SUB_MENU_NO = a.menu_no) 
        where menu_no =:menuNo`;

    db.query(dlt_submenu,null,[menuNo,subMenuNo])
    .then(dlt_result=>{
      return db.query(update_unitprice,null,[menuNo,menuNo]);
    })
    .then(upd_result=>{
      res.status(201).send({message:"delete submenu success"});
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

module.exports = router;