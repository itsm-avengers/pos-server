/**
 * Created by sdh077 on 17. 3. 14.
 */
import { Router } from 'express';
import { db } from '../../../data-base/db';
import { nvl } from '../function';
let router = Router();
let oracledb = require('oracledb');
let jwt  = require("jsonwebtoken");
let lib = require('../../../lib');
let doRelease = lib.doRelease;
let oracleData = lib.oracleData;
let inflating = lib.inflating;
/**
 * 매장정보
 */
router.get('/',(req,res) => {
    console.log('get store!!!!!!!!');
    
    let storeNo = req.user.uid;
    let userNo = req.user.no;
    let query =`
        select a.store_no as "storeNo",
            a.store_name as "storeName",
            a.store_id as "storeID",
            a.pos_type as "posType",
            a.store_disp_name as "storeDispName",
            a.ceo_name as "ceoName",
            a.tel as "tel",
            a.store_category as "storeCategory",
            f_get_codename('200',a.store_category) as "categoryName",
            a.reg_dt as "regDt",
            a.approval_dt as "approvalDt",
            a.stamp_saving_type as "stampSavingType",
            a.area_no as "areaNo",
            c.area_name as "areaName",
            a.service_status as "serviceStatus",
            b.service_time_text as "serviceTimeText",
            b.latitude as "lat",
            b.longitute as "lng",
            b.campaign_text as "campaignText",
            b.store_text as "storeText",
            b.directions as "directions",
            b.membership_text as "membershipText",
            b.hash_tag as "hashTag",
            b.parking_info as "parking",
            a.addr1 as "addr1",
            a.addr2 as "addr2",
            a.zipcode as "zipcode",
            a.point_saving_ratio as "pointSavingRatio",
            d.login_account as "loginAccount",
            e.tel as "hp",
            a.POINT_SAVE_CASH as "pointSaveCash",
            a.POINT_SAVE_CARD as "pointSaveCard",
            a.MILEAGE_SAVE_CASH as "mileageSaveCash",
            a.MILEAGE_SAVE_CARD as "mileageSaveCard",
            a.MILEAGE_STAMP_OPT as "mileageStampOpt",
            b.PROMOTION_IMG as "promotionImg",
            b.POS_BACK_URL as "backgroundImg" 
        from store_info a, store_detail b, service_area c, user_store d, user_master e 
        where 
            a.store_no = b.store_no 
            and c.area_no = a.area_no 
            and e.user_no = d.user_no 
            and d.emp_type = 1 
            and d.store_no = a.store_no
            and a.store_no = ${storeNo} `;
            
    db.query(query)
    .then(store_info=>{
        res.status(200).send(store_info[0]);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err});
    });
});

/**
 * 매장 추가
 */
router.post('/',(req, res) => {

    console.log(req.body)
    console.log('pos store post');

    let storeNo = req.body.storeNo;
    let zipcode = req.body.zipcode;
    let addr1 = req.body.addr1;
    let addr2 = req.body.addr2;
    let storeDispName = req.body.storeDispName;
    let tel = req.body.tel;
    let codeName = req.body.codeName;
    let posType = req.body.posType;
    let storeCategory = req.body.storeCategory;
    let serviceStatus = req.body.serviceStatus;
    let storeText = req.body.storeText;
    let areaNo = req.body.areaNo;
    let pointSavingRatio = req.body.pointSavingRatio;
    let stampSavingType = req.body.stampSavingType;
    let campaignText = req.body.campaignText;
    let serviceTimeText = req.body.serviceTimeText;
    let ceoName = req.body.ceoName;
    let directions = req.body.directions;
    let parkingInfo = req.body.parking;
    let membershipText = req.body.membershipText;
    let hashTag = req.body.hashTag;
    let lat = req.body.lat;
    let lng = req.body.lng;
    let pointSaveCash = req.body.pointSaveCash;
    let pointSaveCard = req.body.pointSaveCard;
    let mileageSaveCash = req.body.mileageSaveCash;
    let mileageSaveCard = req.body.mileageSaveCard;
    let mileageStampOpt = req.body.mileageStampOpt;
    let posNm = req.body.posNm;
    let posBackUrl = req.body.posBackUrl;
    oracledb.getConnection(
        (err, connection) =>
        {
            if (err) { console.error(err.message); res.send(err.message);
            }
            else
            connection.execute(
                "BEGIN PROC_STORE_UPDATE(" +
                ":i_storeNo ," +
                ":i_zipcode                  ," +
                ":i_addr1                  ," +
                ":i_addr2                  ," +
                ":i_storeDispName                  ," +
                ":i_tel                  ," +
                ":i_storeCategory                  ," +
                ":i_serviceStatus                  ," +
                ":i_pointSavingRatio                  ," +
                ":i_stampSavingType                  ," +
                ":i_campaignText                  ," +
                ":i_storeText                  ," +
                ":i_serviceTimeText                  ," +
                ":i_lat                  ," +
                ":i_lng                  ," +
                ":i_directions                  ," +
                ":i_parkingInfo                  ," +
                ":i_membershipText                  ," +
                ":i_hashTag                  ," +
                ":i_pointSaveCash ," +
                ":i_pointSaveCard, "+
                ":i_mileageSaveCash  ,"+
                ":i_mileageSaveCard  ,"+
                ":i_mileageStampOpt  ,    "+
                ":i_posNm, "+
                ":i_posBackUrl, "+
                ":o_result" +
                "); END;",
                {
                    i_storeNo                 :storeNo,
                    i_zipcode                   :zipcode,
                    i_addr1                     :addr1,
                    i_addr2                     :addr2,
                    i_storeDispName             :storeDispName,
                    i_tel                       :tel,
                    i_storeCategory             :storeCategory,
                    i_serviceStatus             :serviceStatus,
                    i_pointSavingRatio          :pointSavingRatio,
                    i_stampSavingType           :stampSavingType,
                    i_campaignText              :campaignText,
                    i_storeText                 :storeText,
                    i_serviceTimeText           :serviceTimeText,
                    i_lat                       :lat,
                    i_lng                       :lng,
                    i_directions                :directions,
                    i_parkingInfo               :parkingInfo,
                    i_membershipText            :membershipText,
                    i_hashTag                   :hashTag,
                    i_pointSaveCash             :pointSaveCash,
                    i_pointSaveCard             :pointSaveCard,
                    i_mileageSaveCash           :mileageSaveCash,
                    i_mileageSaveCard           :mileageSaveCard,
                    i_mileageStampOpt           :mileageStampOpt,
                    i_posNm                     :posNm,
                    i_posBackUrl                :posBackUrl, 
                  o_result                    :{dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 }
                },
                (err, result) =>
                {
                    console.log(result);
                    if (err) {
                        console.error(err.message);
                        res.status(404).send({state:"fail"});
                    }
                    else {
                      console.log(result)
                        if(result.outBinds.o_result == 'OK')
                        {
                            doRelease(connection)
                            console.log(result);
                            res.send({state:"success"});
                        } else if(result.outBinds.o_result.includes('ERRORORA-12899: value too large for column "POHANGPOINT"."STORE_DETAIL"."HASH_TAG"')){
                            doRelease(connection)
                            console.log(result);
                            res.status(404).send({state:"HASH_TAG"});
                        }else if(result.outBinds.o_result.includes('ERRORORA-12899: value too large for column "POHANGPOINT"."STORE_DETAIL"."CAMPAIGN_TEXT"')){
                            doRelease(connection)
                            console.log(result);
                            res.status(404).send({state:"CAMPAIGN_TEXT"});
                        }else if(result.outBinds.o_result.includes('ERRORORA-12899: value too large for column "POHANGPOINT"."STORE_DETAIL"."STORE_TEXT"')){
                            doRelease(connection)
                            console.log(result);
                            res.status(404).send({state:"STORE_TEXT"});
                        }else if(result.outBinds.o_result.includes('ERRORORA-12899: value too large for column "POHANGPOINT"."STORE_DETAIL"."SERVICE_TIME_TEXT"')){
                            doRelease(connection)
                            console.log(result);
                            res.status(404).send({state:"SERVICE_TIME_TEXT"});
                        }else if(result.outBinds.o_result.includes('ERRORORA-12899: value too large for column "POHANGPOINT"."STORE_DETAIL"."DIRECTIONS"')){
                            doRelease(connection)
                            console.log(result);
                            res.status(404).send({state:"DIRECTIONS"});
                        }else if(result.outBinds.o_result.includes('ERRORORA-12899: value too large for column "POHANGPOINT"."STORE_DETAIL"."PARKING_INFO"')){
                            doRelease(connection)
                            console.log(result);
                            res.status(404).send({state:"PARKING_INFO"});
                        }else if(result.outBinds.o_result.includes('ERRORORA-12899: value too large for column "POHANGPOINT"."STORE_DETAIL"."MEMBERSHIP_TEXT"')){
                            doRelease(connection)
                            console.log(result);
                            res.status(404).send({state:"MEMBERSHIP_TEXT"});
                        }

                    }
                });
        });
});

/**
 * 매장 정보 수정
 */
router.put('/',(req, res) => {

    console.log('store put');

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let storeName = req.body.storeName;
    let storeID = req.body.storeID;
    let posType = req.body.posType;
    let storeDispName = req.body.storeDispName;
    let storeCategory = req.body.storeCategory;
    let ceoName = req.body.ceoName;
    let tel = req.body.tel; //store_tel
    let stampSavingType = req.body.stampSavingType;
    let areaNo = req.body.areaNo;
    let serviceStatus = req.body.serviceStatus;
    let serviceTimeText = req.body.serviceTimeText;
    let lat = req.body.lat;
    let lng = req.body.lng;
    let campaignText = req.body.campaignText;
    let storeText = req.body.storeText;
    let directions = req.body.directions;
    let membershipText = req.body.membershipText;
    let hashTag = req.body.hashTag;
    let parkingInfo = req.body.parking;
    let addr1 = req.body.addr1;
    let addr2 = req.body.addr2;
    let zipcode = req.body.zipcode;
    let pointSavingRatio = req.body.pointSavingRatio;
    let pointSaveCash = req.body.pointSaveCash;
    let pointSaveCard = req.body.pointSaveCard;
    let mileageSaveCash = req.body.mileageSaveCash;
    let mileageSaveCard = req.body.mileageSaveCard;
    let mileageStampOpt = req.body.mileageStampOpt;
    
    let upd_storeInfo = `
        update store_info set 
            tel = :tel , addr1 =:addr1, addr2=:addr2, area_no=:areaNo, store_category =:storeCategory,
            ceo_name =:ceoName, POINT_SAVE_CASH =:pointSaveCash,POINT_SAVE_CARD =:pointSaveCard, pos_type =:posType,
            service_status =:serviceStatus, stamp_saving_type =:stampSavingType,STORE_NAME=:storeName,
            STORE_ID=:storeID, ZIPCODE =:zipcode, STORE_DISP_NAME=:storeDispName,UPD_DTTM =sysdate, POINT_SAVING_RATIO=:pointSavingRatio,
            MILEAGE_SAVE_CASH=:mileageSaveCash, MILEAGE_SAVE_CARD=:mileageSaveCard, MILEAGE_STAMP_OPT=:mileageStampOpt 
        where store_no =${storeNo} `;
    let upd_storeDetail = `
        update store_detail set 
            CAMPAIGN_TEXT = :campaignText,
            STORE_TEXT = :storeText,
            SERVICE_TIME_TEXT = :serviceTimeText,
            DIRECTIONS =:directions,
            PARKING_INFO =:parkingInfo,
            MEMBERSHIP_TEXT=:membershipText,
            HASH_TAG=:hashTag, 
            UPD_DTTM=sysdate,
            LATITUDE = :lat,
            LONGITUTE = :lng 
        where store_no = ${storeNo}`;
    let connection = null;
    db.getConnection()
    .then(conn=>{
        connection = conn;
        return db.query(upd_storeInfo,connection, [tel,addr1,addr2,areaNo,storeCategory,
                                                    ceoName,pointSaveCash,pointSaveCard,posType,
                                                    serviceStatus,stampSavingType, storeName, storeID, 
                                                    zipcode, storeDispName,pointSavingRatio,
                                                    mileageSaveCash,mileageSaveCard,mileageStampOpt]);
    })
    .then(upd_userMaster_result=>{
        return db.query(upd_storeDetail,connection,[campaignText,storeText,serviceTimeText,directions,
                                                    parkingInfo,membershipText,hashTag,lat,lng]);
    })
    .then(upd_storeDetail_result=>{
        return connection.commit();
    })
    .then(end=>{
        connection.close().then();
        res.sendStatus(201);
    })
    .catch(err=>{
        if(connection)
        connection.close().then();
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * 매장 정보 수정 /v2
 */
router.put('/v2', async (req, res)=>{
    console.log('메뉴 정보 수정');
    let storeNo = req.user.uid;
    let storeDispName = nvl(req.body.storeDispName);//store_info
    let storeID = nvl(req.body.storeID);//store_info
    let ceoName = nvl(req.body.ceoName);//store_info
    let tel = nvl(req.body.tel);//store_info
    let storeName = nvl(req.body.storeName);//store_info
    let serviceStatus = nvl(req.body.serviceStatus);//store_info
    let posType = nvl(req.body.posType);//store_info
    let mileageStampOpt = nvl(req.body.mileageStampOpt);//store_info
    let promotionImg = nvl(req.body.promotionImg);//store_detail
    let backgroundImg = nvl(req.body.backgroundImg);//store_detail
    let param = [];
    let upd_store_detail = `
        update  store_detail 
        set     promotion_img = :promotionImg,
                POS_BACK_URL = :backgroundImg,
                upd_dttm = sysdate 
        where   store_no = :storeNo `;
    let upd_store = `
        update  store_info 
        set     upd_dttm = sysdate `;
    if(storeDispName){
        param.push(storeDispName);
        upd_store += ` , STORE_DISP_NAME = :storeDispName `;
    }
    if(storeID){
        param.push(storeID);
        upd_store += ` , STORE_ID = :storeID `;
    }
    if(ceoName){
        param.push(ceoName);
        upd_store += ` , CEO_NAME = :ceoName `;
    }
    if(tel){
        param.push(tel);
        upd_store += ` , TEL = :tel `;
    }
    if(storeName){
        param.push(storeName);
        upd_store += ` , STORE_NAME = :storeName `;
    }
    if(serviceStatus){
        param.push(serviceStatus);
        upd_store += ` , SERVICE_STATUS = :serviceStatus `;
    }
    if(posType){
        param.push(posType);
        upd_store += ` , POS_TYPE = :posType `;
    }
    if(mileageStampOpt){
        param.push(mileageStampOpt);
        upd_store += ` , MILEAGE_STAMP_OPT = :mileageStampOpt `;
    }
    let connection = null;
    try{
        connection =  await db.getConnection();
        let upd_result;
        if(promotionImg)
            upd_result = await db.query(upd_store_detail, connection, [promotionImg, backgroundImg, storeNo]);
        upd_store += ` where store_no = :storeNo `;
        param.push(storeNo);
        if(param.length > 1)
            await db.query(upd_store, connection, param);
        await connection.commit();
        connection.close().then();
        res.sendStatus(201);
    }catch(err){
        console.error(err);
        if(connection) connection.close().then();
        res.status(500).send({message:err});
    }
});

// 매장 메뉴 카테고리 옵션 등등 복사! ㅅㅂ!
router.post('/copy', async (req,res)=>{
    console.log('COPY STORE')
    let storeNo = 603;//req.user.uid;
    let target_store = req.body.storeNo;
    let connection = null;
    try{
        connection = await db.getConnection();
        //get category list
        let categoryList = await db.query(`select * from store_menu_category where store_no = :storeNo`, null, [target_store]);
        //insert category list
        for(let item of categoryList){
            await db.query(`insert into store_menu_category (store_no, category_no, category_name, category_name_en, disp_order, tab_color_rgb, use_yn) 
                            values (:storeNo, :category_no, :category_name, :category_name_en, :disp_order, :tab_color_rgb, :use_yn)`, connection,
                            [storeNo, item.CATEGORY_NO, item.CATEGORY_NAME, item.CATEGORY_NAME_EN, item.DISP_ORDER, item.TAB_COLOR_RGB, item.USE_YN]);
        }
        //get category nls list
        let categoryNlsList = await db.query(`select * from store_menu_category_nls where store_no=:storeNo`, null, [target_store]);
        //insert category nls list
        for(let item of categoryNlsList){
            await db.query(`insert into store_menu_category_nls (store_no, category_no, nls_code, category_name) 
                            values (:storeNo, :category_no, :nls_code, :category_name)`, connection, 
                            [storeNo, item.CATEGORY_NO, item.NLS_CODE, item.CATEGORY_NAME]);
        }

        let menuNoPair = [];
        //get menu list
        let menuList = await db.query(`select * from store_menu where store_no = :storeNo`, null, [target_store]);
        //insert menu list

        for(let item of menuList){
            let newMenuNo = await db.query(`select SEQ_MENU_NO.NEXTVAL "no" from dual`);
            newMenuNo = newMenuNo[0].no;
            menuNoPair.push({new:newMenuNo, old:item.MENU_NO});
            await db.query(`insert into store_menu (MENU_NO, MENU_CODE, MENU_NAME, MENU_NAME_EN, TAX_TYPE, TAX_AMT, UNIT_PRICE, SALE_AMT, USE_YN, STAMP_USE_YN, MENU_TYPE, START_DT, END_DT, STORE_NO, CATEGORY_NO, REG_USER_NO, MENU_BG_COLOR, REG_DTTM, UPD_DTTM, DISP_ORDER, SOLDOUT_YN, THUMBNAIL_S, THUMBNAIL_M, THUMBNAIL_L, STOCK_SALE_YN, STOCK_CNT, ORIGIN_TXT) 
                                    values (:MENU_NO, :MENU_CODE, :MENU_NAME, :MENU_NAME_EN, :TAX_TYPE, :TAX_AMT, :UNIT_PRICE, :SALE_AMT, :USE_YN, :STAMP_USE_YN, :MENU_TYPE, :START_DT, :END_DT, :STORE_NO, :CATEGORY_NO, :REG_USER_NO, :MENU_BG_COLOR, :REG_DTTM, :UPD_DTTM, :DISP_ORDER, :SOLDOUT_YN, :THUMBNAIL_S, :THUMBNAIL_M, :THUMBNAIL_L, :STOCK_SALE_YN, :STOCK_CNT, :ORIGIN_TXT)`, connection, 
                                    [newMenuNo, item.MENU_CODE, item.MENU_NAME, item.MENU_NAME_EN, item.TAX_TYPE, item.TAX_AMT, item.UNIT_PRICE, item.SALE_AMT, item.USE_YN, item.STAMP_USE_YN, item.MENU_TYPE, item.START_DT, item.END_DT, storeNo, item.CATEGORY_NO, item.REG_USER_NO, item.MENU_BG_COLOR, item.REG_DTTM, item.UPD_DTTM, item.DISP_ORDER, item.SOLDOUT_YN, item.THUMBNAIL_S, item.THUMBNAIL_M, item.THUMBNAIL_L, item.STOCK_SALE_YN, item.STOCK_CNT, item.ORIGIN_TXT]);
            //해당 메뉴에 맞는 nls 가져오기
            let menuNlsList = await db.query(`select * from store_menu_nls where menu_no = :menuNo`, null, [item.MENU_NO]);
            //insert menu nls 
            for(let nlsItem of menuNlsList){
                await db.query(`insert into store_menu_nls (MENU_NO, NLS_CODE, MENU_NAME) 
                                values (:MENU_NO, :NLS_CODE, :MENU_NAME) `, connection,
                                [newMenuNo, nlsItem.NLS_CODE, nlsItem.MENU_NAME]);
            }

        }
        let optNoPair = [];
        //get store menu opt list
        let storeMenuOptList = await db.query(`select * from store_menu_opt where store_no = :storeNo`, null, [target_store]);
        //insert store menu opt
        for(let item of storeMenuOptList){
            let newMenuOptNo = await db.query(`select SEQ_MENU_OPT_NO.NEXTVAL "no" from dual`);
            newMenuOptNo = newMenuOptNo[0].no;
            optNoPair.push({new:newMenuOptNo, old:item.MENU_OPT_NO});
            await db.query(`insert into store_menu_opt (STORE_NO, MENU_OPT_NO, OPT_NAME, OPT_AMT, REG_DTTM, UPD_DTTM) 
                            values (:STORE_NO, :MENU_OPT_NO, :OPT_NAME, :OPT_AMT, sysdate, sysdate)`, connection, 
                            [storeNo, newMenuOptNo, item.OPT_NAME, item.OPT_AMT]);
        }
        //get menu opt list
        let menuOptList = await db.query(`select * from menu_opt where menu_no in (select menu_no from store_menu where store_no = :storeNo)`, null, [target_store]);
        //insert menu opt
        for(let item of menuOptList){
            let newMenuNo = getNewNoFromOld(menuNoPair, item.MENU_NO);
            let newOptNo = getNewNoFromOld(optNoPair, item.MENU_OPT_NO);
            if(newMenuNo && newOptNo)
                await db.query(`insert into menu_opt (MENU_NO, MENU_OPT_NO) values (:MENU_NO, :MENU_OPT_NO)`, connection, [newMenuNo, newOptNo]);
        }
        let groupNoPair = [];
        //get opt group list
        let optGroupList = await db.query(`select * from opt_group where store_no = :storeNo`, null, [target_store]);
        //insert opt group
        for(let item of optGroupList){
            let newGroupNo = await db.query(`select SEQ_OPT_GROUP_NO.NEXTVAL "no" from dual`);
            newGroupNo = newGroupNo[0].no
            groupNoPair.push({new:newGroupNo, old:item.GROUP_NO});
            await db.query(`insert into opt_group (GROUP_NO, STORE_NO, MULT_YN, NAME) 
                            values (:GROUP_NO, :STORE_NO, :MULT_YN, :NAME)`, connection, 
                            [newGroupNo, storeNo, item.MULT_YN, item.NAME]);
        }
        //get opt group item list
        let optGroupItemList = await db.query(`select * from opt_group_item where group_no in (select group_no from opt_group where store_no = :storeNo)`, null, [target_store]);
        //insert opt group item
        for(let item of optGroupItemList){
            let newGroupNo = getNewNoFromOld(groupNoPair, item.GROUP_NO);
            let newOptNo = getNewNoFromOld(optNoPair, item.MENU_OPT_NO);
            if(newGroupNo && newOptNo)
                await db.query(`insert into opt_group_item (group_no, menu_opt_no) 
                                values (:group_no, :menu_opt_no)`, connection, 
                                [newGroupNo, newOptNo]);
        }
        //get menu_opt_group
        let menuOptGroupList = await db.query(`select * from menu_opt_group where menu_no in (select menu_no from store_menu where store_no = :storeNo)`, null, [target_store]);
        for(let item of menuOptGroupList){
            let newGroupNo = getNewNoFromOld(groupNoPair, item.GROUP_NO);
            let newMenuNo = getNewNoFromOld(menuNoPair, item.MENU_NO);
            if(newGroupNo && newMenuNo)
                await db.query(`insert into menu_opt_group (GROUP_NO, MENU_NO, REG_DTTM) 
                                values (:GROUP_NO, :MENU_NO, sysdate)`, connection, 
                                [newGroupNo, newMenuNo]);
        }
        await connection.commit();
        connection.close().then();
        res.sendStatus(201);
    }catch(err){
        console.error(err);
        if(connection){
            connection.close().then();
        }
        res.status(500).send({message:err});
    }
});
function getNewNoFromOld(pair, no){
    let result = null;
    for(let item of pair){
        if(item.old == no){
            result = item.new;
            break;
        }
    }
    return result;
}
module.exports = router;