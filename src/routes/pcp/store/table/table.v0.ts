/**
 * Created by sdh077 on 17. 3. 9.
 */
import { Router } from 'express';
import { db } from '../../../../data-base/db';
import { pubsub } from '../../../../schema';
let router = Router();
let jwt  = require("jsonwebtoken");

/**
 * 모든 테이블 정보 조회를 조회한다.
 */
router.get('/',(req, res) => {
    let storeNo = req.user.uid;
    let query = `
      select 
        a.FLOOR_NO as "floorNo",
        a.TABLE_NO as "tableNo",
        a.TABLE_NAME as "tableName",
        a.SEAT_CNT as "seatCNT",
        a.DISP_ORDER as "dispOrder",
        b.FLOOR_NAME as "floorName",
        a.DISP_SHAPE as "dispShape"
      from store_table a, store_floor b
      where 
        a.store_no=${storeNo} 
        and b.FLOOR_NO = a.FLOOR_NO
        and b.store_no=${storeNo} 
      order by a.DISP_ORDER asc`;
    db.query(query)
    .then(table_info=>{
      res.status(200).send(table_info);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 해당 층의 테이블 정보를 조회한다.
 */
router.get('/:floorNo',(req, res) => {
    // error list
    // not enough params
    let storeNo = req.user.uid;
    let floorNo = req.params.floorNo;

    if(!floorNo){
      res.status(400).send({message:"not enough params"});
      return;
    }
    let query = `
      select 
        a.FLOOR_NO as "floorNo",
        a.TABLE_NO as "tableNo",
        a.TABLE_NAME as "tableName",
        a.SEAT_CNT as "seatCNT",
        a.DISP_ORDER as "dispOrder",
        b.floor_NAME as "floorName",
        a.DISP_SHAPE as "dispShape"
      from store_table a, store_floor b
      where 
        a.store_no = ${storeNo}
        and b.FLOOR_NO = a.FLOOR_NO
        and b.floor_no = ${floorNo}
        and b.store_no = ${storeNo} 
      order by a.DISP_ORDER asc `;
    db.query(query)
    .then(table_info=>{
      res.status(200).send(table_info);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 좌석수, 층번호, 테이블 이름을 통해 테이블을 추가한다.
 */
router.post('/',(req, res) => {
    let tableName = req.body.tableName;
    let seatCNT = req.body.seatCNT;
    let floorNo = req.body.floorNo;
    let dispShape = req.body.dispShape;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    if(!tableName || !floorNo){
      res.status(400).send({message:"not enough params"});
      return;
    }

    let insert_table = `
      INSERT INTO STORE_TABLE  
        (TABLE_NAME ,DISP_ORDER ,TABLE_NO ,SEAT_CNT ,FLOOR_NO ,STORE_NO, DISP_SHAPE) 
      VALUES 
        ('${tableName}', (select coalesce(max(DISP_ORDER),-1) from store_table where floor_no=${floorNo})+1, 
        SEQ_TABLE_NO.NEXTVAL ,${seatCNT}, ${floorNo},${storeNo},'${dispShape}')`;
    let upd_cnt=`
      update store_floor set 
        TABLE_CNT= (select count(*) from store_table where FLOOR_NO = ${floorNo}) 
      where FLOOR_NO = ${floorNo}`;
    
    let connection = null;
    db.getConnection()
    .then(conn=>{
      connection = conn;
      return db.query(insert_table,connection);
    })
    .then(insert_result=>{
      return db.query(upd_cnt,connection);
    })
    .then(upd_result=>{
      return connection.commit();
    })
    .then(end=>{
      connection.close().then();
      res.sendStatus(201);
    })
    .catch(err=>{
      if(connection)
        connection.close().then();
      res.status(500).send(err.toString());
    });
});

/**
 * 테이블의 이름, 좌석수, 순서를 수정한다.
 */
router.put('/',(req, res) => {
    // error list
    // not enough params

    let tableNo = req.body.tableNo;
    let tableName = req.body.tableName;
    let seatCNT = req.body.seatCNT;
    let dispOrder = req.body.dispOrder;
    let dispShape = req.body.dispShape;

    let storeNo = req.user.uid;
    let userNo = req.user.no;

    if(!tableNo || !tableName || (seatCNT == undefined) || (dispOrder==undefined) || !dispShape){
      res.status(400).send({message:"not enough params"});
      return;
    }
    let query = `
        UPDATE STORE_TABLE 
        SET 
          SEAT_CNT = ${seatCNT},
          DISP_ORDER = ${dispOrder},
          TABLE_NAME = '${tableName}',
          DISP_SHAPE = '${dispShape}' 
        WHERE TABLE_NO = ${tableNo} `;
    console.log(query);
    db.query(query)
    .then(result=>{
      res.sendStatus(201);
    })
    .catch(err=>{
      res.status(500).send({message:err});
    });
});

/**
 * 테이블을 삭제한다.
 */
router.delete('/',(req, res) => {
    let tableNo = req.query.tableNo;
    let floorNo = req.query.floorNo;

    let id=req.user.uid;
    let no=req.user.no;
    let order_check = `
      select 
        count(*) "cnt" 
      from ORDER_MASTER   
      where 
        ORDER_STATUS='1' 
        and :tableNo in ( 
            select regexp_substr(TABLE_NO_ARRAY,'[^|]+', 1, level) 
            from ORDER_MASTER 
            where order_status = '1' 
            connect by regexp_substr(TABLE_NO_ARRAY, '[^|]+', 1, level) is not null 
        ) `;
    let dlt_table = `
      DELETE FROM STORE_TABLE WHERE TABLE_NO = :tableNo`;
    let upd_tableCNT=`
      update store_floor set 
        TABLE_CNT= (select count(*) from store_table where FLOOR_NO = :floorNo)
      where FLOOR_NO = :floorNo `;
  let connection = null;
  db.getConnection()
  .then(conn=>{
    connection=conn;
    return db.query(order_check,connection,[tableNo]);
  })
  .then(cnt=>{
    if(cnt[0].cnt != 0){
      throw "order left";
    }else{
      return db.query(dlt_table,connection,[tableNo]);
    }
  })
  .then(dlt_result=>{
    return db.query(upd_tableCNT,connection,[floorNo,floorNo]);
  })
  .then(upd_result=>{
    return connection.commit();
  })
  .then(end=>{
    connection.close().then();
    res.sendStatus(201);
  })
  .catch(err=>{
    console.log(err);
    if(connection)
      connection.close().then();
    res.status(500).send({message:err});
  });
});

module.exports = router;