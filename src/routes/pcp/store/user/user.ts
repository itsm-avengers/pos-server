import { Router } from 'express';
import { db } from '../../../../data-base/db';
import * as oracledb from "oracledb";
let router = Router();

/**
 * 회원가입 여부 및 매장 단골 여부 조회
 * 회원 미가입 시, 가입처리
 */
router.post('/',(req,res)=>{
    console.log('POST pcp store user')
    let storeNo = req.user.uid;
    let userName = req.body.userName?req.body.userName:'임시이름';
    let tel = req.body.tel.replace('-','');
    console.log(typeof userName);
    if(typeof userName == "string")
        console.log(userName);
    else
        console.log(JSON.stringify(userName));
    console.log(typeof tel);
    if(typeof tel == "string")
        console.log(tel);
    else
        console.log(JSON.stringify(tel));

    let userNo = null;
    let visitCnt = 0;
    //회원 검색
    let findUser = `
        select user_no "userNo" from user_master where replace(tel,'-','') = :tel `;
    //비회원일시, 회원가입
    let addUser = `
        BEGIN PROC_USER_PERSON_INSERT(
            :i_userName,        sysdate,       :i_sex,
            :i_tel,             :i_residence,   :i_devicePlatform,
            :i_deviceModel,     :i_phoneOS,     :i_appVer,
            :i_deviceToken,     :i_accessKey,   :i_loginSite,
            :i_loginAccount,    :i_loginKey,    :i_image_url,
            :i_thumbnail_url, 
            :o_use_yn,          :o_user_name,   :o_login_site,
            :o_login_account,   :o_result 
        ); END; `;

    //매장 단골 여부 조회(방문 횟수)
    let getFavorite = `
        select tran_cnt "tranCnt" from store_favorite where user_no = :userNo and store_no = :storeNo `;
    //매장 방문 회수 추가 or 업데이트
    //지금은 tran_cnt 그냥 +1 해주지만 제대로할때는 결제 하고나면 tran_cnt 올려줘야할듯(-->이 함수를 결제 하고나서 호출하니 괜춘)
    let mergeFavorite = `
        merge into store_favorite using dual on (user_no = :userNo and store_no = :storeNo) 
        when matched then 
            update set 
                tran_cnt = tran_cnt + 1 
        when not matched then 
            insert (user_no, store_no, use_yn, tran_cnt)
            values (:userNo, :storeNo, '1', 1) `;
    let connection = null;
    let result = null;
    db.getConnection()
        .then(conn=>{
            connection = conn;
            return db.query(addUser,connection,[userName,'0',  
                                                tel,null,'cloudpos',  
                                                'cloudpos','cloudpos','cloudpos',  
                                                null,'cloudpos','cloudpos',  
                                                'cloudpos',null,null, 
                                                null,
                                                {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 },
                                                {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 },
                                                {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 },
                                                {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 },
                                                {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 }]);
        })
        .then((proc_result:any)=>{
            console.log(1)
            result = null;
            if(proc_result && proc_result.outBinds)
                result = proc_result.outBinds;  
            console.log(result);
            //회원 검색
            return db.query(findUser,connection,[tel]);
        })
        .then((no:any)=>{
            console.log(2)
            console.log(JSON.stringify(no));
            if(no && no.length > 0){
                userNo = no[0].userNo;
                return db.query(getFavorite,connection,[userNo,storeNo]);
            }else throw result;
        })
        .then(fav=>{
            console.log(3)
            if(fav[0])
                visitCnt = fav[0].tranCnt?fav[0].tranCnt : 0;
            //매장 방문 회수 추가 or 업데이트
            return db.query(mergeFavorite,connection,[userNo, storeNo]);
        })
        .then(_=>{
            console.log(4)
            return connection.commit();
        })
        .then(_=>{
            connection.close().then();
            res.status(201).send({userNo,visitCnt});
        })
        .catch(err=>{
            let status_code = 500;
            let err_msg = err?err.toString():"add user failed";
            console.log(err_msg);
            if(connection)
                connection.close().then();
            if(typeof err == "string")
                status_code = 400;
            res.status(status_code).send({message:err_msg});
        });
});

router.put('/visit',(req,res)=>{
    console.log('PUT store/visit -- 회원 삭제');
    // let tranCnt = 0;
    // let userNo = req.body.userNo;
    // let storeNo = req.user.uid;
    // let updTranCnt = `
    //     update store_favorite set tran_cnt = :tranCnt where user_no = :userNo and store_no = :storeNo`;
    // db.query(updTranCnt,null,[tranCnt,userNo,storeNo])
    // .then(_=>{
    //     res.sendStatus(201);
    // })
    // .catch(err=>{
    //     res.status(500).send({message:err.toString()});
    // });
    let userNo = req.body.userNo;
    let dlt_tables = ['user_mileage_log','user_mileage','store_favorite','user_person','user_master'];
    let dlt_queries = [];
    dlt_tables.forEach(tableName=>{
        dlt_queries.push(`delete from ${tableName} where user_no=${userNo}`);
    })
    let connection = null;
    db.getConnection()
    .then(conn=>{
        connection = conn;
        return db.query(dlt_queries[0],connection);
    })
    .then(_=>{
        return db.query(dlt_queries[1],connection);
    })
    .then(_=>{
        return db.query(dlt_queries[2],connection);
    })
    .then(_=>{
        return db.query(dlt_queries[3],connection);
    })
    .then(_=>{
        return db.query(dlt_queries[4],connection);
    })
    .then(_=>{
        return connection.commit();
    })
    .then(_=>{
        connection.close().then();
        res.sendStatus(201);
    })
    .catch(err=>{
        if(connection) connection.close().then();
        console.log(JSON.stringify(err));
        res.status(500).send(err);
    })
});
module.exports = router;