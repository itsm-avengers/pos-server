import * as express from 'express';
import * as request from 'request-promise';
import * as Busboy from 'busboy';
let router = express.Router();

router.post('/', (req: any, res: any)=>{
    let html = (filename)=>`
            <script type='text/javascript'>
                window.parent.CKEDITOR.tools.callFunction(${req.query.CKEditorFuncNum}, '${filename}','*');
            </script>
        `;
    res.set('Content-Type', 'text/html');

    let busboy = new Busboy({ headers: req.headers });
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      console.log('File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype);
      let newFilename = '' + new Date().getTime() + (Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000) + "." + filename.split('.').pop();
      let url = "https://s3.ap-northeast-2.amazonaws.com/hgus3/origin/cloudpos/" + newFilename;
      file.fileRead = [];

      file.on('data', (chunk) => {
          // Push chunks into the fileRead array
          this.fileRead.push(chunk);
        });

        file.on('error', (err) => {
          console.log('Error while buffering the stream: ', err);
        });

        file.on('end', () => {
            let options = {
                method: 'PUT',
                uri: url,
                headers: {
                    'Content-Type': 'image/jpg',
                    'x-amz-acl': 'public-read-write'
                },
                body: this.fileRead
            };
            request(options).then((result)=>{
                res.send(html(url));
            });
        });
    });
    req.pipe(busboy);
});

module.exports = router;