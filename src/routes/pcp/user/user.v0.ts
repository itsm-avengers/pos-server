/**
 * Created by sdh077 on 17. 3. 21.
 */
import { Router } from 'express';
import { db } from '../../../data-base/db';
let router = Router();
let oracledb = require('oracledb');
let jwt  = require("jsonwebtoken");

/**
 * �뿰�씫泥섎�� �궗�슜�븯�뿬 �궗�슜�옄瑜� 寃��깋�븳�떎.
 */
router.get('/search',(req, res) => {
    console.log('user search get')
    let tel = req.query.tel;
    let storeNo = req.user.uid;
    let kiosk = req.query.kiosk == 'true' ? true : false;
    console.log(tel+", "+storeNo+", "+kiosk);
    let query =`
        select 
            a.user_name as "userName", 
            b.login_site as "loginSite",
            a.tel as "tel", a.user_no as "userNo", 
            b.total_point as "point", 
            (select sum(CURRENT_CNT) from STAMP_PUBLISH WHERE user_no = a.user_no and store_no = ${storeNo} GROUP BY user_no) as "stamp",
            (select MILEAGE_TOTAL from user_mileage WHERE user_no = a.user_no and store_no = ${storeNo}) as "mileage"
        from user_master a , user_person b 
        where `;
    if(kiosk){
        query += ` a.tel = '${tel}' `;
    }else{
        query += ` a.tel like '%${tel}%' `;
    }
    query +=` and b.user_no = a.user_no 
              and a.USER_TYPE = 1`;

    db.query(query)
    .then(userInfo=>{
        console.log(JSON.stringify(userInfo));
        res.status(200).send(userInfo);
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 留덉씪由ъ�� �궡�뿭�쓣 媛��졇�삩�떎
 */
router.get('/mileage/log',(req, res) => {
    let storeNo = req.user.uid;
    let userNo = req.query.userNo;
    let pageNo = req.query.pageNo;
    let pageSize = req.query.pageSize;

    let query =`
        select x.* 
        from ( 
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.* 
            FROM ( 
                select 
                    a.store_no "storeNo", 
                    b.store_name "storeName", 
                    to_char(a.TRAN_DTTM,'yyyy-mm-dd hh24:mi:ss') "tranDttm", 
                    a.TRAN_TYPE "tranType",
                    case when a.tran_type = 1 
                        then a.MILEAGE_POINT 
                        when a.tran_type=2 
                        then a.MILEAGE_POINT*-1 
                    end "mileage" 
                from user_mileage_log a, store_info b 
                where 
                    a.user_no = ${userNo} 
                    and a.store_no = b.store_no 
                    and a.store_no = ${storeNo} 
                order by a.tran_dttm desc 
                ) y 
            ) x 
        WHERE x."page" = ${pageNo} `;
    let get_total = `
        select count(*) "cnt"  
        from user_mileage_log a, store_info b 
        where 
            a.user_no = ${userNo} 
            and a.store_no = b.store_no 
            and a.store_no = ${storeNo}`;
    let mInfo = null;
    db.query(query)
    .then(mileageInfo=>{
        mInfo = mileageInfo;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:mInfo,
            total:total[0].cnt
        });
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 留ㅼ옣�쓽 怨좉컼由ъ뒪�듃瑜� 媛��졇�삩�떎
 */
router.get('/client',(req, res) => {
    let storeNo = req.user.uid;//query.storeNo;
    let pageNo = req.query.pageNo;
    let pageSize = req.query.pageSize;
    let search = req.query.search;
    console.log(req.user)
    if(!pageNo || !pageSize){
        res.status(400).send({message:"not enough params"});
        return;
    }
    if(search == undefined || search == null)
        search = '';

    let get_client =`
        select x.*  
        from (     
            SELECT 
                CEIL(ROWNUM/${pageSize}) AS "page", 
                y.* 
            FROM 
                (select 
                    a.user_no as "userNo",  
                    a.user_name as "userName", 
                    a.tel as "tel", 
                    c.tran_cnt as "tranCnt", 
                    f_get_stamp_cnt(a.user_no,c.store_no) as "stampCnt", 
                    b.total_point as "totalPoint", 
                    (select MILEAGE_TOTAL from user_mileage WHERE user_no = a.user_no and store_no = ${storeNo} ) as "mileage" 
                from 
                    user_master a, user_person b, store_favorite c 
                where 
                    a.user_no = b.user_no 
                    and a.user_no = c.user_no 
                    and c.store_no = ${storeNo} 
                    and c.use_yn = '1'  
                    and (a.tel like '%${search}%' or a.user_name like '%${search}%') 
                    order by a.user_name asc 
                ) y 
            ) x 
        WHERE x."page" = ${pageNo}`;

    let get_total =`
        select 
            count(a.user_no) as "total" 
        from user_master a, user_person b, store_favorite c 
        where 
            a.user_no = b.user_no 
            and a.user_no = c.user_no 
            and c.use_yn = '1'   
            and c.store_no = ${storeNo} 
            and (a.tel like '%${search}%' or a.user_name like '%${search}%')`;

    let clients = [];
    db.query(get_client)
    .then(client_info=>{
        clients = client_info;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            list:clients,
            total:total[0].total
        });
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err});
    });
});

/**
 * 留ㅼ옣�뿉�꽌 嫄곕옒�븳 �뒪�뀥�봽 嫄곕옒�궡�뿭�쓣 媛��졇�삩�떎
 */
router.get('/stamp',(req, res) => {
    // error list
    // not enough params
    let storeNo = req.user.uid;
    let userNo = req.user.no;

    let pageSize = req.query.pageSize;
    let pageNo = req.query.pageNo;
    let startDt = req.query.startDt + ' 00:00';
    let endDt = req.query.endDt + ' 23:59';
    let tel = ((req.query.tel == undefined || req.query.tel == null) ? '' : req.query.tel);
    if(!pageSize || !pageNo || !startDt || !endDt){
        res.status(400).send({message:"not enough params"});
        return;
    }

    let get_publish = `
        select 
            sum(a.SAVING_CNT) as "publish" 
        from stamp_saving a , stamp_publish b,user_master c 
        where 
            a.STAMP_NO = b.STAMP_NO 
            and b.store_no=${storeNo} 
            and c.user_no = b.user_no 
            and a.SAVING_DTTM between to_date('${startDt}', 'yyyy-mm-dd hh24:mi') and 
                                      to_date('${endDt}', 'yyyy-mm-dd hh24:mi') 
            and c.tel like '%${tel}%' `;

    let get_useCNT = `
        select 
            sum(a.USE_CNT) as "use" 
        from stamp_use a , stamp_publish b ,user_master c 
        where 
            a.STAMP_NO = b.STAMP_NO 
            and b.store_no=${storeNo} 
            and c.user_no = b.user_no 
            and a.USE_DTTM between to_date('${startDt}', 'yyyy-mm-dd hh24:mi') and 
                                      to_date('${endDt}', 'yyyy-mm-dd hh24:mi') 
            and c.tel like '%tel%' `;

    let get_info = `
        select x.*   
        from (       
            SELECT CEIL(ROWNUM/${pageSize}) AS "page", y.*          
            FROM (               
                select 
                    c.user_no as "userNo",                      
                    to_char(b.saving_dttm,'yyyy-mm-dd hh24:mi:ss') as "tranDttm",                      
                    c.user_name as "userName",                      
                    c.tel as "tel",                      
                    b.sales_amt as "salesAmt",                     
                    b.saving_cnt as "savingCnt",                      
                    null as "useCnt",                      
                    null as "useTxt"                  
                from stamp_publish a, stamp_saving b, user_master c  
                where     
                    a.store_no = ${storeNo}                  
                    and a.stamp_no = b.stamp_no 
                    and a.user_no = c.user_no 
                    and b.saving_dttm between to_date('${startDt}', 'yyyy-mm-dd hh24:mi') and 
                                      to_date('${endDt}', 'yyyy-mm-dd hh24:mi') 
                    and c.tel like '%${tel}%' 
                UNION ALL  
                select 
                    c.user_no as "userNo",
                    to_char(b.use_dttm,'yyyy-mm-dd hh24:mi:ss') as "tranDttm",                      
                    c.user_name as "userName",
                    c.tel as "tel",                      
                    null as "salesAmt",                      
                    null as "savingPoint",                     
                    b.use_CNT as "useCnt",                     
                    b.use_text as "useTxt"                 
                from stamp_publish a, stamp_use b, user_master c  
                where 
                    a.store_no = ${storeNo} 
                    and a.stamp_no = b.stamp_no 
                    and a.user_no = c.user_no 
                    and b.use_dttm between to_date('${startDt}', 'yyyy-mm-dd hh24:mi') and 
                                      to_date('${endDt}', 'yyyy-mm-dd hh24:mi') 
                    and c.tel like '%${tel}%' 
                ) y 
            order by y."tranDttm" desc 
            ) x 
        WHERE x."page" = ${pageNo} `;
    let get_total = `
        select 
            count(USER_NO) as "total" 
        from (
            select c.user_no 
            from stamp_publish a, stamp_saving b, user_master c 
            where 
                a.store_no = ${storeNo} 
                and a.stamp_no = b.stamp_no 
                and a.user_no = c.user_no 
                and b.saving_dttm between to_date('${startDt}', 'yyyy-mm-dd hh24:mi') and 
                                      to_date('${endDt}', 'yyyy-mm-dd hh24:mi') 
                and c.tel like '%${tel}%' 
            union all 
            select c.user_no 
            from stamp_publish a, stamp_use b, user_master c 
            where 
                a.store_no = ${storeNo}  
                and a.stamp_no = b.stamp_no 
                and a.user_no = c.user_no 
                and c.tel like '%${tel}%' 
                and b.use_dttm between to_date('${startDt}', 'yyyy-mm-dd hh24:mi') and 
                                      to_date('${endDt}', 'yyyy-mm-dd hh24:mi')
            )`;
    
    let publish = null;
    let use = null;
    let info = [];
    db.query(get_publish)
    .then(pub=>{
        publish = pub[0].publish;
        return db.query(get_useCNT);
    })
    .then(cnt=>{
        use = cnt[0].use;
        return db.query(get_info);
    })
    .then(list=>{
        info = list;
        return db.query(get_total);
    })
    .then(total=>{
        res.status(200).send({
            total:total[0].total,
            list:info,
            saving:publish,
            use:use
        });
    })
    .catch(err=>{
        res.status(500).send({message:err});
    });
});

/**
 * 留ㅼ옣�뿉�꽌 怨좉컼�씠 �궗�슜�븷�닔 �엳�뒗 �뒪�꺃�봽 由ъ뒪�듃瑜� 媛�吏�怨� �삩�떎
 */
router.get('/stampActive',(req, res) => {
    console.log('pos stamp active get')
    let storeNo = req.user.uid;
    let userNo = req.query.userNo;
    let query = `
        select 
            stamp_unit as "unit", 
            exchange_text as "exchange" 
        from store_stamp_attr a 
        where 
            a.store_no = ${storeNo} 
            and stamp_unit <= ( 
                select sum(current_cnt)
                from stamp_publish 
                where user_no = ${userNo} and store_no =  ${storeNo})
        order by stamp_unit asc `;
    
    db.query(query)
    .then(result=>{
        res.status(200).send(result);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).send({message:err});
    });
});

/**
 * �뒪�꺃�봽 �궗�슜 �궡�슜�쓣 ����옣�븳�떎.
 */
router.post('/stamp',(req, res) => {
    console.log('pos stamp use post')
    let storeNo = req.user.uid;
    let barcode = req.body.barcode;
    let useCnt = req.body.useCnt;
    let useTxt = req.body.useTxt;
    let proc_stamp_using = `
        BEGIN PROC_STAMP_USING(
            :i_barcode,
            :i_store_no,
            :i_use_cnt,
            :i_use_text,
            :o_result
        ); END;`;
    let result = null;
    db.query(proc_stamp_using,null, [barcode, storeNo, useCnt, useTxt, 
                                    {dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 100 }])
    .then((proc_result:any)=>{
        if(proc_result && proc_result.outBinds)
            result = proc_result.outBinds;
        else result = null;
        if(result == "OK"){
            res.status(201).send("stamp use success");
        }else{
            throw "proc_err";
        }
    })
    .catch(err=>{
        if(err == "proc_err"){
            res.status(400).send({message:result});
        }else{
            res.status(500).send({message:err.toString()});
        }
    });
});
module.exports = router;