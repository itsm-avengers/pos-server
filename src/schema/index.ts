import {GraphQLSchema} from 'graphql';
import {makeExecutableSchema} from 'graphql-tools';
import { SubscriptionManager } from 'graphql-subscriptions';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { validateToken } from '../jwt';
import { REDIS_HOST } from '../config';
/* tslint:disable:no-var-requires */
const modules = [
  require("./modules/user-type"),
  require("./modules/area-type"),
  require("./modules/store-type"),
  require("./modules/menu-type"),
  require("./modules/order-type"),
  require("./modules/query"),
  require("./modules/mutation"),
  require("./modules/subscription")
];

const mainDefs = [`
    schema {
        query: Query,
        mutation: Mutation,
        subscription: Subscription
    }
`,
];

const resolvers = Object.assign({},
  ...(modules.map((m) => m.resolver).filter((res) => !!res)));

const typeDefs = mainDefs.concat(modules.map((m) => m.typeDef).filter((res) => !!res));

const Schema: GraphQLSchema = makeExecutableSchema({
  logger: console,
  resolverValidationOptions: {
    requireResolversForNonScalar: false,
  },
  resolvers: resolvers,
  typeDefs: typeDefs,
});

const pubsub = new RedisPubSub({
  connection: {
    host: REDIS_HOST,
    port: '6379',
    retry_strategy: options => {
      // reconnect after
      return Math.max(options.attempt * 100, 3000);
    }
  }
});
const subscriptionManager = new SubscriptionManager({
  schema: Schema,
  setupFunctions: {
    orderAdded: (options, args) => ({
      orderAdded: {
        filter: res => {
          return validateToken(args.token).then(user=>{
            return res.storeNo === user.uid;
          });
        }
      },
    })
  },
  pubsub
});

export {Schema, subscriptionManager, pubsub};
