export const typeDef = `
type AreaType {
    id: Int!
    name: String
    campaign: String
    imageURL: String
    text: String
    agreement: String
    nMembers: Int
    stores: [StoreType]
}
`;

export const resolver = {
  AreaType: {
    stores: (root, args, ctx) => {
      return ctx.db.stores(args.page, args.orderBy, root.id, null, null, ctx.user.no).then(res=>res);
    }
  },
};