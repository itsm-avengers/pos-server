import * as func from '../../lib';
export const typeDef = `
# Mutations
type Mutation {
  addReview(input: ReviewInput): Review
  deleteReview(reviewNo: Int!): Boolean
  addComment(input: CommentInput): Comment
  deleteComment(commentNo: Int!): Boolean
  voteReview(reviewNo: Int!): Review
  voteStore(storeNo: Int!): Boolean
  # POST/master + POST/detail + POST/detail/multi + PUT/detail/:orderNo + DELETE/detail/:orderNo
  # orderNo == null -> insert
  # orderNo == null && detail == null -> order_master만 insert
  # orderNo != null -> update
  # orderNo != null && detail == null -> delete
  addOrder(input: OrderInput!, details: [OrderDetailInput], deviceId: String): OrderType
  deleteOrder(orderNo: Int!, deviceId: String): Boolean
  addOpt(name: String!, amount: Int!): MenuOpt
  deleteOpt(id: ID!): Boolean
  addOptToMenu(id: ID!, menuNo: ID!): Boolean
  deleteOptFromMenu(id: ID!, menuNo: ID!): Boolean
  addStory(id: Int, input: StoryInput!): Story
  deleteStory(storyNo: Int!):Boolean
  readStory(storyNo:Int):Int
  addCredit(input: OrderInput, details: [OrderDetailInput]): OrderType
  sync(queue: [OrderQuereItem]): Boolean
  addOptGroup(name: String!, multYn: String!): OptGroup
  # 캐스캐이딩
  deleteOptGroup(id: ID): Boolean  
  addOptGroupItem(group_id: ID, opt_id: ID): Boolean
  deleteOptGroupItem(group_id: ID, opt_id: ID): Boolean
  addGroupToMenu(group_id: ID, menu_id: ID): Boolean
  deleteGroupFromMenu(group_id: ID, menu_id: ID): Boolean
}

input OrderQuereItem {
  time: Int
  order: OrderInput
  detail: [OrderDetailInput]
}

input StoryInput {
    text:String
    title:String
    status:String
    imageURL: String
    contentURL: String
    year:Int
    month:Int
    volume: Int
}

input ReviewInput {
  reviewNo: Int
  storeNo: Int!
  rating: Int!
  text: String!
  image: Image!
}

input CommentInput {
  commentNo: Int
  reviewNo: Int
  text: String!
}

input OrderDetailInput {
  menuNo:Int!
  menuName:String!
  itemAmt:Int!
  itemCNT:Int!
  dcName:String
  dcAmt:Int
  # 메뉴 개수 수정 정도(+3개면 -3, -3개면 +3으로)
  diff:Int!
  # 0:비과세 1:과세
  taxType:String!
  opts:[OrderOptInput]
}

input OrderOptInput {
  menu_opt_no: Int!
  cnt: Int
}

input OrderInput {
  #detail메뉴 수정시에만 입력, undefined일 경우 메뉴 추가
  orderNo:Int
  orderName:String!
  orderType:String!
  waitingNo:Int
  tables:String
  floorNo:Int
}
`;


export const resolver = {
  Mutation: {
    addReview: (root, args, ctx) => {
      if (ctx.user.no == -1)
        console.log('로그인이 필요한 기능입니다.');
      else
        return ctx.db.addReview(args.input, ctx.user.no).then(res=>res[0]);
    },
    deleteReview: (root, args, ctx) => {
      if (ctx.user.no == -1)
        console.log('로그인이 필요한 기능입니다.');
      else
        return ctx.db.deleteReview(args.reviewNo, ctx.user.no).then(res=>res);
    },
    addComment: (root, args, ctx) => {
      if (ctx.user.no == -1)
        console.log('로그인이 필요한 기능입니다.');
      else
        return ctx.db.addComment(args.input, ctx.user.no).then(res=>res[0]);
    },
    deleteComment: (root, args, ctx) => {
      if (ctx.user.no == -1)
        console.log('로그인이 필요한 기능입니다.');
      else
        return ctx.db.deleteComment(args.commentNo, ctx.user.no).then(res=>res);
    },
    voteReview: (root, args, ctx) => {
      if (ctx.user.no == -1)
        console.log('로그인이 필요한 기능입니다.');
      else
        return ctx.db.voteReview(args.reviewNo, ctx.user.no).then(res=>res[0]);
    },
    voteStore: (root, args, ctx) => {
      if (ctx.user.no == -1)
        console.log('로그인이 필요한 기능입니다.');
      else
        return ctx.db.voteStore(args.storeNo, ctx.user.no).then(res=>res);
    },
    addOrder : (root, args, ctx ) => {
        console.log('add order');
        if (ctx.user.no == -1)
            console.log('로그인이 필요한 기능입니다.');
        else {
            return  ctx.db.addOrder(args.input, args.details, ctx.user.no, ctx.user.uid)
                    .then(res=>{
                      console.log('end add order');
                      func.orderPublish(ctx.user.uid,res.id,'changed', args.deviceId).then(_=>console.log('fcm send success')).catch(err=>console.log('fcm_err: '+err));
                      return res;
                    });
        }
    },
    addOpt : (root, args, ctx ) => {
      console.log('add opt');
      if (ctx.user.no == -1) {
        console.error('로그인이 필요한 기능입니다.');
        throw 'no auth'
      } else {
        return ctx.db.addOpt(args.name, args.amount, ctx.user.uid)
        .then(res=>{
          return res;
        });
      }
    },
    deleteOpt : (root, args, ctx ) => {
      console.log('delete opt');
      if (ctx.user.no == -1) {
        console.error('로그인이 필요한 기능입니다.');
        throw 'no auth'
      } else {
        return ctx.db.deleteOpt(args.id, ctx.user.uid);
      }
    },
    addOptToMenu : (root, args, ctx ) => {
      console.log('add opt');
      if (ctx.user.no == -1) {
        console.error('로그인이 필요한 기능입니다.');
        throw 'no auth'
      } else {
        return ctx.db.addOptToMenu(args.id, args.menuNo);
      }
    },
    deleteOptFromMenu : (root, args, ctx ) => {
      console.log('delete opt');
      if (ctx.user.no == -1) {
        console.error('로그인이 필요한 기능입니다.');
        throw 'no auth'
      } else {
        return ctx.db.deleteOptFromMenu(args.id, args.menuNo);
      }
    },
    addCredit : (root, args, ctx ) => {
        if (ctx.user.no == -1)
            console.log('로그인이 필요한 기능입니다.');
        else
            return ctx.db.addCredit(args.input, args.details, ctx.user.no, ctx.user.uid).then(res=>{ ctx.pubsub.publish("orderAdded", { storeNo: ctx.user.uid, order: res }); return res;});
    },
    deleteOrder : (root, args, ctx ) => {
        console.log('delete order');
        if (ctx.user.no == -1)
            console.log('로그인이 필요한 기능입니다.');
        else
            return ctx.db.deleteOrder(args.orderNo, ctx.user.uid)
                  .then(res=>{
                    // ctx.pubsub.publish("orderAdded", { storeNo: ctx.user.uid, order: res }); 
                    func.orderPublish(ctx.user.uid,args.orderNo,'deleted', args.deviceId).then(_=>console.log('fcm send success')).catch(err=>console.log('fcm_err: '+err));
                    return res;
                  });
            // return ctx.db.deleteOrder(args.orderNo, ctx.user.uid).then(res=>{ ctx.pubsub.publish("orderAdded", { storeNo: ctx.user.uid }); return res; } );
    },
    addStory : (root, args, ctx ) =>{
        if (args.id)
          return ctx.db.updateStory(args.id,args.input);
        else
          return ctx.db.addStory(args.input);
    },
    readStory : (root, args, ctx ) =>{
        return ctx.db.readStory(args.storyNo);
    },
    deleteStory : (root, args, ctx ) =>{
        return ctx.db.deleteStory(args.storyNo);
    },
    sync(root, args, ctx) {
        ctx.pubsub.publish("orderAdded", { storeNo: ctx.user.uid });
        console.log("ctx.pubsub")
        console.log(ctx.pubsub.publish)
        return true;
    },
    // addOptGroup(name: String!, multYn: String!): Boolean
    async addOptGroup(root, args, ctx){
      let get_group_no = `select SEQ_OPT_GROUP_NO.NEXTVAL "id" from dual`;
      let group_no = 0;
      try{
        group_no = await ctx.db.query(get_group_no);
        group_no = group_no[0].id;
        let sql = `
          insert into opt_group (group_no, store_no, mult_yn, name) 
          values (${group_no}, ${ctx.user.uid}, '${args.multYn}', '${args.name}') `;
        await ctx.db.query(sql);
        return {
          id:group_no,
          multYn:args.multYn,
          name:args.name
        }
      }catch(err){
        console.error('ERROR: add opt group.');
        console.error(err);
        return {
          id:group_no,
          multYn:args.multYn,
          name:args.name
        }
      }
    },
    //cascade
    // deleteOptGroup(group_id: ID): Boolean  
    async deleteOptGroup(root, args, ctx){
      let delete_menu_opt_group = ` delete from menu_opt_group where group_no = ${args.id} `;
      let delete_opt_group_item = ` delete from opt_group_item where group_no = ${args.id} `;
      let delete_opt_group = ` delete from opt_group where group_no = ${args.id} `; 
      let connection = null;
      try{
        connection = await ctx.db.getConnection();
        await ctx.db.query(delete_menu_opt_group, connection);
        await ctx.db.query(delete_opt_group_item, connection);
        await ctx.db.query(delete_opt_group, connection);      
        await connection.commit();
        connection.close().then();
        ctx.db.release
        return true;
      }catch(err){
        if(connection) connection.close().then();
        console.error('ERROR: delete opt group. ');
        console.error(err);
        return false;
      }
    },
    // addOptGroupItem(group_id: ID, opt_id: ID): Boolean
    async addOptGroupItem(root, args, ctx){
      let sql = `
        insert into opt_group_item (group_no, menu_opt_no) 
        values (${args.group_id}, ${args.opt_id}) `;
      try{
        await ctx.db.query(sql);
        return true;
      }catch(err){
        console.error('ERROR: add opt group item.');
        console.error(err);
        return false;
      }
    },
    // deleteOptGroupItem(group_id: ID, opt_id: ID): Boolean
    async deleteOptGroupItem(root, args, ctx){
      let sql = ` delete from opt_group_item where group_no = ${args.group_id} and menu_opt_no = ${args.opt_id}`;
      try{
        await ctx.db.query(sql);
        return true;
      }catch(err){
        console.error('ERROR: delete opt group item.');
        console.error(err);
        return false;
      }
    },
    // addGroupToMenu(group_id: ID, menu_id: ID): Boolean
    async addGroupToMenu(root, args, ctx){
      let sql = ` insert into menu_opt_group (group_no, menu_no, reg_dttm) values (${args.group_id},${args.menu_id}, sysdate) `;
      try{
        await ctx.db.query(sql);
        return true;
      }catch(err){
        console.error('ERROR: add menu opt group.');
        console.error(err);
        return false;
      }
    },
    // deleteGroupFromMenu(group_id: ID, menu_id: ID): Boolean
    async deleteGroupFromMenu(root, args, ctx){
      let sql = ` delete from menu_opt_group where group_no = ${args.group_id} and menu_no = ${args.menu_id} `;
      try{
        await ctx.db.query(sql);
        return true;
      }catch(err){
        console.error('ERROR: dekete menu opt group.');
        console.error(err);
        return false;
      }
    }
  },
};
