/**
 * Created by sdh077 on 17. 4. 13.
 */
import * as request from "request-promise";
export const typeDef = `
  type OrderType {
      id:Int!
      total: Int
      dcTotal: Int
      orderDttm: String
      detail: [OrderDetail]
      error: Error
      tableId: String
  }

  type Error {
    #itemCNT error
    #stock error
    key: String
    value: String
  }
  
  type OrderDetail {
    id: Int!
    orderNo: Int!
    menuNo: Int!
    # 할인 명
    dcName: String
    # 할인 금액
    dcAmt: Int 
    menuName:String
    # 메뉴 수량
    itemCNT:Int
    # 메뉴 가격
    itemAmt: Int
    # 재고
    stockCNT : Int
    #메뉴 변동량
    diff: Int
    #0:비과세, 1:과세
    taxType: String!
    #선택된 메뉴 옵션
    opts: [OrderOpt]
  }

  type OrderOpt {
    id: String!
    name: String
    cnt: Int
    amt: Int
  }
  `;

export const resolver = {
  OrderType : {
      detail(root, args, ctx) {
        if (!root.id)
          return null;
        var sql = `
          select 
            a.ORDER_ITEM_SEQ "id",
            a.ORDER_NO "orderNo", 
            a.MENU_NO "menuNo", 
            a.MENU_NAME "menuName", 
            a.ITEM_CNT "itemCNT", 
            a.ITEM_AMT "itemAmt", 
            a.DC_NAME "dcName", 
            a.DC_AMT "dcAmt",
            DECODE(b.STOCK_SALE_YN,'1',nvl(b.STOCK_CNT,0),-1) "stockCNT",
            0 "diff",
            b.TAX_TYPE "taxType"     
          from ORDER_DETAIL a, STORE_MENU b  
          where 
            ORDER_NO = ${root.id} 
            and b.menu_no = a.menu_no `;
        return ctx.db.query(sql);
      }
  },
  OrderDetail : {
      opts(root, args, ctx){
        if (!root.id)
          return null;
        var sql = `
          select  (order_item_seq || '-' || opt_name) "id", opt_name, opt_cnt, opt_amt 
          from    order_opt 
          where   order_item_seq = ${root.id} `;
        return ctx.db.query(sql);
      }
  } 
};
