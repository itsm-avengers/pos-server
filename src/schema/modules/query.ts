import * as StoreType from './store-type';

export const typeDef = `
# Root Query
type Query {
    testStringConnector: String
    areas: [AreaType]
    getArea(id: Int!): AreaType
    stores(page: Page = {offset: 0, limit: 100}, orderBy: OrderBy = NAME, areaNo: Int, q: String): [StoreType]
    getStore(id: Int): StoreType
    #update order info
    getOrder(id: Int!) : OrderType 
    reviews(page: Page = {offset: 0, limit: 100}, storeNo: Int): [Review]
    comments(reviewNo: Int!): [Comment]
    getUser: UserType
    stories(volume: Int, page: Page = {offset: 0, limit: 100}): [Story]
    getStory(id: Int!): Story
    volumes: [Volume]
    feeds(page: Page = {pffset: 0, limit: 10}): [Feed]
    getFeed(id: Int!): Feed
}

type Feed {
  id: Int!
  title: String
  date: String
  image: String
  text: String
}

type Story {
    id:Int!
    regDate: String
    text: String
    readCNT: Int
    status: String
    title: String
    imageURL: String  
    contentURL: String
    year:Int
    month:Int
    volume: Int
    volumeName: String
}

type Review {
    id: Int!
    userNo: Int
    isMine: Boolean
    user: UserType
    storeNo: Int
    storeName: String
    text: String
    imageURL: String
    thumbnailURL: String
    nComments: Int
    nRecomends: Int
    nVotes: Int
    rating: Int
    regDate: String
    comments: [Comment]
    isVoted: Boolean
}

type Comment {
    id: Int!
    reviewNo: Int
    userNo: Int
    user: UserType
    text: String
    regDate: String
    isMine: Boolean
}

input Image {
  imageURL: String
  thumbnailS: String
  thumbnailM: String
  thumbnailL: String
}

input Page {
  offset: Int!
  limit: Int!
}

type Volume {
  id: Int!
  name: String
}

enum OrderBy {
  NAME
  REVIEW
  RATING
  VOTE
}
`;

let getUserById = (root, args, ctx) => {
    return ctx.db.user(root.userNo).then(res=>res[0]);
}

let isMine = (root, args, ctx) => {
  return ctx.user.no == root.userNo;
}

export const resolver = {
  Query: {
    testStringConnector(root, args, ctx) {
      return ctx.testConnector.testString;
    },
    areas(root, args, ctx) {
      return ctx.db.areas().then(result=>result).catch((err)=>{
        console.log(err);
      });
    },
    getArea(root, args, ctx) {
      return ctx.db.areas(args.id).then(result=>result[0]).catch((err)=>{
        console.log(err);
      });
    },
    stores(root, args, ctx) {
      return ctx.db.stores(args.page, args.orderBy, args.areaNo, null, args.q, ctx.user.no).then(res=>res).catch((err)=>{
        console.log(err);
      });
    },
    getStore(root, args, ctx) {
      if (args.id==undefined) {
        if (ctx.user.uid==undefined)
          Error('Unauthorizted');
        else
          args.id = ctx.user.uid;
      }
      return ctx.db.stores(args.page, args.orderBy, null, args.id, null, ctx.user.no).then(res=>res[0]).catch((err)=>{        
        console.log(err);
      });
    },
    getOrder(root, args, ctx){
      if(args.id==undefined)
          Error('Unauthorized');

      return ctx.db.getChangedOrder(args.id).then(res=>res[0]).catch((err)=>{
        console.log(err);
      });
    },
    reviews(root, args, ctx) {
      return ctx.db.reviews(args.page, args.storeNo, ctx.user.no).then(res=>res);
    },
    comments(root, args, ctx) {
      return ctx.db.comments(args.reviewNo, ctx.user.no).then(res=>res);
    },
    getUser(root, args, ctx) {
      return ctx.db.user(ctx.user.no).then(res=>res[0]);
    },
    stories(root, args, ctx) {
        return ctx.db.story(-1, args.volume, args.page);
    },
    getStory(root, args, ctx) {
        return ctx.db.story(args.id).then(res=>res[0]);
    },
    volumes(root, args, ctx) {
      var sql = `select DETAIL_CODE "id", CODE_NAME "name" FROM CODE_MASTER where MASTER_CODE='500' and CODE_TYPE='2' order by DISP_ORDER`;
      return ctx.db.query(sql);
    },
    feeds(root, args, ctx) {
      return ctx.db.feeds(null, args.page);
    },
    getFeed(root, args, ctx) {
      return ctx.db.feeds(args.id).then(res=>res[0]);
    }
  },
  Review: {
    user: getUserById,
    comments: (root, args, ctx) => {
      return ctx.db.comments(root.id).then(res=>res);
    },
    isMine: isMine
  },
  Comment: {
    user: getUserById,
    isMine: isMine
  }
};