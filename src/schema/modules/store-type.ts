import * as request from "request-promise";
import * as DataLoader from "dataloader";
import { db } from '../../data-base/db';

export const typeDef = `
type StoreType {
    id: Int!
    name: String
    isVoted: Boolean
    campaign: String
    text: String
    areaNo: Int
    area: [AreaType]
    images: [StoreImage]
    mainImageURL: String
    reviews(page: Page): [Review]
    nReviews: Int
    nMembers: Int
    rating: Float
    addr1: String
    addr2: String
    tel: String
    serviceTime: String
    locationInfo: String
    parkingInfo: String
    membershipInfo: String
    attrs: [Attr]
    hashtag: String
    lat: Float
    lng: Float
    nlsCode: String 
    category(nlsCode: String): [Category]
    floor: [Floor]
    table: [Table]
    order: [OrderType]
    opts: [MenuOpt]
    optGroups: [OptGroup]
    posNm: String
    posBackUrl: String
    promotionImg: String
}

type Menu{
   id:Int
   menuNo: Int
   menuType : String
   name: MenuName
   taxType:String
   taxAmt:Int
   saleAmt:Int
   categoryNo:Int
   regDttm: String
   updDttm: String
   soldOutYN: String
   menuCode: String
   unitPrice: Int
   useYN: String
   dispOrder: Int
   stampUseYN:String
   takeoutDCAmt:Int
   menuBgColor:String
   stockCNT: Int
   stockSaleYN: String
   thumbnailS: String
   thumbnailM: String
   thumbnailL: String
   startDt: String
   endDt: String 
   subList: [SubList]
   opts: [MenuOpt]
   optGroups: [OptGroup]
   #메뉴 변동량
   diff: Int
}

type Category {
   id: Int
   name : CategoryName
   tabColorRgb : String
   dispOrder : Int
   menu: [Menu]
}
type CategoryName {
  id: String
  ko: String
  en: String
  cn: String
  ja: String
}
type MenuName {
  id: String
  ko: String
  en: String
  cn: String
  ja: String
}

type SubList{
  id: String
  subMenuNo: Int
  menuText: String
  menuCNT: Int
}

type Floor {
   id:Int
   name:String
   tableCNT:Int
   dispOrder:Int
   table:[Table]
}
type Table {
   id:Int
   floorId:Int
   name:String
   seatCNT:Int
   dispOrder:Int
   floorName:String
   order: OrderType
   dispShape: String
}
type Attr {
    id: String!
    code: String
    value:String
}
type StoreImage {
    id: String!
    dispOrder: Int
    imageURL: String
    thumbnailS: String
    thumbnailM: String
    thumbnailL: String
}
type MenuOpt {
  id: Int!
  name: String
  amt: Int
}
type OptGroup{
  id: Int!
  multYn: String
  name: String
  opts: [MenuOpt]
}
`;
let storeNo = null;
export const resolver = {
  StoreType: {
    images: (root, args, ctx) => {
      return ctx.db.storeImages(root.id).then(res => res);
    },
    reviews: (root, args, ctx) => {
      return ctx.db.reviews(args.page, root.id, ctx.user.no).then(res => res);
    },
    area: (root, args, ctx) => {
      return ctx.db.areas(root.areaNo).then(res => res);
    },
    category(root, args, ctx){
      storeNo = root.id;
      let get_category = `
          select 
              CATEGORY_NO as "categoryNo",
              tab_color_rgb as "tabColorRgb",
              DISP_ORDER as "dispOrder"
          from store_menu_category a 
          where 
              store_no=${root.id} 
              and use_yn = '1' 
          order by disp_order asc `;
  
      return ctx.db.query(get_category)
        .then((info) => {
          var list = [];
          info.map((value) => {
            list.push({
              id: value.categoryNo,
              name: value.name,
              dispOrder: value.dispOrder,
              tabColorRgb: value.tabColorRgb
            });
          });
          return list;
        }).catch(function (err) {          
          // Crawling failed...
        });
    },
    floor(root, args, ctx){
      console.log('gf - get floor');
      let query = `
        select 
            FLOOR_NO as "floorNo",
            floor_name as "floorName",
            TABLE_CNT as "tableCNT",
            DISP_ORDER as "dispOrder" 
        from store_floor 
        where STORE_NO=${root.id} 
        order by DISP_ORDER asc `;
      return ctx.db.query(query)
      .then(info=>{
        let list = [];
        info.map((value) => {
          list.push({
            id: value.floorNo,
            dispOrder: value.dispOrder,
            tableCNT: value.tableCNT,
            name: value.floorName
          });
        });
        return list;
      }).catch((err) => {
        console.log(err.toString())
        // Crawling failed...
      });
    },
    table(root,args,ctx){    
      console.log('gf - get table');
      console.time('table');
      let query = `
        select  a.FLOOR_NO as "floorId",
                a.TABLE_NO as "id",
                a.TABLE_NAME as "name",
                a.SEAT_CNT as "seatCNT",
                a.DISP_ORDER as "dispOrder",
                b.FLOOR_NAME as "floorName",
                a.DISP_SHAPE as "dispShape"
        from    store_table a, store_floor b
        where   b.store_no=${root.id} 
                and b.floor_no = a.floor_no
        order by a.disp_order asc`;
      return ctx.db.query(query);
    },
    order(root, args, ctx){
      var sql = `
        select 
          a.order_no "id", a.order_amt "total", 
          b.dc_amt*b.item_cnt "dcTotal", 
          to_char(a.order_dttm,'yyyy-mm-dd hh24:mi:ss') "orderDttm", 
          a.table_no_array "tableId" 
        from ORDER_MASTER a, ORDER_DETAIL b 
        where 
          a.order_status='1'
          and a.store_no = ${root.id} 
          and a.order_no = b.order_no
        order by a.order_no desc`;
        
      return ctx.db.query(sql);
    },
    attrs(root, args, ctx){
      return ctx.db.attrs(root.id, args.setYN).then(res => res);
    },
    opts(root, args, ctx) {
      var sql = `
        select 
          menu_opt_no "id", opt_name "name", opt_amt "amt" 
        from store_menu_opt
        where store_no = ${root.id} order by upd_dttm `;

      return ctx.db.query(sql);
    },
    optGroups(root, args, ctx){
      let sql = `
        select group_no "id", mult_yn "multYn", name "name" from opt_group where store_no = ${root.id} `;
      return ctx.db.query(sql);
    }
  },
  Floor : {
    table(root,args,ctx){    
      let query = `
        select  a.FLOOR_NO as "floorId",
                a.TABLE_NO as "id",
                a.TABLE_NAME as "name",
                a.SEAT_CNT as "seatCNT",
                a.DISP_ORDER as "dispOrder",
                b.FLOOR_NAME as "floorName",
                a.DISP_SHAPE as "dispShape"
        from    store_table a, store_floor b
        where   a.floor_no=${root.id} 
                and b.floor_no = a.floor_no
        order by a.disp_order asc`;
      return ctx.db.query(query);
    }
    // table(root,args,ctx){    
    //   findTableLoader.load(root.id);
    // }
  },
  Table: {
    order(root, args, ctx){
      var sql = `
        select 
          a.order_no "id", a.order_amt "total", sum(b.dc_amt*b.item_cnt) "dcTotal" to_char(a.order_dttm,'yyyy-mm-dd hh24:mi:ss') "orderDttm"  
        from ORDER_MASTER a, ORDER_DETAIL b
        where 
          a.ORDER_STATUS='1' 
          and a.table_no_array like '%${root.id}%' 
          and '${root.id}' in (
                              select regexp_substr(TABLE_NO_ARRAY,'[^|]+', 1, level) 
                              from ORDER_MASTER 
                              where order_status = 1 
                              connect by regexp_substr(TABLE_NO_ARRAY, '[^|]+', 1, level) is not null
                            ) 
          and a.order_no = b.order_no
        order by a.order_no desc`;
        
      return ctx.db.query(sql).then(res=>{
        if (res.length == 0)
          return null;
        else
          return res[0];
      });
    }
  },
  Category: {//categoryNo, regDttm, updDttm, soldOutYN, menuCode,unitPrice, useYN
    name(root, args, ctx) {
      let get_nls = `
        select  category_no || '-' || nls_code "id", nls_code "nlsCode", category_name "name" 
        from    store_menu_category_nls 
        where   store_no = ${ctx.user.uid} and category_no = ${root.id} `;
      return ctx.db.query(get_nls)
        .then(_=>{
          let returnVal = {};
          _.forEach(data=>{
            returnVal[data.nlsCode] = data.name;
          });
          return returnVal;
        })
        .catch(err=>{
          console.log(err);
          return null;
        })
    },
    menu(root, args, ctx) {
      // console.log('category!! '+root.id + root.nlsCode)
      var sql = `
        select 
            a.MENU_NO "id",               a.MENU_NO "menuNo",           a.MENU_TYPE "menuType", 
            a.DISP_ORDER "dispOrder",     a.TAX_TYPE "taxType",     
            a.TAX_AMT "taxAmt",           a.SALE_AMT "saleAmt",         a.STAMP_USE_YN "stampUseYN", 
            a.MENU_BG_COLOR "menuBgColor",a.STOCK_CNT "stockCNT",       a.STOCK_SALE_YN "stockSaleYN",
            a.THUMBNAIL_S "thumbnailS",   a.THUMBNAIL_M "thumbnailM",   a.THUMBNAIL_L "thumbnailL",
            a.CATEGORY_NO "categoryNo",   a.REG_DTTM "regDttm",         a.UPD_DTTM "updDttm", 
            a.SOLDOUT_YN "soldOutYN",     a.MENU_CODE "menuCode",
            a.UNIT_PRICE "unitPrice",     a.USE_YN "useYN",             to_char(a.START_DT, 'yyyy-mm-dd') "startDt", 
            to_char(a.END_DT, 'yyyy-mm-dd') "endDt", 0 "diff",
            CASE WHEN (select menu_name from store_menu_nls where menu_no = a.menu_no and nls_code = :nlsCode) is null 
                THEN (select menu_name from store_menu_nls where menu_no = a.menu_no and nls_code = :defulatCode) 
                ELSE (select menu_name from store_menu_nls where menu_no = a.menu_no and nls_code = :nlsCode)
            END "menuName"
        from STORE_MENU a
        where 
            a.CATEGORY_NO=${root.id} 
            and a.STORE_NO=${ctx.user.uid}
            and (a.USE_YN='1' or a.USE_YN='2') 
            and sysdate between a.START_DT and a.END_DT 
        order by DISP_ORDER asc`;
      return getDefaultLang(ctx, storeNo)
        .then(code =>{
           let defaultCode = code[0].nlsCode;
           return ctx.db.query(sql,null,[root.nlsCode, defaultCode, root.nlsCode]);
        })
        .catch(err=>{
          return {'error':err};
        });
      
    }
  },
  Menu: {
    name(root, args, ctx) {
      let get_nls = `
        select menu_no || '-' || nls_code "id", nls_code "nlsCode", menu_name "name" from store_menu_nls where menu_no = ${root.id} `;
      return ctx.db.query(get_nls)
        .then(_=>{
          let returnVal = {};
          _.forEach(data=>{
            returnVal[data.nlsCode] = data.name;
          });
          return returnVal;
        })
        .catch(err=>{
          console.log(err);
          return null;
        })
    },
    subList(root, args, ctx) {
      var sql = `
        select 
            set_menu_no || '-' || sub_menu_no "id", sub_menu_no "subMenuNo", menu_text "menuText", menu_cnt "menuCNT" 
        from store_set_menu 
        where set_menu_no = ${root.id}`;

      return ctx.db.query(sql);
    },
    opts(root, args, ctx) {
      var sql = `
        select  a.menu_opt_no "id", b.opt_name "name", b.opt_amt "amt" 
        from    menu_opt a, store_menu_opt b
        where   a.menu_no = ${root.id} and a.menu_opt_no = b.menu_opt_no 
        order   by b.upd_dttm`;

      return ctx.db.query(sql);
    },
    optGroups(root, args, ctx){
      let sql = `
        select  a.group_no "id", a.mult_yn "multYn", a.name "name" 
        from    opt_group a, menu_opt_group b 
        where   a.group_no = b.group_no 
                and b.menu_no=${root.id} 
        order   by b.reg_dttm asc `;
      return ctx.db.query(sql);
    }
  },
  OptGroup: {
    opts(root, args, ctx){
      let sql = `
        select  a.menu_opt_no "id", b.opt_name "name", b.opt_amt "amt" 
        from    opt_group_item a, store_menu_opt b
        where   a.group_no = ${root.id} and a.menu_opt_no = b.menu_opt_no 
        order   by b.upd_dttm `;
      return ctx.db.query(sql);
    }
  }
};

function getDefaultLang(ctx:any,storeNo:number){
  let get_default_lang = `
    select CASE WHEN nls_code is null THEN 'ko' ELSE nls_code END "nlsCode" from store_info where store_no = ${storeNo}`;
  return ctx.db.query(get_default_lang);
}