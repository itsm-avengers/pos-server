import { db } from '../../data-base/db';


export const typeDef = `
type Subscription {
    orderAdded(token: String!): OrderType
}
`;

export const resolver = {
  Subscription: {
    orderAdded: _ => _
  }
};