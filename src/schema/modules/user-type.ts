export const typeDef = `
type UserType {
    id: Int!
    name: String
    imageURL: String
}
`;

export const resolver = {
  UserType: {
    
  },
};